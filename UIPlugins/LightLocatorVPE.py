# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2018                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

# This is an adaptation of the ExampleSpotlightLocator provided with Katana.

#from Katana import FnAttribute, FnGeolibServices
from Katana import FnAttribute, FnGeolibServices, NodegraphAPI
from PluginAPI.BaseViewerPluginExtension import BaseViewerPluginExtension

class LightLocatorVPE(BaseViewerPluginExtension):
    """
    This class is responsible for automatically adding the LightLocatorVDC 
    viewer delegate component to a viewer delegate, and also for adding the 
    LightLocatorViewportLayer to viewports in order to draw the various
    3Delight for Katana light locations.
    """

    def __init__(self):
        """
        Initializes an instance of the class.
        """
        BaseViewerPluginExtension.__init__(self)

        self.__vdcName = 'LightLocatorVDC'
        self.__viewportLayerName = 'LightLocatorViewportLayer'

        self.__tab = None
        self.__drawSpotlightsInCyanAction = None
        self.__viewerDelegate = None

    def onDelegateCreated(self, viewerDelegate, pluginName):
        """
        Adds the C{'LightLocatorVDC'} viewer delegate component to
        the given viewer delegate.

        @type viewerDelegate: C{ViewerDelegate}
        @type pluginName: C{str}
        @param viewerDelegate: The C{ViewerDelegate} that was created.
        @param pluginName: The registered plug-in name of the
            C{ViewerDelegate}.
        """
        # pylint: disable=unused-argument

        self.__viewerDelegate = viewerDelegate
        viewerDelegate.addComponent('LightLocatorVDC', self.__vdcName)

    def onViewportCreated(self, viewportWidget, pluginName, viewportName):
        """
        Adds the C{'LightLocatorViewportLayer'} viewport layer to
        the given viewport widget.

        @type viewportWidget: C{ViewportWidget}
        @type pluginName: C{str}
        @type viewportName: C{str}
        @param viewportWidget: The C{ViewportWidget} that was created.
        @param pluginName: The registered plug-in name of the
            C{ViewportWidget}.
        @param viewportName: The easily identifiable name of the viewport
            which can be used to look-up the viewport in the C{ViewerDelegate}.
        """
        # pylint: disable=unused-argument

        layerCount = viewportWidget.getNumberOfLayers()
        layerIndex = 0
        for layerIndex in range(layerCount):
            layerName = viewportWidget.getLayerName(layerIndex)
            if layerName == 'CameraGateLayer':
                break

        layer = viewportWidget.insertLayer(
            'LightLocatorViewportLayer', self.__viewportLayerName, layerIndex)

        if layer is not None:
            layer.setOptionByName(
                'vdc_name', FnAttribute.StringAttribute(self.__vdcName))

    # Private Slots -----------------------------------------------------------

PluginRegistry = [
    ('ViewerPluginExtension', 1, 'LightLocatorVPE', LightLocatorVPE),
]

