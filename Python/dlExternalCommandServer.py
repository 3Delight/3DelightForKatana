################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2016                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

import NodegraphAPI
import PackageSuperToolAPI
import UI4
from Katana import RenderManager, ResolutionTable, Utils


def GetAnyRenderSettings():
    nodes = NodegraphAPI.GetAllNodesByType('RenderSettings')
    if len(nodes) == 0:
        return None

    for node in nodes:
        if NodegraphAPI.IsNodeEdited(node):
            return node
        if NodegraphAPI.IsNodeViewed(node):
            return node

    renderSettings = nodes[0]
    return renderSettings

def GetRenderSettings():
    nodes = NodegraphAPI.GetAllNodesByType('DlSettings')
    if len(nodes) == 0:
        return GetAnyRenderSettings()

    dlSettings = nodes[0]
    for node in nodes:
        if NodegraphAPI.IsNodeEdited(node):
            dlSettings = node
            break
        if NodegraphAPI.IsNodeViewed(node):
            dlSettings = node
            break

    children = dlSettings.getChildren()
    for child in children:
        if child.getType() == 'RenderSettings':
            return child

    return None

def EditScene( objs ):
    # Get main objects
    operationname = objs[0]

    if operationname == 'start render':
        live = objs[1]
        startNode = NodegraphAPI.GetNode(objs[2])
        if live:
            RenderManager.StartRender('liveRender', node=startNode)
        else:
            RenderManager.StartRender('previewRender', node=startNode)

    elif operationname == 'update crop':
        usecrop = objs[1]
        # cropregion is xmin, ymin, xmax, ymax
        cropregion = objs[2]
        # try with the render start node
        renderSettings = NodegraphAPI.GetNode(objs[3])

        if renderSettings.getType() != 'DlSettings' and \
           renderSettings.getType() != 'RenderSettings':
            # found some node 'RenderSettings'
            renderSettings = GetRenderSettings()
            if not renderSettings:
                return

        elif renderSettings.getType() == 'DlSettings':
            # found the node 'RenderSettings'
            children = renderSettings.getChildren()
            for child in children:
                if child.getType() == 'RenderSettings':
                    renderSettings = child
                    break

        else:
            pass

        resValue = renderSettings.getParameter(
            "args.renderSettings.resolution.value")
        resName = resValue.getValue(0)
        # get resolution
        resTable = ResolutionTable.GetResolutionTable()
        resEntry = resTable.findResolutionByName(resName)
        xres = resEntry.xres()
        yres = resEntry.yres()

        # set region of interest (left, bottom, width, height)
        RenderManager.SetRenderROI((cropregion[0]*xres, (1-cropregion[3])*yres, (cropregion[2]-cropregion[0])*xres, (cropregion[3]-cropregion[1])*yres))

        # Enable it if it's not the default one.
        if (cropregion[0] != 0 or cropregion[1] != 0 or
            cropregion[2] != 1 or cropregion[3] != 1):
            RenderManager.SetRenderUseROI(True)
        else:
            RenderManager.SetRenderUseROI(False)

    elif operationname == 'select layer':
        gaffername = objs[1]
        objectname = objs[2]

        gaffer = NodegraphAPI.GetNode(gaffername)

        # Select GafferThree
        if not NodegraphAPI.IsNodeEdited(gaffer):
            editedNodes = NodegraphAPI.GetAllEditedNodes()
            for n in editedNodes:
                NodegraphAPI.SetNodeEdited(n, False)

            NodegraphAPI.SetNodeEdited(gaffer, True)

        # Select light in the GafferThree UI
        # Get all the UI's
        parametersTab = UI4.App.Tabs.FindTopTab('Parameters')
        gafferEditors = [baseEditor
                         for baseEditor in parametersTab.findChildren(
                             PackageSuperToolAPI.BaseEditor.BaseEditor)
                         if baseEditor.__class__.__name__ == 'GafferEditor']

        # Find the UI that we need
        for gafferEditor in gafferEditors:
            try:
                node = gafferEditor.getMainNode()
                if node.getName() == gaffername:
                    # Get QT widget and select
                    sceneGraphView = gafferEditor.getSceneGraphView()
                    sceneGraphView.selectLocations([objectname])
                    sceneGraphView.updateWidget()
                    break
            except:
                pass

    elif operationname == 'scale layer intensity':
        if len(objs) < 7:
            # Not enough parameters
            return

        gaffername = objs[1]
        objectname = objs[2]

        gaffer = NodegraphAPI.GetNode(gaffername)

        objects = objs[3]
        attributes = objs[4]
        values = objs[5]

        if len(objects) != len(attributes) and len(values) != len(attributes):
            return

        factor = objs[6]

        for i, obj in enumerate(objects):
            attribute = attributes[i]
            value = values[i]

            lightpackage = gaffer.getPackageForPath(obj)
            lightmaterial = lightpackage.getMaterialNode()

            # Enable editing of intensity
            intensity = lightmaterial.getParameter(
                    "shaders." + attribute + ".enable")
            if intensity:
                intensity.setValue(True, 0)

            intensity = lightmaterial.getParameter(
                    "shaders." + attribute + ".value")
            if intensity:
                intensity.setValue(factor * value, 0)

    elif operationname == 'update layer filter':
        if len(objs) < 7:
            # Not enough parameters
            return

        gaffername = objs[1]
        objectname = objs[2]

        gaffer = NodegraphAPI.GetNode(gaffername)

        objects = objs[3]
        color_attributes = objs[4]
        color_values = objs[5]

        if len(objects) != len(color_attributes) and len(color_values) != len(color_attributes):
            return

        colorMult = objs[6]

        for i, obj in enumerate(objects):
            color_attribute = color_attributes[i]
            color_value = color_values[i]

            lightpackage = gaffer.getPackageForPath(obj)
            lightmaterial = lightpackage.getMaterialNode()

            # Enable editing of color
            colorEnable = lightmaterial.getParameter(
                    "shaders." + color_attribute + ".enable")
            if colorEnable:
                colorEnable.setValue(True, 0)

            colorValue = lightmaterial.getParameter(
                    "shaders." + color_attribute + ".value")
            if colorValue:
                colorValue.getChildByIndex(0).setValue(colorMult[0] * color_value[0], 0)
                colorValue.getChildByIndex(1).setValue(colorMult[1] * color_value[1], 0)
                colorValue.getChildByIndex(2).setValue(colorMult[2] * color_value[2], 0)


# Here goes the actual server code.
import json
import os
import select
import socket
import struct
import threading
from Katana import Callbacks

# List of pending edits to be applied in main thread.
_pending_edits = []
_edits_mutex = threading.Lock()
def EditIdleHandler(event, *args, **kwargs):
    """
    Idle event handler which applies scene edits received by the feedback
    server. Its purpose is to make sure the interaction with Katana APIs
    happens in the main thread. Otherwise, BAD STUFF happens.
    """
    with _edits_mutex:
        while len(_pending_edits) > 0:
            EditScene(_pending_edits.pop())

class FeedbackConnection():
    class TryAgain(Exception):
        pass

    def __init__(self, server_socket):
        self.socket, addr = server_socket.accept()
        self.data_buffer = b''
        self.message_length = None

    def fileno(self):
        return self.socket.fileno()

    def read_full(self, nbytes):
        """
        Read from our socket until we have nbytes. Raises TryAgain if not
        enough bytes are available. Raises EOFError if no data.
        """
        if len(self.data_buffer) < nbytes:
            data = self.socket.recv(nbytes - len(self.data_buffer))
            if not data:
                raise EOFError()
            self.data_buffer += data

        # We might not have enough data yet.
        if len(self.data_buffer) < nbytes:
            raise FeedbackConnection.TryAgain()

        buf = self.data_buffer[:nbytes]
        self.data_buffer = self.data_buffer[nbytes:]
        return buf

    def process_message(self, message):
        # EditScene expects arguments in a list, with a specific order.
        # We replicate that here as a transition from the C++ mess we used to
        # have. We should eventually just send the whole json dict.
        try:
            jm = json.loads(message)
            args = [jm['operation']]
            if jm['operation'] == 'start render':
                args.append( jm['live'] )
                args.append( jm['from'] );
            elif jm['operation'] == 'update crop':
                args.append( jm['usecrop'] )
                args.append( jm['cropregion'] )
                args.append( jm['from'] );
            else:
                layer = jm['layer']
                args.append( layer['gaffer'] )
                args.append( layer['name'] )
                if jm['operation'] == 'scale layer intensity':
                    args.append( layer['objects'] )
                    args.append( layer['attributes'] )
                    args.append( layer['values'] )
                    args.append( jm['scale factor'] )
                elif jm['operation'] == 'update layer filter':
                    args.append( layer['objects'] )
                    args.append( layer['color_attributes'] )
                    args.append( [layer['color_values']] )
                    args.append( jm['color multiplier'] )
                    
        except:
            pass
        else:
            # Queue the edit for application in main thread.
            with _edits_mutex:
                _pending_edits.append(args)
            # Queue event to eventually do something. When the stars align.
            Utils.EventModule.QueueEvent(
                'event_idle', 'DlFeedbackProcessingEvent')

    def handle(self):
        try:
            if self.message_length is None:
                # First read 32-bit big endian message size.
                buf_len = self.read_full(4)
                self.message_length = struct.unpack('>I', buf_len)[0]
                # Return as we have no guarantee we can read more bytes without
                # blocking.
                return

            # We already have a message length so read the message.
            buf_message = self.read_full(self.message_length)
            # Clear for next message.
            self.message_length = None
            # It comes with trailing null byte down the pipe. Remove it.
            message = buf_message[:len(buf_message)-1]
            self.process_message(message)
        except FeedbackConnection.TryAgain:
            # We'll get more data later. Hopefully.
            pass

class FeedbackServer():
    def __init__(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Port 0 to use an arbitrary port
        self.socket.bind((socket.gethostname(), 0))
        self.socket.listen(socket.SOMAXCONN)
        # Put server host and port in the environment. This is by far the
        # easiest way to get that information all the way to the renderboot
        # process.
        ip, port = self.socket.getsockname()
        os.environ['DL_FEEDBACKHOST'] = str(ip);
        os.environ['DL_FEEDBACKPORT'] = str(port);

        self.socket_list = [self]
        self.running = True
        # Use Event handler since RenderManager.StartRender can not be called
        # inside this thread (should be called from the main thread)
        # Put some arbitrary ids (0 seems to be use by katana3.1)
        # Most of the things we do in the feedback are not safe outside the
        # main thread so all the updates are send through this.
        Utils.EventModule.RegisterEventHandler(
            EditIdleHandler, 'event_idle', 'DlFeedbackProcessingEvent')

    def serve(self):
        """
        Main service loop for the feedback server.
        """
        while self.running:
            action_sockets = select.select(self.socket_list, [], [])
            read_sockets = action_sockets[0]
            for s in read_sockets:
                try:
                    s.handle()
                except:
                    self.socket_list.remove(s)
        # Be nice and close everyone's socket.
        for s in self.socket_list:
            s.socket.close()

    def fileno(self):
        return self.socket.fileno()

    def handle(self):
        self.socket_list.append(FeedbackConnection(self.socket))

    def shutdown(self):
        self.running = False
        # Connect to the server's socket to break it out of the select().
        dummy_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        dummy_socket.connect(self.socket.getsockname())
        dummy_socket.close()

def onShutdown( objectHash ):
    """
    Stop the server cleanly so Katana does not hang on shutdown.
    """
    server.shutdown()
    server_thread.join()

# Only start the server when there is a UI. The main reason is that we don't
# get the shutdown callback when katana runs with --shell so shutdown hangs.
if PackageSuperToolAPI.IsUIMode():
    server = FeedbackServer()

    # Start the server in a thread.
    server_thread = threading.Thread(target=server.serve)
    server_thread.start()

    Callbacks.addCallback(Callbacks.Type.onShutdown, onShutdown)

# vim: set softtabstop=4 expandtab shiftwidth=4:
