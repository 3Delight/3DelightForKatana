# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2017                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

from Katana import QT4FormWidgets, UI4, Plugins

from PackageSuperToolAPI import UIDelegate
import PackageSuperToolAPI
import PackageSuperToolAPI.NodeUtils as NU
from PackageSuperToolAPI import Packages

from . import LightBase;

GafferThreeAPI = Plugins.GafferThreeAPI

LightUIDelegate = UIDelegate.GetUIDelegateClassForPackageClass(
    GafferThreeAPI.PackageClasses.LightPackage)
LightEditUIDelegate = UIDelegate.GetUIDelegateClassForPackageClass(
    GafferThreeAPI.PackageClasses.LightEditPackage)

class EnvironmentLightUIDelegate(LightUIDelegate,LightBase.BaseUIDelegate):
    """
    The UI delegate for the EnvironmentLight package.

    This class is responsible for exposing the parameters on each of the
    parameter tabs. This is done by creating parameter policies attached to the
    parameters on the package's nodes. We can also modify the appearance of the
    parameter tabs by modifying the hints dictionaries on the policies.
    """

    def getTabPolicy(self, tabName):
        """
        The main method of a UIDelegate. This is responsible for returning a
        policy instance for each tab. The policy will contain other policies
        that should drive the actual package node's parameters.
        """
        if tabName == "Object":
            return self.__getObjectTabPolicy()
        elif tabName == "Linking":
            return self.__getLinkingTabPolicy()
        else:
            return LightUIDelegate.getTabPolicy(self, tabName)

    def __getObjectTabPolicy(self):
        """
        Returns the widget that should be displayed under the 'Object' tab.
        """
        # Create a root group policy and add some hints on it
        rootPolicy = QT4FormWidgets.PythonGroupPolicy('object')
        rootPolicy.getWidgetHints()['open'] = True
        rootPolicy.getWidgetHints()['hideTitle'] = True

        # Add visibility attributes
        self.addObjectSettingsPolicy(rootPolicy, False)

        # Add transform group
        self.addTransformPolicy(rootPolicy, False)

        # DELETE THIS CODE IN THE FUTURE. It is legacy code to deal with older
        # environment light packages. This node is no longer created.
        # Look for the AttributeSet that defines the camera visibility 
        # attribute. We tagged that node with "visibilityAttrNode" in 
        # AreaLightPackage.py
        #
        packageNode = self.getPackageNode()
        visAttrNode = NU.GetRefNode(packageNode, "visibilityAttrNode")
        visAttr = None

        # Don't assume that the node was found, so we don't break older scenes
        if visAttrNode is not None:
            visAttr = visAttrNode.getParameter("numberValue.i0")

        if visAttr is not None:
            # Add a Visibility group
            visGroupPolicy = QT4FormWidgets.PythonGroupPolicy('Visibility')
            visGroupPolicy.getWidgetHints()['open'] = True

            # Add a checkbox to control the value of the attribute added by the
            # AttributeSet node. Label it "Visible to Camera".
            #
            cameraVisPolicy = FormMaster.CreateParameterPolicy( None, visAttr )
            cameraVisPolicy.getWidgetHints()['widget'] = 'checkBox'
            cameraVisPolicy.getWidgetHints()['label'] = 'Visible to Camera'

            # Add the toggle to the visibility group
            visGroupPolicy.addChildPolicy(cameraVisPolicy)

            # Add the visibility group to the 'object' group/
            rootPolicy.addChildPolicy(visGroupPolicy)
        # END OF OBSOLETE BLOCK

        return rootPolicy

    def __getLinkingTabPolicy(self):
        return LightUIDelegate.GetLightLinkingTabPolicy(
            self.getReferencedNode("node_lightLink_illumination"),
            self.getReferencedNode("node_lightLink_shadow"),
            self.getReferencedNode("node_create") )

class EnvironmentLightEditUIDelegate(LightEditUIDelegate,LightBase.BaseUIDelegate):
    """
    The UI delegate for the EnvironmentLightEdit package.
    """

    def getTabPolicy(self, tabName):
        """
        The main method of a UIDelegate. This is responsible for returning a
        Value Policy for each tab. The Value Policy will contain other policies
        that should drive the actual package node's parameters.
        """
        if tabName == "Object":
            return self.__getObjectTabPolicy()
        else:
            return LightEditUIDelegate.getTabPolicy(self, tabName)

    def __getObjectTabPolicy(self):
        """
        Returns the widget that should be displayed under the 'Object' tab.
        """

        rootPolicy = QT4FormWidgets.PythonGroupPolicy('object')
        rootPolicy.getWidgetHints()['open'] = True
        rootPolicy.getWidgetHints()['hideTitle'] = True

        # Add visibility attributes
        self.addObjectSettingsPolicy(rootPolicy, True)

        # Add transform group
        self.addTransformPolicy(rootPolicy, True)

        return rootPolicy

