# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2017                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

from Katana import NodegraphAPI, Plugins
import PackageSuperToolAPI
import PackageSuperToolAPI.NodeUtils as NU
from PackageSuperToolAPI import Packages
# Only import UI components in UI mode.
if PackageSuperToolAPI.IsUIMode():
    from Katana import QT4FormWidgets, UI4

# Our common base.
from . import GafferItemBase

class BasePackage(GafferItemBase.BasePackage):
    """
    Common code which serves as a base for all our LightFilterPackage and
    LightFilterEditPackage classes.
    """

    @classmethod
    def addCreateNode(cls, packageNode, locationType):
        """
        Create and append a node to create the light location. It is also
        returned so it may be further edited.
        """
        locExpr = cls.getLocationExpression()
        createNode = NodegraphAPI.CreateNode('PrimitiveCreate', packageNode)
        createNode.getParameter('type').setValue(locationType, 0)

        createNodeExtraAttrsParameter = \
            createNode.getParameters().createChildGroup('extraAttrs')
        createNodeExtraAttrsParameter.createChildString(
            '__gafferPackage', '').setExpression('@%s' % packageNode.getName())
        createNode.getParameter('name').setExpression(locExpr)

        # Store the package class as a parameter on the create node
        NU.SetOrCreateDeepScalarParameter(
            createNode.getParameters(), 'extraAttrs.info.gaffer.packageClass',
            cls.__name__)

        NU.AddNodeRef(packageNode, 'create', createNode)
        # First node of the package needs to call WireInlineNodes directly.
        NU.WireInlineNodes(packageNode, (createNode,))
        return createNode

    @classmethod
    def addPostCreateStandardNodes(cls, packageNode):
        """
        Create and append some standard nodes which come after the node used to
        create the light filter location.
        """
        cls.addTypeAndBBoxNodes(packageNode, 'light filter')

class BaseUIDelegate:
    """
    Common code which serves as a base for all our LightFilterUIDelegate classes.
    """

