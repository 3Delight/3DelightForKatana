# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2017                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

from Katana import NodegraphAPI, Plugins
import PackageSuperToolAPI
import PackageSuperToolAPI.NodeUtils as NU
from PackageSuperToolAPI import Packages
import sys
# Only import UI components in UI mode.
if PackageSuperToolAPI.IsUIMode():
    from Katana import QT4FormWidgets, UI4
    # Have not found any nicer way to access this. It's internal code that I
    # really don't want to duplicate.
    ConstraintPolicies = sys.modules[
        Plugins.GafferThreeAPI.__package__ + '.ConstraintPolicies']

# Our common base.
from . import GafferItemBase

class BasePackage(GafferItemBase.BasePackage):
    """
    Common code which serves as a base for all our LightPackage and
    LightEditPackage classes.
    """

    @classmethod
    def setGenericAssignCEL(cls, node):
        """
        Sets a GenericAssign node's CEL to the expression so it refers to the
        light's location. Also does the magic to show upstream values in an
        editing context.
        """
        node.getParameter('CEL').setExpression(cls.getLocationExpression())
        # This hides the CEL field and makes the whole thing correctly show
        # upstream values.
        fc = node.getParameter('args').createChildString('__fixedCEL', '')
        fc.setExpression('=CEL')

    # TODO: I think we could scrap this and call cls.createPackageGroupNode
    # from PackageSuperToolAPI.Packages. It contains the exact same code.
    @classmethod
    def createPackageNode(cls, enclosingNode, locationPath):
        """
        Create the package group node and set it up properly.
        """
        packageNode = NodegraphAPI.CreateNode('Group', enclosingNode)
        packageNode.addOutputPort('out')
        # Add parameter containing the package type and location path to the
        # package node
        NU.AddPackageTypeAndPath(packageNode, cls.__name__, locationPath)
        return packageNode

    @classmethod
    def addMasterMaterialPortNode(cls, packageNode):
        """
        Create a DistantPort node to reference the master material. This must be
        the first thing added to the light package.
        """
        portNode = NodegraphAPI.CreateNode('DistantPort', packageNode)
        portNode.setBypassed(True)
        NU.AddNodeRef(packageNode, 'master', portNode)
        # First node of the package needs to call WireInlineNodes directly.
        NU.WireInlineNodes(packageNode, (portNode,))

    @classmethod
    def addCreateNode(cls, packageNode):
        """
        Create and append a node to create the light location. It is also
        returned so it may be further edited.
        """
        locExpr = cls.getLocationExpression()
        createNode = NodegraphAPI.CreateNode('LightCreate', packageNode)

        createNodeExtraAttrsParameter = \
            createNode.getParameters().createChildGroup('extraAttrs')
        createNodeExtraAttrsParameter.createChildString(
            '__gafferPackage', '').setExpression('@%s' % packageNode.getName())
        createNode.getParameter('name').setExpression(locExpr)
        createNode.addInputPort('masterMaterial')

        # Store the package class as a parameter on the create node
        NU.SetOrCreateDeepScalarParameter(
            createNode.getParameters(), 'extraAttrs.info.gaffer.packageClass',
            cls.__name__)

        NU.AddNodeRef(packageNode, 'create', createNode)
        cls.appendNode(packageNode, False, createNode)
        return createNode

    @classmethod
    def addPostCreateStandardNodes(cls, packageNode):
        """
        Create and append some standard nodes which come after the node used to
        create the light location. They do the following:
        - Set location type to light.
        - Delete the bounding box.
        """
        # Set the type of the package location to "light", so that the Gaffer
        # recognizes it as such
        cls.addTypeAndBBoxNodes(packageNode, 'light')

    @classmethod
    def addMaterialNodes(cls, packageNode, editing, shaderType, shaderName):
        """
        Create and append the nodes needed to set or edit the material on the
        light location.
        """
        matNode = cls.addShaderNode(
            packageNode, editing, shaderType, shaderName)

        if editing:
            return

        locExpr = cls.getLocationExpression()
        # Create the MaterialAssign node
        assignNode = NodegraphAPI.CreateNode('MaterialAssign', packageNode)
        assignNode.getParameter('CEL').setExpression(locExpr)
        assignNode.getParameter('args.materialAssign.enable').setValue(1, 0)
        matExpr = \
            'scenegraphLocationFromNode(getNode(\'' + matNode.getName() + '\'))'
        assignNode.getParameter('args.materialAssign.value').setExpression(
            matExpr)

        cls.appendNode(packageNode, editing, assignNode)

    @classmethod
    def addObjectSettingsNode(cls, packageNode, editing):
        """
        Create and append the DlObjectSettings node used to set camera
        visibility of lights.
        """
        node = NodegraphAPI.CreateNode('DlObjectSettings', packageNode)
        cls.setGenericAssignCEL(node)
        # Fix label. Child parameter __hints is string representation of a
        # python directionary with all the hints in it.
        hintsParam = node.getParameter(
            'args.dlObjectSettings.visibility.camera.__hints')
        hints = eval(hintsParam.getValue(0))
        hints['label'] = 'Visible to Camera'
        hintsParam.setValue(str(hints), 0)
        if not editing:
            # Set invisible to camera locally.
            node.getParameter(
                'args.dlObjectSettings.visibility.camera.value').setValue(0, 0)
            node.getParameter(
                'args.dlObjectSettings.visibility.camera.enable').setValue(1, 0)
        # Change default as invisible. This is for editing, until I figure out
        # how to show the real value.
        node.getParameter(
            'args.dlObjectSettings.visibility.camera.default').setValue(0, 0)
        # Add node reference.
        NU.AddNodeRef(packageNode, 'objectSettingsNode', node)
        cls.appendNode(packageNode, editing, node)

    @classmethod
    def addPruneOpScriptNode(cls, packageNode):
        """
        Create and append an OpScript node needed by the GafferThree for Master
        Material support.
        """
        # Create a prune OpScript that deletes all the locations not having
        # common ancestor with the light location stored in the 'lightPath' user
        # argument. It's necessary for working of the Master Material. If we
        # don't have it, Katana fails in LightPackage.setMasterMaterial
        # This code is from katana's LightPackage.
        node = NodegraphAPI.CreateNode('OpScript', packageNode)
        node.setBypassed(True)
        node.getParameter('applyWhere').setValue('at locations matching CEL', 0)
        nodeName = node.getName()
        getParamExpr = "getParam('%s.user.gafferRootPath')" % nodeName
        node.getParameter('CEL').setExpression(
                "'((%%s %%s//*))' %% (%s, %s)" % (getParamExpr, getParamExpr))
        # Add some user parameters used by the script and CEL expression.
        parameters = node.getParameters()
        userParameters = parameters.createChildGroup('user')
        gafferRootParameter = userParameters.createChildString(
            'gafferRootPath', '')
        rootPackage = cls(packageNode).getMainNode().getRootPackage()
        gafferRootParameterExpr = '=%s/%s' % (
            rootPackage.getPackageNode().getName(),
            NU.GetPackageLocationParameterPath())
        gafferRootParameter.setExpression(gafferRootParameterExpr)
        lightPathParameter = userParameters.createChildString('lightPath', '')
        lightPathParameterExpr = '=^/' + NU.GetPackageLocationParameterPath()
        lightPathParameter.setExpression(lightPathParameterExpr)
        # Grab the script itself straight from the LightPackage base class.
        LightPackage = Plugins.GafferThreeAPI.PackageClasses.LightPackage
        node.getParameter('script.lua').setValue(
            LightPackage._LightPackage__getPruneOpScriptCode(), 0)

        NU.AddNodeRef(packageNode, 'prune_master', node)
        cls.appendNode(packageNode, False, node)

    @classmethod
    def addChildPackageMergeNode(cls, packageNode):
        """
        Create and append the Merge node, for merging in child packages.
        """
        # If we don't have it, Katana fails in GroupPackage.getChildPackages of
        # python/PackageSuperToolAPI/Packages.py when the user changes current
        # material in the GafferThree table.
        node = NodegraphAPI.CreateNode('Merge', packageNode)
        node.addInputPort('i0')
        NU.AddNodeRef(packageNode, 'merge', node)
        cls.appendNode(packageNode, False, node)

    @classmethod
    def addLightLinkingNodes(cls, packageNode, editing):
        """
        Create and append light linking nodes.
        """
        linkingNodes = \
            Packages.LinkingMixin.getLinkingNodes(packageNode, create=True)
        NU.AppendNodes(
            packageNode,
            tuple(linkingNode
                for linkingNode in linkingNodes
                if linkingNode is not None))

    @classmethod
    def addOpScriptNode(cls, packageNode, editing, nodeName, script):
        """
        Create and append an OpScript node with the given name and script.
        """
        node = NodegraphAPI.CreateNode('OpScript', packageNode)
        node.setName(nodeName)
        node.getParameter('CEL').setExpression(cls.getLocationExpression())
        node.getParameter('script.lua').setValue(script, 0)
        cls.appendNode(packageNode, editing, node)
        return node

class BaseUIDelegate:
    """
    Common code which serves as a base for all our LightUIDelegate classes.
    """

    @staticmethod
    def disableStateChange(policy, editing):
        """
        Remove the UI to revert to default (the yellow L box) when not editing.
        """
        def noDisplayState():
            return False
        if not editing:
            policy.shouldDisplayState = noDisplayState

    def addObjectSettingsPolicy(self, parentPolicy, editing):
        """
        Add the UI for camera visibility.
        """
        packageNode = self.getPackageNode()
        attrNode = NU.GetRefNode(packageNode, 'objectSettingsNode')
        if attrNode is None:
            return
        for paramName in (
                'args.dlObjectSettings.visibility.camera',
                'args.dlObjectSettings.emissiontype'):
            param = attrNode.getParameter(paramName)
            policy = UI4.FormMaster.CreateParameterPolicy(parentPolicy, param)
            self.disableStateChange(policy, editing)
            parentPolicy.addChildPolicy(policy)

    def addTransformPolicy(self, parentPolicy, editing):
        """
        Add the UI for the light's transform.
        """
        packageNode = self.getPackageNode()
        transformPolicy = QT4FormWidgets.PythonGroupPolicy('transform')
        transformPolicy.getWidgetHints()['open'] = True

        if not editing:
            # Get the create node in the package, which contains the transform
            # parameter.
            createNode = NU.GetRefNode(packageNode, "create")
            if createNode is None:
                return

            for paramName in (
                'transform.translate', 'transform.rotate', 'transform.scale'):
                policy = UI4.FormMaster.CreateParameterPolicy(
                    transformPolicy, createNode.getParameter(paramName))
                transformPolicy.addChildPolicy(policy)
        else:
            # Get the transform_edit node used to change the transform.
            transformEditNode = NU.GetRefNode(packageNode, "transform_edit")
            if transformEditNode is None:
                return

            # Add these three top-level parameters in TransformEdit to the root
            # policy.
            for paramName in ('action', 'rotationOrder', 'args'):
                parameter = transformEditNode.getParameter(paramName)
                policy = UI4.FormMaster.CreateParameterPolicy(
                    transformPolicy, parameter)
                transformPolicy.addChildPolicy(policy)

        parentPolicy.addChildPolicy(transformPolicy)

    def addAimPolicy(self, parentPolicy, editing):
        """
        Add the UI for the light's aim constraint.
        """
        # This code is from the base LightUIDelegate class. The aim constraint
        # system appears to be fairly complex as it adds the node dynamically
        # the first time it is enabled. There are a bunch of callbacks in the
        # LightPackage base class which are involved. It all seems excessively
        # complex to me but it's already in place so we reuse it.
        package = self.getPackage()
        if not editing:
            ConstraintPolicies.AddConstraintPolicy(
                package, parentPolicy, 'AimConstraint', 'aim',
                [
                    'targetPath.i0',
                    'addToConstraintList',
                    'targetOrigin',
                    'baseAimAxis',
                    'baseUpAxis',
                    'targetUpAxis',
                    'setRelativeTargets'
                ])
        else:
            ConstraintPolicies.AddConstraintEditPolicy(
                package, parentPolicy, 'AimConstraint', 'aim',
                [
                    'action',
                    'constraintIsNew',
                    'removeFromConstraintList',
                    'addToConstraintList',
                    'args'
                ])
