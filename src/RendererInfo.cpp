/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2016                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "RendererInfo.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <cassert>
#include <algorithm>
#include <iterator>

#include <FnAttribute/FnDataBuilder.h>
#include <FnAsset/FnDefaultAssetPlugin.h>
#include <FnLogging/FnLogging.h>

#include "delight.h"
#include "LogReporting.h"
#include "Renderer.h"
#include "utils.h"

RendererInfo::RendererInfo()
{
}

RendererInfo::~RendererInfo()
{
}

FnKat::RendererInfo::RendererInfoBase* RendererInfo::create()
{
    return new RendererInfo();
}

/*
	Configure the render method which is always present for batch rendering.
	Adding debug output support here is what gives an entry in the Debug menu
	when you right click on a node in the node graph.
*/
void RendererInfo::configureBatchRenderMethod(
		Foundry::Katana::RendererInfo::DiskRenderMethod& batchRenderMethod)const
{
    batchRenderMethod.setDebugOutputSupported(true);
    batchRenderMethod.setDebugOutputFileType("nsi");
}

/*
	Add more render methods.

	Adding scene graph debug output support here gives entries in the debug
	menu when you right click on a scene graph location.

	Enabling debug output is required for the debug output from the node graph
	to work when "Render Only Selected Objects" is checked in the scene graph
	tab. That's because disk renders do not support a partial graph (according
	to kanata's doc) so they switch to a preview render.

	The two render methods here are giving rather weird file types so they can
	be identified in the debug menu.
*/
void RendererInfo::fillRenderMethods(
	std::vector<Foundry::Katana::RendererInfo::RenderMethod*>& renderMethods)const
{
	FnKat::RendererInfo::DiskRenderMethod *diskRenderMethod =
		new FnKat::RendererInfo::DiskRenderMethod();

	diskRenderMethod->setDebugOutputSupported(true);
	diskRenderMethod->setSceneGraphDebugOutputSupported(true);
	diskRenderMethod->setDebugOutputFileType("disk.nsi");
	renderMethods.push_back(diskRenderMethod);

	FnKat::RendererInfo::PreviewRenderMethod *previewRenderMethod =
		new FnKat::RendererInfo::PreviewRenderMethod();

	previewRenderMethod->setDebugOutputSupported(true);
	previewRenderMethod->setSceneGraphDebugOutputSupported(true);
	previewRenderMethod->setDebugOutputFileType("preview.nsi");

	renderMethods.push_back(previewRenderMethod);

	FnKat::RendererInfo::LiveRenderMethod *liveRenderMethod =
		new FnKat::RendererInfo::LiveRenderMethod();

	liveRenderMethod->setDebugOutputSupported( true );
	liveRenderMethod->setDebugOutputFileType( "live.nsi" );

	renderMethods.push_back( liveRenderMethod );

}

/*
	Return a list of shaders that match the specified type tags, or all shaders
	if no tags are provided.
*/

void RendererInfo::fillRendererObjectNames(
		std::vector<std::string>& rendererObjectNames,
		const std::string& type,
		const std::vector<std::string>& typeTags) const
{
	if (type != kFnRendererObjectTypeShader)
	{
		return;
	}

	// We need to filter the returned list of shaders based on the provided type
	// tags.
	for( auto cache_item : Utils::GetOSLShaders() )
	{
		// Skip this shader if it has the 'hidden' tag
		if( cache_item.second.HasTag( "hidden" ) )
			continue;

		if( typeTags.size() == 0 )
		{
			rendererObjectNames.push_back( cache_item.first );
		}
		else
		{
			for( unsigned i = 0; i < typeTags.size(); ++i )
			{
				if( cache_item.second.HasTag( typeTags[i] ) )
				{
					rendererObjectNames.push_back( cache_item.first );
					break;
				}
			}
		}
	}
}

void RendererInfo::fillRendererObjectTypes(
		std::vector<std::string>& renderObjectTypes,
		const std::string& type) const
{
    renderObjectTypes.clear();

	if (type == kFnRendererObjectTypeShader)
	{
		renderObjectTypes.push_back( _3DELIGHT_SURFACE_TERMINAL );
		renderObjectTypes.push_back( _3DELIGHT_LIGHT_TERMINAL );
		renderObjectTypes.push_back( "LightFilter" );
		renderObjectTypes.push_back( _3DELIGHT_DISPLACEMENT_TERMINAL );
		renderObjectTypes.push_back( _3DELIGHT_ENVIRONMENT_TERMINAL );
		renderObjectTypes.push_back( _3DELIGHT_VOLUME_TERMINAL );
	}
	else if (type == kFnRendererObjectTypeRenderOutput)
	{
		renderObjectTypes.push_back(kFnRendererOutputTypeColor);
	}
	else if (type == kFnRendererObjectTypeOutputChannelCustomParam)
	{
		renderObjectTypes.push_back("exposure");
	}
}

void RendererInfo::fillRendererShaderTypeTags(std::vector<std::string>& shaderTypeTags,
                                                   const std::string& shaderType) const
{
	// The tag will be equivalent to the actual shader type, except for the
	// "coshader" case, for which no tags will be added to allow any shading
	// node to be connected to it.
	shaderTypeTags.clear();

	if (shaderType != "coshader")
	{
		shaderTypeTags.push_back(shaderType);
	}
}

std::string RendererInfo::getRendererCoshaderType() const
{
	return "";
}

std::string RendererInfo::getRegisteredRendererName() const
{
    return _3DELIGHT_RENDERER_NAME;
}

std::string RendererInfo::getRegisteredRendererVersion() const
{
	return DlGetVersionString();
}

bool RendererInfo::isPresetLocalFileNeeded(const std::string &outputType) const
{
    if (outputType == OutputTypePtc)
    {
        return true;
    }

    return FnKat::RendererInfo::RendererInfoBase::isPresetLocalFileNeeded(outputType);
}

bool RendererInfo::isPolymeshFacesetSplittingEnabled() const
{
    return false;
}

bool RendererInfo::isNodeTypeSupported(const std::string &nodeType) const
{
    if (nodeType == "ShadingNode")
    {
        return true;
    }
    else
    {
        return false;
    }
}

void RendererInfo::fillShaderInputNames(
		std::vector<std::string>& shaderInputNames,
		const std::string& shaderName) const
{
	const DlShaderInfo& shader =
		Utils::GetOSLShaders().getShaderInfo(shaderName);

	for( size_t i=0; i<shader.nparams(); i++)
	{
		const DlShaderInfo::Parameter* parameter = shader.getparam(i);

		if( parameter->isoutput )
		{
			continue;
		}

		shaderInputNames.push_back(Utils::GetParameterName(parameter));
	}
}

void RendererInfo::fillShaderInputTags(
		std::vector<std::string>& shaderInputTags,
		const std::string& shaderName,
		const std::string& inputName) const
{
	shaderInputTags.clear();

	/* Don't want to use the .args files, so we extract type info from
	   OSL API. */
	const DlShaderInfo& shader =
		Utils::GetOSLShaders().getShaderInfo(shaderName);
	const DlShaderInfo::Parameter* parameter =
		Utils::GetParam(shader, inputName);

	if( parameter->type.IsOneFloat() )
	{
		shaderInputTags.push_back( "float" );
	}
	else if( parameter->type.IsOneColor() )
	{
		shaderInputTags.push_back( "color or point or vector or normal" );
	}
	else if( parameter->type.IsOneString() )
	{
		shaderInputTags.push_back( "string" );
	}
	else if( parameter->type.IsOneInteger() )
	{
		shaderInputTags.push_back( "int" );
	}
	else if( parameter->type.IsOnePoint() )
	{
		shaderInputTags.push_back( "color or point or vector or normal" );
	}
	else if( parameter->type.IsOneVector() )
	{
		shaderInputTags.push_back( "color or point or vector or normal" );
	}
	else if( parameter->type.IsOneNormal() )
	{
		shaderInputTags.push_back( "color or point or vector or normal" );
	}
	else if( parameter->type.IsOneMatrix() )
	{
		shaderInputTags.push_back( "matrix" );
	}
	else if( Utils::IsOSLTypeFloatArray( parameter->type ) )
	{
		shaderInputTags.push_back( "float or array_float" );
	}
	else if( Utils::IsOSLTypeColorArray( parameter->type ) )
	{
		shaderInputTags.push_back( "color or array_color" );
	}
}

void RendererInfo::fillShaderOutputNames(
		std::vector<std::string>& shaderOutputNames,
		const std::string& shaderName) const
{
	const DlShaderInfo& shader =
		Utils::GetOSLShaders().getShaderInfo(shaderName);

	for( size_t i=0; i<shader.nparams(); i++)
	{
		const DlShaderInfo::Parameter* parameter = shader.getparam(i);

		if( !parameter->isoutput )
		{
			continue;
		}

		shaderOutputNames.push_back(Utils::GetParameterName(parameter));
	}

	/* Add all the input names. We should have an ability to have input->input
	   connections like in Maya. */
	fillShaderInputNames( shaderOutputNames, shaderName );
}

void RendererInfo::fillShaderOutputTags(
		std::vector<std::string>& shaderOutputTags,
		const std::string& shaderName,
		const std::string& outputName) const
{
	shaderOutputTags.clear();

	/* Don't want to use the .args files, so we extract type info from
	   OSL API. */
	const DlShaderInfo& shader =
		Utils::GetOSLShaders().getShaderInfo(shaderName);
	const DlShaderInfo::Parameter* parameter =
		Utils::GetParam(shader, outputName);

	if( parameter->isoutput )
	{
		const OSLShaderInfo& shaderInfo =
			Utils::GetOSLShaders().getShaderInfo(shaderName);

		if( shaderInfo.HasTag( "surface" ) && parameter->isclosure )
 		{
			shaderOutputTags.push_back( _3DELIGHT_SURFACE_TERMINAL );
		}
		else if( shaderInfo.HasTag( "displacement" ) )
 		{
			shaderOutputTags.push_back( _3DELIGHT_DISPLACEMENT_TERMINAL );
		}
		else if( shaderInfo.HasTag( "volume" ) )
		{
			shaderOutputTags.push_back( _3DELIGHT_VOLUME_TERMINAL );
		}
	}

	if( parameter->type.IsOneFloat() )
	{
		shaderOutputTags.push_back( "float" );
	}
	else if( parameter->type.IsOneColor() )
	{
		shaderOutputTags.push_back( "color" );
	}
	else if( parameter->type.IsOneString() )
	{
		shaderOutputTags.push_back( "string" );
	}
	else if( parameter->type.IsOneInteger() )
	{
		shaderOutputTags.push_back( "int" );
	}
	else if( parameter->type.IsOnePoint() )
	{
		shaderOutputTags.push_back( "point" );
	}
	else if( parameter->type.IsOneVector() )
	{
		shaderOutputTags.push_back( "vector" );
	}
	else if( parameter->type.IsOneNormal() )
	{
		shaderOutputTags.push_back( "normal" );
	}
	else if( parameter->type.IsOneMatrix() )
	{
		shaderOutputTags.push_back( "matrix" );
	}
	else if( Utils::IsOSLTypeFloatArray( parameter->type ) )
	{
		shaderOutputTags.push_back( "array_float" );
	}
	else if( Utils::IsOSLTypeColorArray( parameter->type ) )
	{
		shaderOutputTags.push_back( "array_color" );
	}
}

bool RendererInfo::buildRendererObjectInfo(
		FnAttribute::GroupBuilder& rendererObjectInfo,
		const std::string& name,
		const std::string& type,
		const FnAttribute::GroupAttribute inputAttr) const
{
	if( type == kFnRendererObjectTypeShader )
	{
		const OSLShaderInfo& shader =
			Utils::GetOSLShaders().getShaderInfo(name);

		std::set<std::string> typeTags;
		typeTags.insert( _3DELIGHT_SHADER );
		FnKat::Attribute containerHintsAttr;
		configureBasicRenderObjectInfo(
				rendererObjectInfo,
				type,
				std::vector<std::string>(typeTags.begin(), typeTags.end()),
				name,
				name,
				kFnRendererObjectValueTypeUnknown,
				containerHintsAttr);

		/* Define lists of attribute names of shader parameters to collect names
		   of actual shader parameter attributes for shader parameter mapping
		   after the following loop */
		std::vector<std::string> color_parameters;
		std::vector<std::string> intensity_parameters;
		std::vector<std::string> exposure_parameters;

		/* TODO: We should distinguish the shader type and fill the shader
		   parameter mapping only for lights. But now all the surface shaders
		   can be lights. So all the shaders have the mapping of the light
		   shader. We need it for GafferThree.
		   This comment is no longer quite true but I don't understand it
		   enough to fix it. */
		std::string shader_terminal;
		if( shader.HasTag( "environment" ) )
		{
			shader_terminal =
				_3DELIGHT_RENDERER_NAME _3DELIGHT_ENVIRONMENT_TERMINAL;
		}
		else
		{
			shader_terminal = _3DELIGHT_RENDERER_NAME _3DELIGHT_LIGHT_TERMINAL;
		}

		setShaderParameterMapping(
			rendererObjectInfo,
			"shader",
			shader_terminal + _3DELIGHT_SHADER);
		const std::string parameters_prefix =
			shader_terminal + "Params.";

		for( size_t i=0; i<shader.nparams(); i++)
		{
			const DlShaderInfo::Parameter* parameter = shader.getparam(i);

			BuildSingleShaderParameter(
					parameter,
					parameters_prefix,
					rendererObjectInfo,
					color_parameters,
					intensity_parameters,
					exposure_parameters );
		}

		/* Set up mapping of shader parameter meta names to actual attribute
		   names */
		if(color_parameters.size() > 0)
		{
			setShaderParameterMapping(
					rendererObjectInfo, "color", color_parameters);
		}
		if(intensity_parameters.size() > 0)
		{
			setShaderParameterMapping(
					rendererObjectInfo, "intensity", intensity_parameters);
		}
		if(exposure_parameters.size() > 0)
		{
			setShaderParameterMapping(
					rendererObjectInfo, "exposure", exposure_parameters);
		}

		return true;
	}
	else if (type == kFnRendererObjectTypeRenderOutput)
	{
		std::set<std::string> typeTags;
		FnKat::Attribute containerHintsAttr;
		configureBasicRenderObjectInfo(
			rendererObjectInfo,
			type,
			std::vector<std::string>(typeTags.begin(), typeTags.end()),
			name,
			name,
			kFnRendererObjectValueTypeUnknown,
			containerHintsAttr);

		/* Parameters */
		EnumPairVector enums;
		FnAttribute::GroupBuilder typeGroupBuilder;

		/* CameraName. This attribute name is known by Katana and read into
		   FnKat::Render::RenderSettings::RenderOutput */
		addRenderObjectParam(
			rendererObjectInfo,
			"cameraName",
			kFnRendererObjectValueTypeString,
			0,
			FnAttribute::StringAttribute(""),
			typeGroupBuilder.build(),
			enums);

		/* LightSet */
		addRenderObjectParam(
			rendererObjectInfo,
			"lightSet",
			kFnRendererObjectValueTypeString,
			0,
			FnAttribute::StringAttribute(""),
			typeGroupBuilder.build(),
			enums);

		/* layerType */
		FnAttribute::GroupBuilder layertype_hints;
		layertype_hints.set("widget", FnAttribute::StringAttribute("popup"));
		layertype_hints.set(
			"options",
			FnAttribute::StringAttribute("color|scalar|vector|quad"));

		addRenderObjectParam(
			rendererObjectInfo,
			"layerType",
			kFnRendererObjectValueTypeString,
			0,
			FnAttribute::StringAttribute("color"),
			layertype_hints.build(),
			enums);

		/* variableSource */
		FnAttribute::GroupBuilder source_hints;
		source_hints.set("widget", FnAttribute::StringAttribute("popup"));
		source_hints.set(
			"options",
			FnAttribute::StringAttribute("shader|attribute|builtin"));

		addRenderObjectParam(
			rendererObjectInfo,
			"variableSource",
			kFnRendererObjectValueTypeString,
			0,
			FnAttribute::StringAttribute("shader"),
			source_hints.build(),
			enums);

		/* withAlpha */
		FnAttribute::GroupBuilder withalpha_hints;
		withalpha_hints.set("widget", FnAttribute::StringAttribute("checkBox"));

		addRenderObjectParam(
			rendererObjectInfo,
			"withAlpha",
			kFnRendererObjectValueTypeInt,
			0,
			FnAttribute::IntAttribute(0),
			withalpha_hints.build(),
			enums);

		/* driver */
		FnAttribute::GroupBuilder driver_hints;
		driver_hints.set("widget", FnAttribute::StringAttribute("popup"));
		driver_hints.set(
			"options",
			FnAttribute::StringAttribute(
				"tiff|exr|deepexr|deepalphaexr|dwaaexr|deepalphadwaaexr"));

		addRenderObjectParam(
			rendererObjectInfo,
			"driver",
			kFnRendererObjectValueTypeString,
			0,
			FnAttribute::StringAttribute("exr"),
			driver_hints.build(),
			enums);

		return true;
	}

	return false;
}

/*
	Answer from Nick Hutchinson about assertion when using FnConfig::Config:

	If you're implementing a plug-in (e.g. a Renderer or RendererInfo plug-in)
	that uses the FnConfig plug-in module, you'll need to ensure that this
	module is initialised. (This isn't specific to FnConfig, but the default
	implementations of setHost() will already initialise commonly used plug-in
	modules for you so you might not have hit this issue before.)

	You can initialise additional plug-in modules by overriding the setHost()
	static function in your RenderBase or RendererInfoBase subclass, calling
	the base implementation, then calling FnConfig::Config::setHost().
*/
FnPlugStatus RendererInfo::setHost(FnPluginHost* host)
{
    FnPlugStatus status = RendererInfoBase::setHost(host);
    if (status != FnPluginStatusOK)
        return status;

    return FnConfig::Config::setHost(host);
}

/*
	This adds a terminal op which is a script (lua OpScript).

	Note that unlike the OpScript node, the OpScript Op does not have a CEL
	field or any kind of filtering. This needs to be written into the script.

	The other major difference with an OpScript node is that these run after
	the implicit resolvers, so the attributes they see are different in a few
	cases.

	Otherwise, an OpScript node is a good way to prototype such a script.
*/
void RendererInfo::AddOpScript(
	const char *i_script,
	OpDefinitionQueue &terminalOps )
{
	FnAttribute::GroupBuilder opArgs;
	opArgs.set( "script", FnAttribute::StringAttribute( i_script ) );

	terminalOps.push_back( std::make_pair( "OpScript.Lua", opArgs.build() ) );
}

/*
	This adds a terminal op which is a script to make the 'path' attribute of a
	light in a light list local when the 'enable' attribute is local. The main
	purpose is to get that value in live updates. But it also lets us avoid
	having to do global attribute lookups in a regular export.
*/
void RendererInfo::AddOpLocalizeLightListPath(
	OpDefinitionQueue &terminalOps )
{
	AddOpScript( R"DONE(
-- Go through locally set light linking and localize the path value.
local lightList = Interface.GetAttr( 'lightList' )
if lightList ~= nil then
    local nLights = lightList:getNumberOfChildren()
    for i = 0, nLights-1 do
        local lightGrp = lightList:getChildByIndex( i )
        local enable = lightGrp:getChildByName( 'enable' )
        if enable ~= nil then
            local pathAttrName = 'lightList.' .. lightList:getChildName( i ) .. '.path'
            Interface.SetAttr( pathAttrName, Interface.GetGlobalAttr( pathAttrName ) )
        end
    end
end
)DONE"
	, terminalOps );
}

/*
	This adds a terminal op which is a script to gather basic information about
	a light's parents, up to the root, and package it in an attribute structure
	identical to how live updates are sent from Katana. The goal is so that an
	update sent for a light location alone will work even if the parent
	locations were never exported. To that end, we add two fake updates per
	scene graph location:
	- One to create the group/rig/etc.
	- One to apply the transform, if any.

	This is coordinated with code in ExportLightLocation() to actually apply
	those fake updates (more comments there). And with the live render filter
	for light locations to include the dl_light_parents attribute.
*/
void RendererInfo::AddOpLightGatherParents( OpDefinitionQueue &terminalOps )
{
	AddOpScript( R"DONE(
-- This only runs on lights.
if Interface.GetInputLocationType() ~= 'light' then
    return
end

-- Build groups structured like live updates to package a light's parents.
local path = Interface.GetOutputLocationPath()
path = PathUtils.GetLocationParent(path)
local gb = GroupBuilder()
while path ~= '/root' do
    local ident = PathUtils.MakeSafeIdentifier(path)
    -- A fake update for the transform, if present.
    local xform = Interface.GetAttr('xform', path)
    if xform ~= nil then
        local xform_grp = GroupAttribute({
            {'location', StringAttribute(path)},
            {'type', StringAttribute('transform')},
            {'attributes', GroupAttribute({{'xform', xform}}, false)}},
            false)
        gb:set('xform_' .. ident, xform_grp)
    end
    -- A fake update to add the group.
    local group_grp = GroupAttribute({
        {'location', StringAttribute(path)},
        {'type', StringAttribute(Interface.GetInputLocationType(path))},
        {'attributes', GroupAttribute({}, false)}},
        false)
    gb:set(ident, group_grp)

    path = PathUtils.GetLocationParent(path)
end
Interface.SetAttr('dl_light_parents', gb:build())
)DONE"
	, terminalOps );
}

/*
	This adds a terminal op which is a script to store the information needed
	for light visibility in a single group on the light. It gets:
	- The initial state from the light list on /root/world
	- The mute setting from info.light (added by implicit resolvers).
*/
void RendererInfo::AddOpLightLocalState( OpDefinitionQueue &terminalOps )
{
	AddOpScript( R"DONE(
-- This only runs on lights.
if Interface.GetInputLocationType() ~= 'light' then
    return
end

-- Attribute name in light list.
local light_path = Interface.GetOutputLocationPath()
local light_attr = string.sub( PathUtils.MakeSafeIdentifier( light_path ), 2 )

-- Check in the global light list if it is enabled by default.
local enable_attr = Interface.GetGlobalAttr( 'lightList.' .. light_attr .. '.enable', '/root/world' )
if enable_attr ~= nil then
    Interface.SetAttr( 'info.dl_light.enable', enable_attr )
end

-- Also fetch the mute information.
local muteState_attr = Interface.GetAttr( 'info.light.muteState' )
if  muteState_attr ~= nil then
    if muteState_attr:getValue() ~= 'muteEmpty' then
        Interface.SetAttr( 'info.dl_light.mute', IntAttribute( 1 ) )
    end
end
)DONE"
	, terminalOps );
}

/*
	This adds a terminal op which is a script to gather light filters below a
	light location and copy their material to the light. The export code knows
	to export them as extra shaders connected to the light's material. This
	conveniently 'just works' with live render too.

	It handles filter references by copying the referenced filter.

	It processes mute/solo on both filters and references. This essentially
	means ignoring muted filters.

	TODO: Improve mute/solo behavior of referenced filters. They are not
	included in a solo light unless explicitly made solo too, because solo
	mutes everything else by default. So I think we should only handle local
	mute for referenced filters.
*/
void RendererInfo::AddOpGatherLightFilters( OpDefinitionQueue &terminalOps )
{
	AddOpScript( R"DONE(
-- This only runs on lights.
if Interface.GetInputLocationType() ~= 'light' then
    return
end
-- Function to check mute state.
function is_muted( location )
    local muteState_attr = Interface.GetAttr( 'info.light.muteState', location )
    if  muteState_attr ~= nil then
        if muteState_attr:getValue() ~= 'muteEmpty' then
            return true
        end
    end
    return false
end
-- Check all children location of the light for filters or references.
local children = Interface.GetPotentialChildren():getNearestSample(0)
for i = 1, #children do
    local filter_location
    local loc_type = Interface.GetInputLocationType( children[i] )
    if loc_type == 'light filter' then
        filter_location = children[i]
    elseif loc_type == 'light filter reference' then
        local ref_path_attr = Interface.GetAttr( 'referencePath', children[i] )
        if ref_path_attr ~= nil and not is_muted( children[i] ) then
            filter_location = ref_path_attr:getValue( '', false )
            -- Check that the ref actually points to a filter.
            if Interface.GetInputLocationType( filter_location ) ~= 'light filter' then
                filter_location = nil
            end
        end
    end
    -- Copy the filter's material.
    if filter_location ~= nil and not is_muted( filter_location ) then
        local filter_material = Interface.GetAttr( 'material', filter_location )
        if filter_material ~= nil then
            local dest_mat = 'material.filters.' .. children[i]
            Interface.SetAttr( dest_mat, filter_material )
            -- Set the filter's filterCoordinateSystem parameter to its location.
            Interface.SetAttr(
                dest_mat .. '.dlLightFilterParams.filterCoordinateSystem',
                StringAttribute( Interface.GetAbsInputLocationPath( filter_location ) ) )
        end
    end
end
)DONE"
	, terminalOps );
}

/*
	This adds a terminal op which is a script to expand the CEL field of the
	incandescence light. The export code (ExportLightLocation) needs a full
	list of locations and we can't do this conversion when exporting because of
	live updates which don't have access to the whole scene.
*/
void RendererInfo::AddOpIncandescenceLightCEL( OpDefinitionQueue &terminalOps )
{
	AddOpScript( R"DONE(
-- This only runs on lights.
if Interface.GetInputLocationType() ~= 'light' then
    return
end
-- Only on incandescence lights.
local package_attr = Interface.GetAttr( 'info.gaffer.packageClass' )
if package_attr == nil or package_attr:getValue() ~= 'IncandescenceLightPackage' then
    return
end
local sourceMesh_attr = Interface.GetAttr( 'geometry.sourceMesh' )
if sourceMesh_attr == nil then
    return
end
local sourceMesh = sourceMesh_attr:getValue()

-- Function to turn CEL into list of locations.
function ResolveCEL( cel, location, result )
    local matchesLoc, matchesChild = InterfaceUtils.MatchesCEL( cel, location )
    -- If location matches, no need to check its children because of NSI inheritance.
    if matchesLoc then
        table.insert( result, location )
        return
    end
    -- Stop traversal for performance.
    if not matchesChild then
        return
    end
    -- May potentially match some child. Recurse.
    local children = Interface.GetPotentialChildren( location ):getNearestSample(0)
    for i = 1, #children do
        ResolveCEL( cel, location .. '/' .. children[i], result )
    end
end

local resolved = {}
ResolveCEL( sourceMesh, '/root', resolved )
Interface.SetAttr( 'geometry.sourceMesh', StringAttribute( table.concat( resolved, ' ' ) ) )
)DONE"
	, terminalOps );
}

/*
	This adds a terminal op which is a script to remove the 'material.layout'
	attribute group. This is needed in live render to avoid bogus updates when
	nodes a moved around in a NetworkMaterialCreate group. Note that this
	exists on material locations but also on geometry where the material is
	assigned, after implicit resolvers have run.

	There is no need to use this in a regular (not live) render.
*/
void RendererInfo::AddOpNoMaterialLayoutLive( OpDefinitionQueue &terminalOps )
{
	AddOpScript( R"DONE(
Interface.DeleteAttr('material.layout')
)DONE"
	, terminalOps );
}

/*
	This adds a terminal op which is a script to set the 'material' attribute
	of child materials locally. These are created with Material in 'create
	child material' mode and normally just inherit the parent material's
	attributes. This is bad for two reasons:
	- Our export code does not read global attributes.
	- The inherited (global) attributes do not get sent with live updates.

	Although we really only need material.dlSurfaceShader,
	material.dlDisplacementShader, etc there's not much else below material so
	I set the whole thing.
*/
void RendererInfo::AddOpLocalizeMaterial( OpDefinitionQueue &terminalOps )
{
	AddOpScript( R"DONE(
-- This only runs on child materials.
if Interface.GetInputLocationType() ~= 'material' or
   Interface.GetInputLocationType('..') ~= 'material' then
    return
end
local material = Interface.GetGlobalAttr( 'material' )
Interface.SetAttr( 'material', material )
)DONE"
	, terminalOps );
}

/*
	This adds a terminal op which will decide for the exporter when to use the
	local definition of a material.

	Our exporter usually exports materials separately (from /root/materials/)
	and then uses the materialAssign attribute to decide which material to
	connect to which geometry. This is more efficient as we only have one copy
	of the shading network to assemble and optimize (by OSL/llvm).

	However, that's the higher level input from the katana recipe. The
	MaterialResolve runs before the renderer sees the scene and puts a local
	copy of each material where it is assigned. It also applies any material
	overrides to that local copy (a Material node in 'override material' mode,
	applied to a group/geo). So if we want material overrides, we pretty much
	have to use the local copy or find some other way to handle the overrides
	ourselves. Note that a material override will even add a local copy of the
	material if it is inherited from a parent location.

	Another case is with look files which store baked local material copies.
	When that is reloaded, the original material might not be present at all in
	the scene. So we have no choice but to use the local material if we are to
	render the good image. That's why the script checks materialAssign and will
	use the local copy if the location it points to does not exist.

	Yet another case is the use of {attr:xxx} syntax in string attributes. This
	is resolved by the MaterialResolve and causes the local copy of the
	material to differ from the source. There is unfortunately no nice way for
	us to know this which is why we fall back on comparing material attribute
	hashes. We don't compare 'material' itself as its group inherit flag is
	different on the local copy. Another way would be to use a previous
	terminal script to flag materials with '{attr' in them. But comparing in
	here is easier and, assuming Katana attribute hashes are reasonably
	efficient, just as fast. It will also catch *any* difference in the
	material, in case there is some other feature I don't know about.

	Another case for using local materials is in live render updates. This is
	done to fix a problem with a new feature or Katana which allows changing
	the render working set during live render. This can cause an incoherent
	view of the scene when the filter is disabled, this Op thinks the source
	material is available but it is not actually sent to the renderer because
	the live render filter is restricted to only a few locations. We detect the
	live render update case by checking for the 'liveRender' attribute added by
	other terminal Ops. This should normally not be present inside renderboot
	as the live render terminal Ops are not present there. We want to avoid
	using local materials all the time as it is less efficient (many copies are
	output and later deduplicated inside 3Delight, many updates need to be sent
	from Katana to renderboot for every parameter change, etc). The long term
	plan to fix this is for an API to be added so we can ask for missing
	materials, regardless of any working set.

		The above required futher patching. The problem is that attribute
		updates may come for reasons other than the material (visibility, light
		linking, ...). In that case, we'll also get the new material assign
		pointing to the local material but we will not yet have received the
		local material as that has not changed. So, to make sure we receive it,
		the local material is made to include hashes of the attributes we
		monitor together with material assign (see add_hash_to_material()). The
		attributes being hashed are those of the "attributes" live render
		filter.

		For a similar reason, a hash of the local material is added to
		dlObjectSettings. This is so we actually get an attributes update on
		material assign change. Otherwise, we would only get the new local
		material since Katana sees the material assign as unchanged (keeps
		pointing to the local material). Note that the initial export used the
		unpatched material assign, to the real material location. I'd use
		info.materialAssign instead of dlObjectSettings but it's not a group.

	When we decide we want the local material copy, we fiddle with the
	materialAssign attribute so it points to the location itself (so no change
	to the assignment code) and make another copy of the material in the
	dl_local_material attribute. Using a separate attribute makes it easy to
	only export the ones we want and, more importantly, to only get live
	updates for the ones we want. Grep dl_local_material to see where this
	happens.

	The material compare skips the material.info attribute. That's because the
	SGOMaterialChecksum implicit resolver, which is part of Rosetta, adds
	material.info.checksum to the localized materials and causes us to use
	local copies everywhere if we don't ignore it.
*/
void RendererInfo::AddOpUseLocalMaterial( OpDefinitionQueue &terminalOps )
{
	AddOpScript( R"DONE(
-- This does not apply to actual material locations.
if Interface.GetInputLocationType() == 'material' then
    return
end

-- Nothing to do if there is no local material.
local local_material = Interface.GetAttr( 'material' )
if local_material == nil then
    return
end

-- Function to compare materials
function has_same_material( location, material )
    for i = 0, material:getNumberOfChildren()-1 do
        local name = 'material.' .. material:getChildName( i )
        if name ~= 'material.info' then
            local other_att = Interface.GetAttr( name, location )
            if other_att == nil or
               other_att:getHash() ~= material:getChildByIndex( i ):getHash() then
                return false
            end
        end
    end
    return true
end

-- Do nothing if there is no override and the materialAssign is valid.
-- Unless we're in live render update, where we always use a local material.
local material_assign = Interface.GetAttr( 'info.materialAssign' )
local material_override = Interface.GetAttr( 'info.materialOverride' )
local live_render = Interface.GetAttr( 'liveRender' )
if material_assign ~= nil and material_override == nil and live_render == nil then
    local material_location = material_assign:getValue( '', false )
    if Interface.DoesLocationExist( material_location ) and 
       has_same_material( material_location, local_material ) then
        return
    end
end

-- Copy material to dl_local_material and tweak materialAssign.
Interface.SetAttr( 'dl_local_material', local_material )
local loc = Interface.GetOutputLocationPath()
Interface.SetAttr( 'info.materialAssign', StringAttribute( loc ) )

-- Add dependency on attributes so material update is sent along with it.
function add_hash_to_material( attr_name, hash_name )
    local a = Interface.GetAttr( attr_name )
    if a ~= nil then
        Interface.SetAttr('dl_local_material.' .. hash_name, StringAttribute(a:getHash()))
    end
end

if live_render ~= nil then
    add_hash_to_material('dlObjectSettings', 'hash1')
    add_hash_to_material('lightList', 'hash2')
    add_hash_to_material('info.dl_light', 'hash3')
    Interface.SetAttr('dlObjectSettings.hash1', StringAttribute(local_material:getHash()))
end
)DONE"
	, terminalOps );
}

/*
	This adds a terminal op which makes katana's visibility from the
	VisibilityAssign node work. It does so be simply removing scene graph
	locations. While it does not do that as efficiently as pruning (I suspect
	the geometry is still generated), it is still better than renderer
	visibility as the objects are at least not held in memory by 3Delight.

	Note that invisible locations can't all be removed directly as there could
	be different visibility on children. So we only remove locations which are
	not groups of some kind. We don't just check for the presence children as
	that would not correctly remove meshes with face sets. We also do not
	remove face sets as that simply won't do what the user wants.

	As far as Live Render is concerned, visibility changes become object
	add/remove operations so we have nothing extra to do there.
*/
void RendererInfo::AddOpApplyVisibility( OpDefinitionQueue &terminalOps )
{
	AddOpScript( R"DONE(
-- Get visible attribute. If not present, we have nothing to do.
local visible_attr = Interface.GetGlobalAttr( 'visible' )
if visible_attr == nil then
    return
end
-- If visible, nothing to do.
local visible = visible_attr:getValue( 1, false )
if visible ~= 0 then
    return
end
-- Remove if not some kind of group with potentially visible children.
local input_type = Interface.GetInputLocationType()
if
    input_type ~= 'group' and
    input_type ~= 'assembly' and
    input_type ~= 'component' and
    input_type ~= 'faceset' and
    input_type ~= 'rig' then
    Interface.DeleteSelf()
end
)DONE"
	, terminalOps );
}

/*
	This adds a terminal op which adds the liveRender group needed to obtain
	live render updates of dlGlobalStatements and renderSettings on /root. I
	did not do it with the LiveRenderFilter Op as I do not know a way to
	restrict the attribute to /root only with that.

	We also use this Op to enable render working set updates in live render.
	It is done in here because this Op already runs only in live render and
	only on /root, which is exactly what we need.
*/
void RendererInfo::AddOpLiveGlobalStatements( OpDefinitionQueue &terminalOps )
{
	AddOpScript( R"DONE(
-- This only runs on /root.
if Interface.GetOutputLocationPath() ~= '/root' then
    return
end
local dGS = StringAttribute( 'dlGlobalStatements' )
Interface.SetAttr( 'liveRender.dlGlobalStatements.dlGlobalStatements', dGS )
Interface.SetAttr( 'liveRender.dlGlobalStatements.renderSettings', dGS )

Interface.SetAttr( 'liveRenderSettings.allowFollowRenderWorkingSet',
    IntAttribute(1), true )
-- No need to run at all on child locations.
Interface.StopChildTraversal()
)DONE"
	, terminalOps );
}

/*
	This adds terminal Ops to the Op tree when live rendering, allowing changes
	to the scene graph to be made. Mostly, we want to setup live render filters.

	Note that unlike the Renderer part of the plugin which is loaded in
	renderboot, this part is used inside katana so you need to restart katana
	to see changes.

	+ The LiveRenderFilter Op
	This determines which live render updates we'll get in the Renderer and how
	they will be presented. It is built with a list of arguments:
	  - type : string. Only send updates for this location type. Any type if
	    not specified or left empty.
	  - location : string. Only send updates for locations under this one.
	  - typeAlias : string. Replaces the "type" in the update data sent.
	  - attributeNames : Multiple strings. Only send updates if something under
		one of these attributes changes. Also only send these attributes in the
		update. No attributes if the list is empty and I don't know if there's
		a wildcard. It looks like these don't have to be top level attributes
		but I'm not sure about that yet. It also seems to send all attributes
		under every one in the list, so you essentially get an up to date view
		of the location.
	  - collectUpstream : Integer. Will get updates if the attribute is set on
	    any parent location.
	  - exclusions : Multiple strings. Appears to be types which are excluded.
	    Only makes sense if "type" is empty.

	It currently has a bug (TP 266688 - Live rendering does not send update on
	deleted attributes) which makes it not send updates when the last monitored
	attribute is deleted. That's why we also monitor the LiveRenderFilter's own
	attribute in some places. It's a workaround suggested by Christopher
	Beckford, after a first suggestion to use "type" which we need elsewhere.

	On the internal working, Christopher writes:
		This creates a liveRender group attribute on applicable locations. The
		children of this attribute are also group attributes that correspond to
		the type or typeAlias assigned to the LiveRenderFilter op. Each of
		these groups then contains a list of attributes that should be
		monitored and sent together as a batch should any of them change. Since
		this is just setting attributes you could create the same structure
		manually in your scene, or through your own ops.

	+ The LiveAttribute Op
	Christopher Beckford says this:
		The LiveAttribute op is a special case that listens for changes made
		during manipulator interaction, and sets the manipulated values as
		attributes in the scene graph as they happen. It needs to do this
		because manipulators don't store their final manipulated values in a
		node parameter (and so it won't get cooked by geolib) until the mouse
		button has been released.
	So we need it for smooth updates of camera movement and perhaps a few other
	similar things. It does not need any argument that I know of.
	It is also buggy in katana: the transforms it sends for the camera
	manipulator of the viewer tab include parent transforms. It does not
	include them for other manipulators (transform, rotate, etc). This is often
	not visible as cameras usually don't have a parent transform.
	UPDATE: This behavior changed around 3.0v1.001402a. It now sends an out of
	date (from start of drag) local transform, followed by 'origin'
	(RiIdentity) and a matrix which includes the parent transform. So it is
	still absolute but we are at least told that it is.

	If several updates happen at once, they appear to be sent in reverse order
	they are added here. Meaning the last filter in this list is the first
	update you will get. THIS IS NOT TRUE. But I'm leaving the comment until I
	understand why I wrote it as I do remember having to move the 'light'
	update around for things to work. But it no longer appears to be required.
	I have checked 2.5v4 (adding a light does not work), 2.5v5 and 3.0v1.

	I have not retested this recently but from old comments, I suspect that if
	a given attribute is listed in several liveRender groups, we will only
	receive one of the updates when it changes. The filters we use are careful
	to avoid that case anyway. "type" is watched by several filters but they
	apply to different location types so there should be no overlap.
*/
void RendererInfo::fillLiveRenderTerminalOps(
	OpDefinitionQueue& terminalOps,
	const FnAttribute::GroupAttribute& stateArgs) const
{
	/* Smooth camera movement (see function comment). */
	if( !getenv( "NOLIVEATTRIBUTE" ) ) /* bug workaround */
	{
		FnAttribute::GroupBuilder opArgs;
		terminalOps.push_back(
			std::make_pair( "LiveAttribute", opArgs.build() ) );
	}

	/* Options stored on /root */
	AddOpLiveGlobalStatements( terminalOps );

	/*  Camera updates. Geometry and dlCameraSettings.
		Transform will be "transform" below. */
	{
		FnAttribute::GroupBuilder opArgs;
		opArgs.set( "type", FnAttribute::StringAttribute("camera") );
		opArgs.set( "location", FnAttribute::StringAttribute("/root/world") );
		/* Camera's geometry (eg. fov, etc) and settings. */
		std::string attributes[] = {
			"geometry",
			"dlCameraSettings" };
		opArgs.set( "attributeNames",
			FnAttribute::StringAttribute( attributes, 2, 1 ) );

		terminalOps.push_back(
			std::make_pair( "LiveRenderFilter", opArgs.build() ) );
	}

	/* Any transform update. */
	{
		FnAttribute::GroupBuilder opArgs;
		opArgs.set( "typeAlias", FnAttribute::StringAttribute("transform") );
		opArgs.set( "location", FnAttribute::StringAttribute("/root/world") );
		opArgs.set( "attributeNames", FnAttribute::StringAttribute("xform") );

		terminalOps.push_back(
			std::make_pair( "LiveRenderFilter", opArgs.build() ) );
	}

	/* Material updates. */
	{
		FnAttribute::GroupBuilder opArgs;
		opArgs.set( "type", FnAttribute::StringAttribute("material") );
		opArgs.set( "location", FnAttribute::StringAttribute("/root") );
		opArgs.set( "attributeNames",
			FnAttribute::StringAttribute( "material" ) );

		terminalOps.push_back(
			std::make_pair( "LiveRenderFilter", opArgs.build() ) );
	}

	/* Light material updates. */
	{
		FnAttribute::GroupBuilder opArgs;
		opArgs.set( "type", FnAttribute::StringAttribute("light") );
		opArgs.set( "typeAlias", FnAttribute::StringAttribute("material") );
		opArgs.set( "location",
			FnAttribute::StringAttribute("/root/world/lgt") );
		opArgs.set( "attributeNames",
			FnAttribute::StringAttribute( "material" ) );

		terminalOps.push_back(
			std::make_pair( "LiveRenderFilter", opArgs.build() ) );
	}

	/*
		Local material updates. See AddOpUseLocalMaterial().
		We also monitor the attribute added by the LiveRenderFilter to work
		around TP 266688 (see function comment).
	*/
	{
		FnAttribute::GroupBuilder opArgs;
		opArgs.set( "typeAlias",
			FnAttribute::StringAttribute("local_material") );
		opArgs.set( "location",
			FnAttribute::StringAttribute("/root/world") );
		std::string attributes[] = {
			"dl_local_material",
			"liveRender.local_material"
		};
		opArgs.set( "attributeNames",
			FnAttribute::StringAttribute( attributes, 2, 1 ) );

		terminalOps.push_back(
			std::make_pair( "LiveRenderFilter", opArgs.build() ) );
	}

	/*
		Material assignment updates, dlObjectSettings attributes, etc. They
		must be sent together as any update first removes all attributes before
		exporting the new ones. We also monitor the attribute added by the
		LiveRenderFilter to work around TP 266688 (see function comment).
	*/
	{
		FnAttribute::GroupBuilder opArgs;
		opArgs.set( "typeAlias", FnAttribute::StringAttribute("attributes") );
		opArgs.set( "location", FnAttribute::StringAttribute("/root/world") );
		std::string attributes[] = {
			"info.materialAssign",
			"dlObjectSettings",
			"lightList",
			"info.dl_light",
			"liveRender.attributes"
		};
		opArgs.set( "attributeNames",
			FnAttribute::StringAttribute( attributes, 5, 1 ) );

		terminalOps.push_back(
			std::make_pair( "LiveRenderFilter", opArgs.build() ) );
	}

	/*
		Light (adding or removing).

		We grab what is needed to export the light geometry. The material will
		be exported by the light material update above. The transform will be
		exported by the transform update. This means this filter must come
		after the light material update one and the transform one, so we get
		this update first. Otherwise, the other two will connect/edit stuff
		which doesn't exist yet.

		See AddOpLightGatherParents() about dl_light_parents.
	*/
	{
		FnAttribute::GroupBuilder opArgs;
		opArgs.set( "type", FnAttribute::StringAttribute("light") );
		opArgs.set( "location",
			FnAttribute::StringAttribute("/root/world/lgt") );
		std::string attributes[] = {
			"info.gaffer", "geometry", "dl_light_parents" };
		opArgs.set(
			"attributeNames", FnAttribute::StringAttribute(attributes, 3, 1) );

		terminalOps.push_back(
			std::make_pair( "LiveRenderFilter", opArgs.build() ) );
	}

	/*
		Mesh geometry (adding or removing).

		This has ordering requirements similar to lights above: we must get the
		mesh update first so the transform (separate update) is set on a valid
		node and the attributes (separate update) connect to an existing node.
		This must be enforced in Renderer::queueDataUpdates().

		The reason we monitor "type" is to handle the changes between subdmesh
		and polymesh.
	*/
	{
		FnAttribute::GroupBuilder opArgs;
		opArgs.set( "type", FnAttribute::StringAttribute("polymesh") );
		opArgs.set( "location",
			FnAttribute::StringAttribute("/root/world") );
		std::string attributes[] = { "type", "geometry", "abcUser" };
		opArgs.set( "attributeNames",
			FnAttribute::StringAttribute( attributes, 3, 1 ) );

		terminalOps.push_back(
			std::make_pair( "LiveRenderFilter", opArgs.build() ) );
	}

	/*
		Subdivision mesh geometry. Same as polymesh above.
	*/
	{
		FnAttribute::GroupBuilder opArgs;
		opArgs.set( "type", FnAttribute::StringAttribute("subdmesh") );
		opArgs.set( "location",
			FnAttribute::StringAttribute("/root/world") );
		std::string attributes[] = { "type", "geometry", "abcUser" };
		opArgs.set( "attributeNames",
			FnAttribute::StringAttribute( attributes, 3, 1 ) );

		terminalOps.push_back(
			std::make_pair( "LiveRenderFilter", opArgs.build() ) );
	}

	/*
		DlOpenVDB node. Same as polymesh.
	*/
	{
		FnAttribute::GroupBuilder opArgs;
		opArgs.set( "type", FnAttribute::StringAttribute("openvdb") );
		opArgs.set( "location",
			FnAttribute::StringAttribute("/root/world") );
		opArgs.set( "attributeNames",
			FnAttribute::StringAttribute( "openvdb" ) );

		terminalOps.push_back(
			std::make_pair( "LiveRenderFilter", opArgs.build() ) );
	}

	/*
		Yeti geometry. Ordering requirements similar to mesh & lights above.
	*/
	{
		FnAttribute::GroupBuilder opArgs;
		opArgs.set( "type", FnAttribute::StringAttribute("yeti") );
		opArgs.set( "location",
			FnAttribute::StringAttribute("/root/world") );
		opArgs.set( "attributeNames",
			FnAttribute::StringAttribute( "yeti" ) );

		terminalOps.push_back(
			std::make_pair( "LiveRenderFilter", opArgs.build() ) );
	}

	/*
		instance array (what the "usd point instancer" actually does)

		This has everything under geometry and it is treated as a single atomic
		block for now. We could eventually split off the transforms for more
		efficient updates of those. But I'm not sure it is worth the trouble as
		I think it unlikely we will receive an update only to the transforms.
	*/
	{
		FnAttribute::GroupBuilder opArgs;
		opArgs.set( "type", FnAttribute::StringAttribute("instance array") );
		opArgs.set( "location",
			FnAttribute::StringAttribute("/root/world") );
		opArgs.set( "attributeNames",
			FnAttribute::StringAttribute( "geometry" ) );

		terminalOps.push_back(
			std::make_pair( "LiveRenderFilter", opArgs.build() ) );
	}

	/*
		curves
	*/
	{
		FnAttribute::GroupBuilder opArgs;
		opArgs.set( "type", FnAttribute::StringAttribute("curves") );
		opArgs.set( "location",
			FnAttribute::StringAttribute("/root/world") );
		opArgs.set( "attributeNames",
			FnAttribute::StringAttribute( "geometry" ) );

		terminalOps.push_back(
			std::make_pair( "LiveRenderFilter", opArgs.build() ) );
	}

	/*
		pointcloud
	*/
	{
		FnAttribute::GroupBuilder opArgs;
		opArgs.set( "type", FnAttribute::StringAttribute("pointcloud") );
		opArgs.set( "location",
			FnAttribute::StringAttribute("/root/world") );
		opArgs.set( "attributeNames",
			FnAttribute::StringAttribute( "geometry" ) );

		terminalOps.push_back(
			std::make_pair( "LiveRenderFilter", opArgs.build() ) );
	}

	/*
		Group: adding or removing.

		There's not much to monitor on a group. We used to monitor the
		attribute added by the LiveRenderFilter itself (eg. 'liveRender.group')
		but we don't actually need that. We do need the type now because the
		'instance source' groups have special handling and we want to get
		correct updates if the type changes to/from that.

		'assembly' and 'component' are also groups. Many other locations are
		treated like groups so we treat them in a big happy loop here.
	*/
	const char *group_like_locations[] =
	{
		"group", "assembly", "component", "rig", "instance source",
		"usd point instancer"
	};
	for( const char *group_type : group_like_locations )
	{
		FnAttribute::GroupBuilder opArgs;
		opArgs.set( "type", FnAttribute::StringAttribute(group_type) );
		opArgs.set( "location",
			FnAttribute::StringAttribute("/root/world") );
		opArgs.set( "attributeNames",
			FnAttribute::StringAttribute( "type" ) );

		terminalOps.push_back(
			std::make_pair( "LiveRenderFilter", opArgs.build() ) );
	}

	AddOpLocalizeLightListPath( terminalOps );
	AddOpLightGatherParents( terminalOps );
	AddOpLightLocalState( terminalOps );
	AddOpGatherLightFilters( terminalOps );
	AddOpIncandescenceLightCEL( terminalOps );
	AddOpNoMaterialLayoutLive( terminalOps );
	AddOpLocalizeMaterial( terminalOps );
	AddOpUseLocalMaterial( terminalOps );
	AddOpApplyVisibility( terminalOps );
}


void RendererInfo::fillRenderTerminalOps(
	OpDefinitionQueue& terminalOps,
	const FnAttribute::GroupAttribute& stateArgs) const
{
	AddOpLocalizeLightListPath( terminalOps );
	AddOpLightLocalState( terminalOps );
	AddOpGatherLightFilters( terminalOps );
	AddOpIncandescenceLightCEL( terminalOps );
	AddOpLocalizeMaterial( terminalOps );
	AddOpUseLocalMaterial( terminalOps );
	AddOpApplyVisibility( terminalOps );
}

void RendererInfo::flushCaches()
{
#ifndef NDEBUG
	LOGMESSAGE(eLevelInfo, "Clear the cache" );
#endif
    Utils::FlushOSLShaders();
}

void RendererInfo::BuildSingleShaderParameter(
		const DlShaderInfo::Parameter* i_parameter,
		const std::string& i_parameters_prefix,
		FnAttribute::GroupBuilder& io_renderer_object_info,
		std::vector<std::string>& o_color_parameters,
		std::vector<std::string>& o_intensity_parameters,
		std::vector<std::string>& o_exposure_parameters ) const
{
	if( i_parameter->isoutput )
	{
		return;
	}

	const std::string parameter_name =
		Utils::GetParameterName(i_parameter);

	/* Additional parameters. */
	std::string meta_widget;
	std::string meta_label;
	std::string meta_page;
	std::string meta_help;
	std::string meta_options;
	std::string meta_related_to_widget;

	bool meta_fmax_exists = false;
	bool meta_fmin_exists = false;
	bool meta_fsensitivity_exists = false;
	bool meta_fslidercenter_exists = false;
	bool meta_fsliderexponent_exists = false;
	bool meta_fslidermax_exists = false;
	bool meta_fslidermin_exists = false;
	bool meta_imax_exists = false;
	bool meta_imin_exists = false;
	bool meta_isensitivity_exists = false;
	bool meta_islidercenter_exists = false;
	bool meta_isliderexponent_exists = false;
	bool meta_islidermax_exists = false;
	bool meta_islidermin_exists = false;
	float meta_fmax;
	float meta_fmin;
	float meta_fsensitivity;
	float meta_fslidercenter;
	float meta_fsliderexponent;
	float meta_fslidermax;
	float meta_fslidermin;
	int meta_imax;
	int meta_imin;
	int meta_isensitivity;
	int meta_islidercenter;
	int meta_isliderexponent;
	int meta_islidermax;
	int meta_islidermin;
	int meta_slider = 0;

	const DlShaderInfo::Parameter* lockop = NULL;
	const DlShaderInfo::Parameter* lockleft = NULL;
	const DlShaderInfo::Parameter* lockright = NULL;

	/* Check all the meta data. */
	const auto& meta = i_parameter->metadata;

	for(auto it = meta.begin(); it != meta.end(); it++)
	{
		if( it->name == "widget" && it->type.IsOneString() )
		{
			meta_widget = it->sdefault[0].c_str();
		}
		else if( it->name == "label" && it->type.IsOneString() )
		{
			meta_label = it->sdefault[0].c_str();
		}
		else if( it->name == "page" && it->type.IsOneString() )
		{
			meta_page = it->sdefault[0].c_str();
		}
		else if( it->name == "help" && it->type.IsOneString() )
		{
			meta_help = it->sdefault[0].c_str();
		}
		else if( it->name == "options" && it->type.IsOneString() )
		{
			meta_options = it->sdefault[0].c_str();
		}
		else if( it->name == "related_to_widget" && it->type.IsOneString() )
		{
			meta_related_to_widget = it->sdefault[0].c_str();
		}
		else if( it->name == "max" && it->type.IsOneFloat() )
		{
			meta_fmax_exists = true;
			meta_fmax = it->fdefault[0];
		}
		else if( it->name == "min" && it->type.IsOneFloat() )
		{
			meta_fmin_exists = true;
			meta_fmin = it->fdefault[0];
		}
		else if( it->name=="sensitivity" && it->type.IsOneFloat() )
		{
			meta_fsensitivity_exists = true;
			meta_fsensitivity = it->fdefault[0];
		}
		else if( it->name=="slidercenter" && it->type.IsOneFloat() )
		{
			meta_fslidercenter_exists = true;
			meta_fslidercenter = it->fdefault[0];
		}
		else if(it->name=="sliderexponent" && it->type.IsOneFloat())
		{
			meta_fsliderexponent_exists = true;
			meta_fsliderexponent = it->fdefault[0];
		}
		else if(it->name == "slidermax" && it->type.IsOneFloat())
		{
			meta_fslidermax_exists = true;
			meta_fslidermax = it->fdefault[0];
		}
		else if(it->name == "slidermin" && it->type.IsOneFloat())
		{
			meta_fslidermin_exists = true;
			meta_fslidermin = it->fdefault[0];
		}
		else if( it->name == "max" && it->type.IsOneInteger() )
		{
			meta_imax_exists = true;
			meta_imax = it->idefault[0];
		}
		else if( it->name == "min" && it->type.IsOneInteger() )
		{
			meta_imin_exists = true;
			meta_imin = it->idefault[0];
		}
		else if(it->name == "sensitivity" && it->type.IsOneInteger())
		{
			meta_isensitivity_exists = true;
			meta_isensitivity = it->idefault[0];
		}
		else if( it->name=="slidercenter" && it->type.IsOneInteger() )
		{
			meta_islidercenter_exists = true;
			meta_islidercenter = it->idefault[0];
		}
		else if( it->name=="sliderexponent" && it->type.IsOneInteger() )
		{
			meta_isliderexponent_exists = true;
			meta_isliderexponent = it->idefault[0];
		}
		else if( it->name == "slidermax" && it->type.IsOneInteger() )
		{
			meta_islidermax_exists = true;
			meta_islidermax = it->idefault[0];
		}
		else if( it->name == "slidermin" && it->type.IsOneInteger() )
		{
			meta_islidermin_exists = true;
			meta_islidermin = it->idefault[0];
		}
		else if( it->name == "slider" && it->type.IsOneInteger() )
		{
			meta_slider = it->idefault[0];
		}
		else if( it->name == "lock_op" &&
			it->type.IsOneString() )
		{
			lockop = &(*it);
		}
		else if( it->name == "lock_left" &&
			it->type.IsOneString() )
		{
			lockleft = &(*it);
		}
		else if( it->name == "lock_right" &&
			( it->type.IsOneInteger() || it->type.IsOneFloat() ) )
		{
			lockright = &(*it);
		}
	}

	/* UI */
	/* UI hints for the parameter */
	FnAttribute::GroupBuilder hints;
	/* Enumerated value pairs for the parameter */
	EnumPairVector enumValues;

	if( !meta_label.empty() )
	{
		hints.set(
				"label",
				FnAttribute::StringAttribute(meta_label));
	}

	if( !meta_page.empty() )
	{
		hints.set(
				"page",
				FnAttribute::StringAttribute(meta_page));
	}

	if( !meta_help.empty() )
	{
		hints.set( "help", FnAttribute::StringAttribute( meta_help ) );
	}

	/* Setting widget. */
	bool has_widget = true;

	/* All the Katana widgets are in FormMaster/KatanaFactory.py */
	if( meta_widget == "null" ||
		meta_widget == "mapper" ||
		meta_widget == "popup" )
	{
		hints.set(
				"widget",
				FnAttribute::StringAttribute(meta_widget));

		if( !meta_options.empty() )
		{
			hints.set(
					"options",
					FnAttribute::StringAttribute(meta_options));
		}
	}
	else if( meta_widget == "filename" )
	{
		hints.set(
				"widget",
				FnAttribute::StringAttribute("assetIdInput"));
	}
	else if( meta_widget == "newScenegraphLocation" )
	{
		hints.set(
				"widget",
				FnAttribute::StringAttribute("newScenegraphLocation"));
	}
	else
	{
		has_widget = false;
	}

	if( meta_widget != "null" &&
		lockop != NULL &&
		lockleft != NULL &&
		lockright != NULL )
	{
		hints.set( 
			"conditionalLockOps.conditionalLockOp",
			FnAttribute::StringAttribute( lockop->sdefault[0].c_str() ) );

		// FIXME: this assumes the param is in the same page
		std::string paramPath("../");
		paramPath += lockleft->sdefault[0].c_str();

		hints.set( 
			"conditionalLockOps.conditionalLockPath",
			FnAttribute::StringAttribute( paramPath ) );

		if( lockright->type.IsOneInteger() )
		{
			hints.set(
				"conditionalLockOps.conditionalLockValue",
				FnAttribute::IntAttribute( lockright->idefault[0] ) );
		}
		else if( lockright->type.IsOneFloat() )
		{
			hints.set(
				"conditionalLockOps.conditionalLockValue",
				FnAttribute::FloatAttribute( lockright->fdefault[0] ) );			
		}
	}

	if( i_parameter->type.IsOneColor() )
	{
		/* Default */
		FnAttribute::FloatBuilder fb(3);
		fb.push_back(i_parameter->fdefault[0]);
		fb.push_back(i_parameter->fdefault[1]);
		fb.push_back(i_parameter->fdefault[2]);

		/* UI */
		if( !has_widget )
		{
			hints.set(
					"widget",
					FnAttribute::StringAttribute("color"));
		}

		addRenderObjectParam(
				io_renderer_object_info,
				parameter_name,
				kFnRendererObjectValueTypeColor3,
				-1,
				fb.build(),
				hints.build(),
				enumValues );

		/* FIXME: This should be shader metadata. */
		if(
			parameter_name.find("color") != std::string::npos ||
			parameter_name.find("mult") != std::string::npos ||
			parameter_name.find("sky_tint") != std::string::npos )
		{
			o_color_parameters.push_back(
					i_parameters_prefix + parameter_name);
		}
	}
	if( Utils::IsOSLTypeColorArray( i_parameter->type ) )
	{
		int size = i_parameter->type.arraylen;
		if( size < 0 )
		{
			size = static_cast<int>( i_parameter->fdefault.size() / 3 );
		}

		std::string ramp_name;
		if( meta_widget == "colorRamp" || meta_widget == "ramp" ||
			meta_widget == "maya_colorRamp" )
		{
			/* 
				The attributes convention for the ramps is made of 4 siblings (in the
				same page) attributes:
				
				"rampName": 
				the 'main' attribute. Array of integers. It has "widget"='colorRamp', 
				and indicates the number of elements in the ramp.

				"rampName_Colors":
				array of colors. The color values of the ramp points. 'widget' should be
				set to 'null'.
				
				"rampName_Interpolation" : 
				string attribute; indicates the interpolation mode. 'widget' should be 
				set to null.
				
				"rampName_Knots" : 
				array of floats. The positions of the ramp points. 'widget' should be 
				set to 'null'.

				We expect the shader parameter that has the katana_name xyz_Colors
				annotation to have the "colorRamp" widget type.
			*/

			std::string suffix("_Colors");
			if( std::string::npos != 
			 parameter_name.find(suffix, parameter_name.length() - suffix.length() ) )
			{
				ramp_name = 
					parameter_name.substr(0, parameter_name.length() - suffix.length() );
			}
		}

		if( ramp_name.length() > 0 )
		{
			/*
				Construct the "main" attribute - it does not correspond to any
				shader parameter. We derive it from the xyz_Colors parameter.
				Add 2 to size for the two control points needed in katana ramp
				(needed if meta data widget is "maya_colorRamp").
			*/
			int new_size = meta_widget == "maya_colorRamp" ? size+2 : size;

 			FnAttribute::IntBuilder ib(1);
			ib.push_back(new_size);

			hints.set(
				"widget",
				FnAttribute::StringAttribute("colorRamp"));

			addRenderObjectParam(
					io_renderer_object_info,
					ramp_name,
					kFnRendererObjectValueTypeInt,
					-1,
					ib.build(),
					hints.build(),
					enumValues );

			{
				/* 
					Construct the "ramp_name_Colors" attribute 
				*/
				FnAttribute::FloatBuilder fb(new_size*3);
				for(int i = 0; i<size; i++)
				{
					fb.push_back( i_parameter->fdefault[i*3 + 0] );
					fb.push_back( i_parameter->fdefault[i*3 + 1] );
					fb.push_back( i_parameter->fdefault[i*3 + 2] );
					if (new_size > size && (i == 0 || i == size-1))
					{
						fb.push_back( i_parameter->fdefault[i*3 + 0] );
						fb.push_back( i_parameter->fdefault[i*3 + 1] );
						fb.push_back( i_parameter->fdefault[i*3 + 2] );
					}
				}

				FnAttribute::GroupBuilder hints;
				hints.set(
					"widget",
					FnAttribute::StringAttribute("null"));

				if( !meta_page.empty() )
				{
					hints.set(
						"page",
						FnAttribute::StringAttribute(meta_page));
				}

				addRenderObjectParam(
						io_renderer_object_info,
						parameter_name,
						kFnRendererObjectValueTypeFloat,
						new_size * 3,
						fb.build(),
						hints.build(),
						enumValues );
			}

			/*
				The ramp_name_Interpolation and ramp_name_Knots are expected to be
				provided by the shader.
			*/
		}
		else
		{
			/* Default */
			FnAttribute::FloatBuilder fb(3);
			for( int i = 0; i < i_parameter->type.arraylen; i++ )
			{
				fb.push_back(i_parameter->fdefault[i*3 + 0]);
				fb.push_back(i_parameter->fdefault[i*3 + 1]);
				fb.push_back(i_parameter->fdefault[i*3 + 2]);
			}

			/* UI */
			if( !has_widget && i_parameter->type.arraylen == -1)
			{
				hints.set(
						"widget",
						FnAttribute::StringAttribute("dynamicArray"));
			}

			addRenderObjectParam(
					io_renderer_object_info,
					parameter_name,
					kFnRendererObjectValueTypeColor3,
					-1,
					fb.build(),
					hints.build(),
					enumValues );
		}
	}
	else if( i_parameter->type.IsOneFloat() )
	{
		/* Default */
		FnAttribute::FloatBuilder fb(1);
		fb.push_back(i_parameter->fdefault[0]);

		/* Determine if we need a slider */
		bool slider = meta_slider;
		if( !slider )
		{
			slider =
				( meta_fmax_exists && meta_fmin_exists ) ||
				( meta_fslidermax_exists && meta_fslidermin_exists );
		}

		if( slider )
		{
			hints.set(
				"slider",
				FnAttribute::IntAttribute(1));
		}

		if( meta_fmax_exists )
		{
			hints.set(
					"max",
					FnAttribute::FloatAttribute(meta_fmax));
		}
		if( meta_fmin_exists )
		{
			hints.set(
					"min",
					FnAttribute::FloatAttribute(meta_fmin));
		}
		if( meta_fsensitivity_exists )
		{
			hints.set(
					"sensitivity",
					FnAttribute::FloatAttribute(meta_fsensitivity));
		}
		if( meta_fslidercenter_exists )
		{
			hints.set(
					"slidercenter",
					FnAttribute::FloatAttribute(meta_fslidercenter));
		}
		if( meta_fsliderexponent_exists )
		{
			hints.set(
					"sliderexponent",
					FnAttribute::FloatAttribute(meta_fsliderexponent));
		}
		if( meta_fslidermax_exists )
		{
			hints.set(
					"slidermax",
					FnAttribute::FloatAttribute(meta_fslidermax));
		}
		else if( meta_fmax_exists && slider )
		{
			/* If we have a slider, we need slidermax anyway */
			hints.set(
					"slidermax",
					FnAttribute::FloatAttribute(meta_fmax));
		}
		if( meta_fslidermin_exists )
		{
			hints.set(
					"slidermin",
					FnAttribute::FloatAttribute(meta_fslidermin));
		}
		else if( meta_fmin_exists && slider )
		{
			/* If we have a slider, we need slidermin anyway */
			hints.set(
					"slidermin",
					FnAttribute::FloatAttribute(meta_fmin));
		}

		addRenderObjectParam(
				io_renderer_object_info,
				parameter_name,
				kFnRendererObjectValueTypeFloat,
				-1,
				fb.build(),
				hints.build(),
				enumValues );


		if( parameter_name.find("inten") != std::string::npos ||
			parameter_name.find("mult") != std::string::npos )
		{
			o_intensity_parameters.push_back(
					i_parameters_prefix + parameter_name);
		}
		else if( parameter_name.find("exp") != std::string::npos)
		{
			o_exposure_parameters.push_back(
					i_parameters_prefix + parameter_name);
		}
	}
	// TODO: this could be reworked to handle single float, color and color array
	// 
	else if( 
		i_parameter->type.elementtype == NSITypePoint ||
		i_parameter->type.elementtype == NSITypeNormal ||
		i_parameter->type.elementtype == NSITypeVector ||
		i_parameter->type.elementtype == NSITypeMatrix ||
		(i_parameter->type.is_array() && 
			i_parameter->type.elementtype == NSITypeFloat))
	{
		// The size of one array element
		size_t element_size = i_parameter->type.elementsize();
		size_t num_floats_in_element = element_size / sizeof( float );

		int size = i_parameter->type.arraylen;
		if( size < 0 )
		{
			size = int(i_parameter->fdefault.size() / num_floats_in_element);
		}

		std::string ramp_name;
		if( meta_widget == "floatRamp" || meta_widget == "ramp" ||
			meta_widget == "maya_floatRamp" )
		{
			/* 
				See comment block for color ramps for details about the Katana
				convention. Float ramps use the same convention, except that the values
				are expected in a "_Floats" attribute.
			
				We expect the floatRamp / ramp widget on the parameter that has the 
				katana_name = "rampname_Floats" annotation.
			*/
			std::string suffix("_Floats");
			if( std::string::npos != 
			 parameter_name.find(suffix, parameter_name.length() - suffix.length() ) )
			{
				ramp_name = 
					parameter_name.substr(0, parameter_name.length() - suffix.length() );
			}
		}

		if( ramp_name.length() > 0 )
		{
			/*
				Construct the "main" attribute - it does not correspond to any
				shader parameter. We derive it from the xyz_Floats parameter.
				Add 2 to size for the two control points needed in katana ramp
				(needed if meta data widget is "maya_floatRamp").
			*/
			int new_size = meta_widget == "maya_floatRamp" ? size+2 : size;

			FnAttribute::IntBuilder ib(1);
			ib.push_back(new_size);

			hints.set(
				"widget",
				FnAttribute::StringAttribute("floatRamp"));

			addRenderObjectParam(
				io_renderer_object_info,
				ramp_name,
				kFnRendererObjectValueTypeInt,
				-1,
				ib.build(),
				hints.build(),
				enumValues );

			{
				/* 
					Construct the "ramp_name_Floats" attribute 
				*/
				FnAttribute::FloatBuilder fb(new_size);
				for(int i = 0; i<size; i++)
				{
					fb.push_back( i_parameter->fdefault[i] );
					if (new_size > size && (i == 0 || i == size-1))
					{
						fb.push_back( i_parameter->fdefault[i] );
					}
				}

				FnAttribute::GroupBuilder hints;
				hints.set(
					"widget",
					FnAttribute::StringAttribute("null"));

				if( !meta_page.empty() )
				{
					hints.set(
						"page",
						FnAttribute::StringAttribute(meta_page));
				}

				addRenderObjectParam(
					io_renderer_object_info,
					parameter_name,
					kFnRendererObjectValueTypeFloat,
					new_size,
					fb.build(),
					hints.build(),
					enumValues );

			}

			/*
				The ramp_name_Interpolation and ramp_name_Knots are expected to be
				provided by other shader parameters.
			*/
		}
		else
		{
			// The number of initial values we need to provide to the attribute.
			int num_init_elements = i_parameter->type.is_array() ? size : 1;
			int num_init_floats = int(num_init_elements * num_floats_in_element);

			// It is possible to define less default values than the specified array
			// size. OSL currently provides as many default values as the array
			// size dictates.
			//
			assert( num_init_floats == i_parameter->fdefault.size() );

			// If we may need to duplicate first and last position in float
			// ramp, we add 2 more floats.

			int new_num_init_floats =
				meta_related_to_widget == "maya_floatRamp" ||
                meta_related_to_widget == "maya_colorRamp"
				? num_init_floats+2 : num_init_floats;

			FnAttribute::FloatBuilder fb(new_num_init_floats);

			// Copy default parameter values to the Katana attribute
			for( int i = 0; i < i_parameter->fdefault.size() ; i++ )
			{
				fb.push_back( i_parameter->fdefault[ i ] );
				if (new_num_init_floats > num_init_floats &&
					(i == 0 || i == i_parameter->fdefault.size()-1))
				{
					fb.push_back( i_parameter->fdefault[i] );
				}
			}

			/* 
				Katana has a widget type dynamicArray, which was added to support
				PRMan shaders with dynamic array parameters. Unlike the other
				array types listed in Widget Types in the Katana User Guide,
				dynamicArrays cannot be created as User Parameters on nodes
				through the UI wrench menu. 
			*/
			if( !has_widget && i_parameter->type.arraylen == -1 )
			{
				hints.set(
					"widget",
					FnAttribute::StringAttribute("dynamicArray"));
			}

			addRenderObjectParam(
				io_renderer_object_info,
				parameter_name,
				kFnRendererObjectValueTypeFloat,
				new_num_init_floats,
				fb.build(),
				hints.build(),
				enumValues );
		}
	}
	else if( Utils::IsOSLTypeIntArray( i_parameter->type ) )
	{
		if( meta_related_to_widget == "floatRamp" ||
			meta_related_to_widget == "maya_floatRamp" ||
			meta_related_to_widget == "colorRamp" ||
			meta_related_to_widget == "maya_colorRamp" )
		{
			/* Default */
			FnAttribute::StringBuilder sb(1);
			sb.push_back(
				Utils::RampInterpolationInt2Str(i_parameter->idefault[0]));

			hints.set(
				"options",
				FnAttribute::StringAttribute(Utils::RampInterpolationOptions()));
			addRenderObjectParam(
				io_renderer_object_info,
				parameter_name,
				kFnRendererObjectValueTypeString,
				-1,
				sb.build(),
				hints.build(),
				enumValues );
		}
	}
	else if( i_parameter->type.IsOneInteger() )
	{
		/* Default */
		FnAttribute::IntBuilder ib(1);
		ib.push_back(i_parameter->idefault[0]);

		int type;
		if( meta_widget == "checkBox" )
		{
			type = kFnRendererObjectValueTypeBoolean;
		}
		else
		{
			type = kFnRendererObjectValueTypeInt;

			/* Slider/min/max can only be if it's not a checkbox. */

			/* Determine if we need a slider */
			bool slider = meta_slider;
			if( !slider )
			{
				slider =
					( meta_fmax_exists && meta_fmin_exists ) ||
					( meta_fslidermax_exists && meta_fslidermin_exists );
			}

			if( slider )
			{
				hints.set(
						"slider",
						FnAttribute::IntAttribute(1));
			}

			if( meta_imax_exists )
			{
				hints.set(
						"max",
						FnAttribute::IntAttribute(meta_imax));
			}
			if( meta_imin_exists )
			{
				hints.set(
						"min",
						FnAttribute::IntAttribute(meta_imin));
			}
			if( meta_isensitivity_exists )
			{
				hints.set(
						"sensitivity",
						FnAttribute::IntAttribute(meta_isensitivity));
			}
			if( meta_islidercenter_exists )
			{
				hints.set(
						"slidercenter",
						FnAttribute::IntAttribute(meta_islidercenter));
			}
			if( meta_isliderexponent_exists )
			{
				hints.set(
						"sliderexponent",
						FnAttribute::IntAttribute(meta_isliderexponent));
			}
			if( meta_islidermax_exists )
			{
				hints.set(
						"slidermax",
						FnAttribute::IntAttribute(meta_islidermax));
			}
			else if( meta_imax_exists && slider )
			{
				/* If we have a slider, we need slidermax anyway */
				hints.set(
						"slidermax",
						FnAttribute::IntAttribute(meta_imax));
			}
			if( meta_islidermin_exists )
			{
				hints.set(
						"slidermin",
						FnAttribute::IntAttribute(meta_islidermin));
			}
			else if( meta_imin_exists && slider )
			{
				/* If we have a slider, we need slidermin anyway */
				hints.set(
						"slidermin",
						FnAttribute::IntAttribute(meta_imin));
			}
		}

		addRenderObjectParam(
				io_renderer_object_info,
				parameter_name,
				type,
				-1,
				ib.build(),
				hints.build(),
				enumValues );
	}
	else if( i_parameter->type.IsOneString() )
	{
		/* Default */
		FnAttribute::StringBuilder sb(1);
		sb.push_back(i_parameter->sdefault[0].c_str());

		addRenderObjectParam(
				io_renderer_object_info,
				parameter_name,
				kFnRendererObjectValueTypeString,
				-1,
				sb.build(),
				hints.build(),
				enumValues );
	}
	else if( i_parameter->isclosure )
	{
		/* There is never a widget for closure parameters. */
		hints.set("widget", FnAttribute::StringAttribute("null"));

		addRenderObjectParam(
			io_renderer_object_info,
			parameter_name,
			kFnRendererObjectValueTypeUnknown,
			-1,
			FnAttribute::IntAttribute(0), /* needed for null widget to work */
			hints.build(),
			enumValues );
	}
}

