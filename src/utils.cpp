/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2016                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "utils.h"

#include "System.h"

#include <mutex>
#include <sstream>
#include <string.h>

static std::mutex g_osl_cache_mutex;
static Utils::OSLCache g_osl_cache;

static std::mutex g_bounding_sphere_cache_mutex;
static std::map<std::string, float> g_bounding_sphere_cache;

/* Reads a OSL shader from file and adds it to the cache */
void RegisterSingleOSLShader(
		const char* i_path, const char* i_name, void *i_cache)
{
	/* Check if the filename is not too short. Since there is an extension, we
	   need at least 5 symbols. */
	if( strlen(i_path) < 5 )
	{
		return;
	}

	Utils::OSLCache* cache = reinterpret_cast<Utils::OSLCache*>(i_cache);

	OSLShaderInfo shader;

	if( !shader.open(i_path) )
	{
		/* It's not a shader. */
		return;
	}

	cache->addShaderInfo(shader.shadername().c_str(), shader);
}

/* Reads all the shaders and fills the cache */
void CacheOSLShaders(void *i_cache)
{
	Utils::OSLCache* cache = reinterpret_cast<Utils::OSLCache*>(i_cache);

	std::string osl_path = Utils::GetOSLDirectory();

	if( osl_path.empty() )
	{
		return;
	}

	if( !System::FileIsDir(osl_path.c_str()) )
	{
		return;
	}

	/* Read all the files in the dir and load each as a osl shader */
	System::ScanDir(
			osl_path.c_str(), &RegisterSingleOSLShader, cache );
}

/*
	Given an OSL shader name (without the .oso), this returns a full path to
	it. It is assumed for now that it is a shader shipped with 3DFK.
*/
std::string Utils::GetOSLShaderPath( const std::string &i_shader )
{
	return GetOSLDirectory() + "/" + i_shader;
}

struct OSLDirectory
{
	std::string m_dir;

	OSLDirectory()
	{
		/* Check OSL_PATH first and if it doesn't exist, find the path where
		   3Delight is installed. */
		const char* osl_path = getenv("OSL_PATH");
		if( osl_path && strlen(osl_path) > 3 && System::FileIsDir(osl_path) )
		{
			m_dir = osl_path;
		}
		else
		{
			std::string path_3dfk = System::LibraryPath();
			/*
				path_3dfk ends with something like
				3DelightForKatana/Libs/3Delight_for_Katana.so
				We want to keep the part of the path before Libs so we just
				remove everything after the last two path separators.
			*/
			std::string::size_type sep = path_3dfk.find_last_of( "/\\" );
			if( sep != std::string::npos )
			{
				path_3dfk = path_3dfk.substr( 0, sep );
				sep = path_3dfk.find_last_of( "/\\" );
				if( sep != std::string::npos )
				{
					m_dir = path_3dfk.substr( 0, sep ) + "/osl";
				}
			}
		}
	}
};
static OSLDirectory s_osl_directory;

std::string Utils::GetOSLDirectory()
{
	return s_osl_directory.m_dir;
}

const Utils::OSLCache& Utils::GetOSLShaders()
{
	std::lock_guard<std::mutex> cache_l( g_osl_cache_mutex );

	if( g_osl_cache.isEmpty() )
	{
		CacheOSLShaders( &g_osl_cache );
	}

	return g_osl_cache;
}

void Utils::FlushOSLShaders()
{
	g_osl_cache.flush();
}

std::string Utils::GetParameterName(const DlShaderInfo::Parameter* i_parameter)
{
	// Returns one of katana_attribute, attribute or the parameter name, in that 
	// order of priority.
	//
	std::string katana_attribute;
	std::string attribute;

	const auto& meta = i_parameter->metadata;
	for(auto it = meta.begin(); it != meta.end(); it++)
	{
		if( it->name == "katana_attribute" && it->type.IsOneString() )
		{
			katana_attribute = it->sdefault[0].c_str();
		}
		else if( it->name == "attribute" && it->type.IsOneString() )
		{
			attribute = it->sdefault[0].c_str();
		}
	}

	if( katana_attribute.length() > 0 )
		return katana_attribute;

	if( attribute.length() > 0 )
		return attribute;

	return i_parameter->name.c_str();
}

const DlShaderInfo::Parameter* Utils::GetParam(
		const DlShaderInfo& i_shader, std::string i_name)
{
	for (size_t i = 0;  i < i_shader.nparams(); ++i)
	{
		const DlShaderInfo::Parameter* param = i_shader.getparam(i);

		if( !param )
		{
			continue;
		}

		if (GetParameterName(param) == i_name)
		{
			return param;
		}
	}

	return NULL;
}

void Utils::ClearBoundingSphereCache()
{
	std::lock_guard<std::mutex> cache_l( g_bounding_sphere_cache_mutex );
	g_bounding_sphere_cache.clear();
}

void Utils::SetBoundingSphereCache(const std::string& i_name, float i_sphere)
{
	std::lock_guard<std::mutex> cache_l( g_bounding_sphere_cache_mutex );
	g_bounding_sphere_cache[i_name] = i_sphere;
}

bool Utils::GetBoundingSphereCache(const std::string& i_name, float& o_sphere)
{
	std::lock_guard<std::mutex> cache_l( g_bounding_sphere_cache_mutex );
	std::map<std::string, float>::iterator search =
		g_bounding_sphere_cache.find(i_name);

	if( search != g_bounding_sphere_cache.end() )
	{
		o_sphere = search->second;
		return true;
	}

	return false;
}

std::string Utils::Float2Str(float f)
{
	char buffer[1024];
	snprintf( buffer, sizeof(buffer), "%f", f );
	return buffer;
}

std::string Utils::Color2Str(float f[3])
{
	char buffer[1024];
	snprintf( buffer, sizeof(buffer), "%f,%f,%f", f[0], f[1], f[2] );
	return buffer;
}

std::vector<std::string> InitRampInter()
{
	std::vector<std::string> rampInter;
    rampInter.push_back("constant");
    rampInter.push_back("linear");
    rampInter.push_back("catmull-rom");
    rampInter.push_back("bspline");
    return rampInter;
}

std::vector<std::string> rampInter = InitRampInter();

std::string Utils::RampInterpolationInt2Str(int i_inter)
{
	if (i_inter >= 0 && i_inter < rampInter.size())
	{
		return rampInter[i_inter];
	}

	assert(false);
	return rampInter[1];
}

int Utils::RampInterpolationStr2Int(const std::string& i_inter)
{
	for (int i = 0;	i < rampInter.size(); i++)
	{
		if (rampInter[i] == i_inter)
		{
			return i;
		}
	}

	assert(false);
	return 1;
}

std::string Utils::RampInterpolationOptions()
{
	return rampInter[0] + "|" + rampInter[1] + "|" + rampInter[2] + "|" + rampInter[3];
}

bool Utils::IsOSLTypeIntArray(const DlShaderInfo::TypeDesc& i_type)
{
	return i_type.IsIntegerArray();
}

bool Utils::IsOSLTypeFloatArray(const DlShaderInfo::TypeDesc& i_type)
{
	return i_type.IsFloatArray();
}

bool Utils::IsOSLTypeColorArray(const DlShaderInfo::TypeDesc& i_type)
{
	return i_type.IsColorArray();
}

std::string Utils::GetMetaDataValue(
	const DlShaderInfo::Parameter &i_parameter,
	const std::string& i_name)
{
	for( const DlShaderInfo::Parameter &m : i_parameter.metadata )
	{
		if( !m.type.IsOneString() )
		{
			continue;
		}
		if( m.name == i_name )
		{
			return m.sdefault[0].string();
		}
	}

	return "";
}

void Utils::EnsurePathInEnvironment(
		const char* i_env, const std::string& i_path)
{
	char* value = getenv(i_env);

	std::string value_str;
	if( value )
	{
		value_str = value;
	}

	/* Check if i_path is already in i_env */
	if( value_str.find(i_path) != std::string::npos )
	{
		return;
	}

	if( value )
	{
		value_str += PATHSEPARATOR;
	}

	value_str += i_path;

#ifdef _WIN32
	_putenv_s(i_env, value_str.c_str());
#else
	setenv(i_env, value_str.c_str(), 1);
#endif
}

