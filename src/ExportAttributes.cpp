/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "ExportAttributes.h"

#include "AttributeUtils.h"
#include "ExportContext.h"
#include "utils.h"

using AttributeUtils::GetString;

namespace ExportAttributes
{

/**
	\brief Export the NSI attributes at the given scene graphe location.

	\param i_ctx
		The exporting context.
	\param i_location
		The location for which to export all attributes.
*/
void ExportAttributes(
	ExportContext &i_ctx,
	const SGLocation &i_location )
{
	/*
		For deletion, remove our own attributes node and that's it. We don't
		do the disconnect part because that will happen automatically when
		the transform node gets deleted.

		We also delete the |attr node for live updates as otherwise attributes
		which are no longer set will stick around on the node, as long as there
		is at least one other attribute to cause it to be reconnected to the
		transform. To reproduce: set both light linking and visibility on the
		same location. Disable shadow visibility. Then unset visibility with
		"disable local assignment", not turning it back on.
	*/
	if( i_location.IsDeleted() || i_location.IsLiveUpdate() )
	{
		std::string trs_handle = i_location.getFullName();
		std::string attr_handle = trs_handle + "|attr";
		i_ctx.m_nsi.Delete( attr_handle );

		if( i_location.IsDeleted() )
			return;
	}

	/* If this is a live update, first disconnect previous attributes nodes. */
	if( i_location.IsLiveUpdate() )
	{
		i_ctx.m_nsi.Disconnect(
			NSI_ALL_NODES, "", i_location.getFullName(), "geometryattributes" );
	}

	ExportMaterialAssign( i_ctx, i_location );
	bool has_attr_node = false;
	ExportVisibilityAttributes( i_ctx, i_location, has_attr_node );
	ExportLightLinking( i_ctx, i_location, has_attr_node );
	ExportLightAttributes( i_ctx, i_location, has_attr_node );
	ExportCompositingMode( i_ctx, i_location, has_attr_node );
}

/**
	\brief Exports an empty NSI attributes for the location.

	\param i_ctx
		The exporting context.
	\param i_location
		The location for which to export an attributes node.
	\param io_has_attr_node
		Flag used to create the attributes node only once.
	\returns
		The handle to the attributes node for the location.

	The attributes node is connected to the appropriate NSI transform node.
*/
std::string ExportAttributesNode(
	ExportContext &i_ctx,
	const SGLocation &i_location,
	bool &io_has_attr_node )
{
	std::string trs_handle = i_location.getFullName();
	std::string attr_handle = trs_handle + "|attr";

	if( io_has_attr_node )
		return attr_handle;

	io_has_attr_node = true;
	i_ctx.m_nsi.Create( attr_handle, "attributes" );
	i_ctx.m_nsi.Connect( attr_handle, "", trs_handle, "geometryattributes" );
	return attr_handle;
}

/**
	\brief Export material assignment on a scene graph location.

	\param i_ctx
		The export context.
	\param i_location
		The scene graph location for which to export material assignment.
*/
void ExportMaterialAssign(
	ExportContext &i_ctx,
	const SGLocation &i_location )
{
	std::string material;
	GetString( i_location, "info.materialAssign", material );

	if( material.empty() )
		return;

	/*
		Create the material node in case it gets exported after its assignement.
	*/
	std::string material_handle = material + "|material";
	i_ctx.m_nsi.Create( material_handle, "attributes" );

	i_ctx.m_nsi.Connect(
		material_handle, "",
		i_location.getFullName(), "geometryattributes" );
}

/**
	\brief Adds one visibility attribute to a NSI attribute node.

	\param i_group_attr
		The group attribute that will be searched.
	\param i_attr_name
		The name of the attribute to look for.
	\param io_attributes
		The NSI attribute list where we'll add the required attribute.

	A utility procedure for ExportVisibilityAttributes.
*/

void ExportOneVisibilityAttribute(
	FnAttribute::GroupAttribute &i_group_attr,
	const char* i_attr_name,
	NSI::ArgumentList &io_attributes )
{
	using AttributeUtils::GetInteger;

	int value = -1;
	GetInteger( i_group_attr, i_attr_name, value );

	if( value >= 0 )
	{
		int visible = value > 0 ? 1 : 0;
		io_attributes.push( new NSI::IntegerArg( i_attr_name, visible ) );
	}
}
/**
	\brief Export visibility attributes for a scene graph location.

	\param i_ctx
		The export context.
	\param i_location
		The scene graph location for which to export visibility attributes.
	\param io_has_attr_node
		Flag used to output the attributes node only once, when first needed.
		Set to false initially, will be changed to true on export of
		attributes node.

	Only exports the visibility attributes that have been modified.
*/
void ExportVisibilityAttributes(
	ExportContext &i_ctx,
	const SGLocation &i_location,
	bool &io_has_attr_node )
{
	using AttributeUtils::GetInteger;

	std::string group_name(_3DELIGHT_RENDERER_NAME);
	group_name += "ObjectSettings";

	FnAttribute::GroupAttribute settings_attr( 
		i_location.getAttribute( group_name.c_str() ) );

	if( settings_attr.isValid() )
	{
		NSI::ArgumentList nsi_attributes;

		ExportOneVisibilityAttribute(
			settings_attr, "visibility.camera", nsi_attributes );

		ExportOneVisibilityAttribute(
			settings_attr, "visibility.shadow", nsi_attributes );

		ExportOneVisibilityAttribute(
			settings_attr, "visibility.diffuse", nsi_attributes );

		ExportOneVisibilityAttribute(
			settings_attr, "visibility.specular", nsi_attributes );

		ExportOneVisibilityAttribute(
			settings_attr, "visibility.reflection", nsi_attributes );

		ExportOneVisibilityAttribute(
			settings_attr, "visibility.refraction", nsi_attributes );

		/* Do emission type. */
		int et_value = -1;
		GetInteger( settings_attr, "emissiontype", et_value );
		if( et_value >= 0 )
		{
			nsi_attributes.push(
				new NSI::IntegerArg( "regularemission", et_value != 1 ) );
			nsi_attributes.push(
				new NSI::IntegerArg( "quantizedemission", et_value != 2 ) );
		}

		if( !nsi_attributes.empty() )
		{
			std::string attr_handle =
				ExportAttributesNode( i_ctx, i_location, io_has_attr_node );

			i_ctx.m_nsi.SetAttribute( attr_handle, nsi_attributes );
		}
	}
}

/**
	\brief Export light linking for a scene graph location.

	\param i_ctx
		The export context.
	\param i_location
		The scene graph location for which to export light linking.
	\param io_has_attr_node
		Flag used to output the attributes node only once, when first needed.
		Set to false initially, will be changed to true on export of
		attributes node.

	Only exports the light linking attributes that have been modified.
*/
void ExportLightLinking(
	ExportContext &i_ctx,
	const SGLocation &i_location,
	bool &io_has_attr_node )
{
	/*
		Skip this at /root/world. That location is not typical light linking
		but rather used as the default state for lights. We output that as
		visibility on the light itself in ExportLightAttributes instead of as
		connections as it's much more efficient.
	*/
	if( i_location.getFullName() == "/root/world" )
		return;

	// The base light linking attribute group.
	std::string group_name( "lightList" );

	FnAttribute::GroupAttribute lightlist_attr( 
		i_location.getAttribute( group_name.c_str() ) );

	if( lightlist_attr.isValid() )
	{
		/*
			Each child attribute is a group that contains:
			  - "path" : the light path
			  - "enable" : wheter or not the light lights this location.

			Normally, "path" is locally defined in /root/world, but not on children
			nodes; they only have the "enable" attribute visible because it is
			locally assigned. We now have an OpScript that makes "path" visible as if
			it had a local assignment.
		*/
		
		int num_children = lightlist_attr.getNumberOfChildren();
		for(int i = 0; i < num_children; i++ )
		{
			FnAttribute::GroupAttribute lightlist_item = 
				lightlist_attr.getChildByIndex(i);

			std::string light;
			AttributeUtils::GetString( lightlist_item, "path", light );

			int enable;
			AttributeUtils::GetInteger( lightlist_item, "enable", enable );

			/*
				Create the light link attribute in case we have not exported
				the light yet.
			*/
			std::string lightlink_attr = light + "|visibility";
			i_ctx.m_nsi.Create( lightlink_attr, "attributes" );

			/*
				Make a visibility connection between this location's attribute
				node and the light's attribute node. Its priority is based on
				this location's depth, so that children light linking may
				override parent's light linking.
			*/
			std::string attr_handle =
				ExportAttributesNode( i_ctx, i_location, io_has_attr_node );

			i_ctx.m_nsi.Connect(
				attr_handle, "",
				lightlink_attr, "visibility",
				NSI::IntegerArg( "value", enable > 0 ? 1 : 0 ) );
		}
	}
}

/*
	This exports the attributes specific to a light source.
*/
void ExportLightAttributes(
	ExportContext &i_ctx,
	const SGLocation &i_location,
	bool &io_has_attr_node )
{
	/* This is added on lights by our own terminal Op. */
	FnAttribute::GroupAttribute dl_light_group =
		i_location.getAttribute( "info.dl_light" );

	/* If it's not there, this is not a light. */
	if( !dl_light_group.isValid() )
		return;

	/*
		Create light attribute for light linking, in case not already done
		because there is no specific linking for this light. Then connect it to
		the light.
	*/
	std::string light_name = i_location.getFullName();
	std::string lightlink_attr = light_name + "|visibility";

	i_ctx.m_nsi.Create( lightlink_attr, "attributes" );
	i_ctx.m_nsi.Connect( 
		lightlink_attr, "", 
		light_name, "geometryattributes" );

	/* Go on with actual attributes. */
	std::string attr_handle =
		ExportAttributesNode( i_ctx, i_location, io_has_attr_node );

	int enable = 1;
	int mute = 0;
	AttributeUtils::GetInteger( i_location, "info.dl_light.enable", enable );
	AttributeUtils::GetInteger( i_location, "info.dl_light.mute", mute );

	/* Make it invisible if not enabled by default. */
	int visibility = enable == 0 ? 0 : 1;

	/* Make it *really* invisible if muted, to override light linking. */
	if( mute != 0 )
	{
		visibility = 0;
		i_ctx.m_nsi.SetAttribute(
			attr_handle,
			NSI::IntegerArg( "visibility.priority", 50 ) );
	}
	else if( i_location.IsLiveUpdate() )
	{
		/* Don't want that priority to stick around when mute is unchecked. */
		i_ctx.m_nsi.DeleteAttribute( attr_handle, "visibility.priority" );
	}

	i_ctx.m_nsi.SetAttribute(
		attr_handle,
		NSI::IntegerArg( "visibility", visibility ) );
}

/**
	\brief This exports the matte and prelit attributes.
*/
void ExportCompositingMode(
	ExportContext &i_ctx,
	const SGLocation &i_location,
	bool &io_has_attr_node )
{
	int value = -1;
	AttributeUtils::GetInteger(
		i_location, "dlObjectSettings.visibility.compositingMode", value );
	if( value != -1)
	{
		std::string attr_handle =
			ExportAttributesNode( i_ctx, i_location, io_has_attr_node );

		i_ctx.m_nsi.SetAttribute(
			attr_handle,
			(
				NSI::IntegerArg( "matte", value == 1 ),
				NSI::IntegerArg( "prelit", value == 2 )
			) );
	}
}

}
