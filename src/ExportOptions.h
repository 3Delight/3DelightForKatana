/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __ExportOptions_h
#define __ExportOptions_h

#include "SGLocation.h"

class ExportContext;

/*
	This namespace is for code which handles exporting rendering options.
*/
namespace ExportOptions
{
	void ExportRenderNotes( ExportContext &i_ctx );

	void ExportOptions( ExportContext &i_ctx );

	void UpdateRenderSettings(
		ExportContext &i_ctx,
		const SGLocation &i_location );
}

#endif

