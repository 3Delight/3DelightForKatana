#include "KatanaMonitorDriver.h"
#include "ndspy.h"

#include <FnDisplayDriver/FnKatanaDisplayDriver.h>

#include <algorithm>
#include <cassert>
#include <cstring>
#include <memory>
#include <vector>

namespace FnKat = Foundry::Katana;

/*
	Global data for the whole render, shared by all opened displays. We need
	this because:
	- The display API is old and unsuitable for sending all channels at once.
	  Yet they should share some information.
	- We must sometimes hold that data after the display is closed. See
	  'editable' hack above.

	This is never properly cleaned up. Does not really matter as the process
	ends soon after we're done using it.
*/
struct ImageData
{
	FnKat::KatanaPipe *m_pipe;

	struct FrameData
	{
		FrameData( int frameID, FnKat::NewFrameMessage *frame )
		: m_frameID{ frameID }, m_channelID{ 0 }, m_frame_message{ frame } {}

		const int m_frameID;
		uint16_t m_channelID; /* Sequential ID counter. */
		std::unique_ptr<FnKat::NewFrameMessage> m_frame_message;
	};

	std::vector<FrameData> m_frames;
	/*
		Ugly hack to support changing the camera which currently requires
		restarting the render (which implies reopening the display drivers).
		When this flag is true, it might happen (ie. we're in live render) so
		we can never really close anything.
	*/
	bool m_editable;
};

static ImageData *s_image;


static PtDspyError MonitorDspyImageOpen(
	PtDspyImageHandle *image_handle,
	const char *drivername,
	const char *filename,
	int width,
	int height,
	int paramCount,
	const UserParameter *parameters,
	int formatCount,
	PtDspyDevFormat *format,
	PtFlagStuff *flagstuff )
{
	/* Get all data as 32-bit float. */
	for( int i = 0; i < formatCount; ++i )
		format[i].type = PkDspyFloat32;

	if( !s_image )
	{
		/* First channel to be opened. Setup global stuff. */
		char *host = 0x0;
		DspyFindStringInParamList(
			"katana_host", &host, paramCount, parameters );
		if( !host )
			return PkDspyErrorBadParams;

		/* Parse the hostname and port number which we can't just get straight
		   from an API because simple is not fun. */
		std::string hostname( host );
		std::string::size_type colon = hostname.find_first_of( ':' );
		if( colon == std::string::npos )
			return PkDspyErrorBadParams;
		/* Add 100 for mysterious, undocumented reasons. */
		unsigned port = 100 + std::stoul( hostname.substr( colon + 1 ) );
		hostname = hostname.substr( 0, colon );
		/* No idea why Katana would send this but apparently it can happen. */
		if( hostname[0] == '_' )
			hostname = hostname.substr( 1 );

		ImageData *image = new ImageData;
		image->m_pipe = FnKat::PipeSingleton::Instance( hostname, port );

		if( 0 != image->m_pipe->connect() )
		{
			return PkDspyErrorUndefined;
		}

		/* Get editable parameter (1 if in live render). */
		int editable = 0;
		DspyFindIntInParamList(
			"editable", &editable, paramCount, parameters );
		image->m_editable = editable != 0;

		/* Store for reuse by other channels */
		s_image = image;
	}

	/* Get frameID and name. */
	int frameID = 0;
	DspyFindIntInParamList( "frameID", &frameID, paramCount, parameters );
	char *frameName = 0x0;
	DspyFindStringInParamList(
		"frameName", &frameName, paramCount, parameters );

	/* Encode frameID into name. Not sure this is still needed. */
	std::string encodedName;
	if( frameName )
	{
		FnKat::encodeLegacyName( frameName, frameID, encodedName );
	}

	/* Get frame time from parameters. */
	float frameTime = 0.0f;
	DspyFindFloatInParamList(
		"frameTime", &frameTime, paramCount, parameters );

	/* Get frame full size. */
	int frameSize[2] = { 0, 0 };
	int count = 2;
	DspyFindIntsInParamList(
		"OriginalSize", &count, frameSize, paramCount, parameters );

	/*
		See if we can find an existing frame. This will occur when displays
		are reopened after a camera change. Also for the ID pass which has the
		same frameID as the primary.
	*/
	FnKat::NewFrameMessage *frame = 0x0;
	uint16_t channelID;
	for( auto &&f : s_image->m_frames )
	{
		if( f.m_frameID == frameID )
		{
			frame = f.m_frame_message.get();
			channelID = f.m_channelID++;
		}
	}
	if( !frame )
	{
		/* Start new frame. */
		frame = new FnKat::NewFrameMessage;
		frame->setFrameTime( frameTime );
		frame->setFrameDimensions( frameSize[0], frameSize[1] );
		frame->setFrameName( encodedName );
		s_image->m_pipe->send( *frame );
		s_image->m_frames.emplace_back( frameID, frame );
		channelID = s_image->m_frames.back().m_channelID++;
	}

	/* Get data origin, for crop. */
	int frameOrigin[2] = { 0, 0 };
	DspyFindIntsInParamList(
		"origin", &count, frameOrigin, paramCount, parameters );

	/* Now setup and send the channel message. */
	FnKat::NewChannelMessage_v2 *channel = new FnKat::NewChannelMessage_v2(
			*frame, channelID, 
			formatCount == 4
				? FnKat::NewChannelMessage_v2::RGBA
				: FnKat::NewChannelMessage_v2::UNDEFINED );

	/*
		Katana's idea of coordinates is designed to short circuit your brain.
		If you must really know:
		- The Y axis is flipped here. But it's not for a DataMessage.
		- "Dimensions" is the coordinate where the crop ends. Not its size.
	*/
	int katanaTop = frameSize[1] - frameOrigin[1];
	channel->setChannelDimensions( width + frameOrigin[0], katanaTop );
	channel->setChannelOrigin( frameOrigin[0], katanaTop - height );

	const float sampleRate[2] = { 1.0f, 1.0f };
	channel->setSampleRate( sampleRate );
	channel->setDataSize( sizeof(float) * formatCount );
	channel->setChannelName( encodedName );
	s_image->m_pipe->send( *channel );

	*image_handle = channel;

	return PkDspyErrorNone;
}

static PtDspyError MonitorDspyImageData(
	PtDspyImageHandle image_handle,
	int xmin, int xmax_plusone,
	int ymin, int ymax_plusone,
	int entrysize,
	const unsigned char *data_buffer )
{
	FnKat::NewChannelMessage_v2 *channel =
		reinterpret_cast<FnKat::NewChannelMessage_v2*>( image_handle );
	if( !s_image || !channel )
		return PkDspyErrorBadParams;

	int w = xmax_plusone - xmin, h = ymax_plusone - ymin;
	FnKat::DataMessage data( *channel );
	data.setStartCoordinates( xmin, ymin );
	data.setDataDimensions( w, h );
	data.setData( data_buffer, w * h * entrysize );
	data.setByteSkip( entrysize );
	s_image->m_pipe->send( data );

	return PkDspyErrorNone;
}

static PtDspyError MonitorDspyImageQuery(
	PtDspyImageHandle image_handle,
	PtDspyQueryType type,
	int datalen,
	void *data )
{
	if( !data && type != PkStopQuery )
		return PkDspyErrorBadParams;

	switch( type )
	{
		/* We do want all passes of a progressive render. */
		case PkProgressiveQuery:
		{
			if( datalen < sizeof(PtDspyProgressiveInfo) )
				return PkDspyErrorBadParams;
			((PtDspyProgressiveInfo*)data)->acceptProgressive = 1;
			break;
		}
		default:
			return PkDspyErrorUnsupported;
	}

	return PkDspyErrorNone;
}

static PtDspyError MonitorDspyImageClose(
	PtDspyImageHandle image_handle )
{
	FnKat::NewChannelMessage_v2 *channel =
		reinterpret_cast<FnKat::NewChannelMessage_v2*>( image_handle );
	if( !s_image || !channel )
		return PkDspyErrorBadParams;

	/*
		Flush all data. Don't close the channels when they might be reused or
		Katana will simply not accept new image data later.
	*/
	s_image->m_pipe->flushPipe( *channel );
	if( !s_image->m_editable )
	{
		s_image->m_pipe->closeChannel( *channel );
	}
	delete channel;

	return PkDspyErrorNone;
}

void RegisterKatanaMonitorDriver()
{
	PtDspyDriverFunctionTable table;
	std::memset( &table, 0, sizeof(table) );

	table.Version = k_PtDriverCurrentVersion;
	table.pOpen = &MonitorDspyImageOpen;
	table.pWrite = &MonitorDspyImageData;
	table.pQuery = &MonitorDspyImageQuery;
	table.pClose = &MonitorDspyImageClose;

	DspyRegisterDriverTable( KatanaMonitorDriverName(), &table );
}
