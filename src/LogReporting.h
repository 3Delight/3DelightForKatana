/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2016                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __logreporting_h
#define __logreporting_h

enum ErrorLevel
{
	eLevelInfo,
	eLevelWarning,
	eLevelError,
	eLevelFatal
};

#ifdef WIN32
#	define LOGMESSAGE(s, t) LogMessage(__FUNCTION__, (s), (t))
#	define LOGVAMESSAGE(s, t, ...) \
		LogMessage(__FUNCTION__, (s), (t), __VA_ARGS__)
#else
#	define LOGMESSAGE(s, t) LogMessage(__PRETTY_FUNCTION__, (s), (t))
#	define LOGVAMESSAGE(s, t, ...) \
		LogMessage(__PRETTY_FUNCTION__, (s), (t), __VA_ARGS__)
#endif

void LogMessage(
		const char* i_func, int i_severity, const char* i_message, ...);

#endif

