/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2016                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __UTILS_H
#define __UTILS_H 

#define _3DELIGHT_RENDERER_NAME "dl"

#define _3DELIGHT_SHADER "Shader"
#define _3DELIGHT_SURFACE_TERMINAL "Surface"
#define _3DELIGHT_LIGHT_TERMINAL "Light"
#define _3DELIGHT_DISPLACEMENT_TERMINAL "Displacement"
#define _3DELIGHT_ENVIRONMENT_TERMINAL "Environment"
#define _3DELIGHT_VOLUME_TERMINAL "Volume"

#define _3DELIGHT_PARAMS "Params"

#include <FnRendererInfo/plugin/RendererInfoBase.h>
#include <FnRendererInfo/plugin/ShaderInfoCache.h>
#include <FnScenegraphIterator/FnScenegraphIterator.h>

#include "OSLShaderInfo.h"

#ifdef _WIN32
#define snprintf _snprintf
#define PATHSEPARATOR ";"
#else
#define PATHSEPARATOR ":"
#endif

namespace Utils
{
	typedef FnKat::RendererInfo::ShaderInfoCache<OSLShaderInfo> OSLCache;
	typedef OSLCache::Iterator OSLCacheIterator;

	std::string GetOSLShaderPath( const std::string &i_shader );
	std::string GetOSLDirectory();

	/* Reads and returns all the OSL shaders. */
	const OSLCache& GetOSLShaders();
	/* Flush the OSL shaders cache. Not thread safe. */
	void FlushOSLShaders();

	/* Get the name of the parameter. It takes into account "maya_name" meta. */
	std::string GetParameterName(const DlShaderInfo::Parameter* i_parameter);
	/* Get parameter by name. It takes into account "maya_name" meta. */
	const DlShaderInfo::Parameter* GetParam(
		const DlShaderInfo& i_shader, std::string i_name);

	/* Storing the displacement bounds. */
	void ClearBoundingSphereCache();
	void SetBoundingSphereCache(const std::string& i_name, float i_sphere);
	bool GetBoundingSphereCache(const std::string& i_name, float& o_sphere);

	std::string Float2Str(float f);
	std::string Color2Str(float f[3]);
	/* Converts the specified ramp interpolation value from int to string. */
	std::string RampInterpolationInt2Str(int i_inter);
	/* Converts the specified ramp interpolation value from string to int. */
	int RampInterpolationStr2Int(const std::string& i_inter);
	/* Returns string "options" for float or color ramp. */
	std::string RampInterpolationOptions();

	/* Returns true if the OSL parameter is an int array. */
	bool IsOSLTypeIntArray(const DlShaderInfo::TypeDesc& i_type);
	/* Returns true if the OSL parameter is a float array. */
	bool IsOSLTypeFloatArray(const DlShaderInfo::TypeDesc& i_type);
	/* Returns true if the OSL parameter is a color array. */
	bool IsOSLTypeColorArray(const DlShaderInfo::TypeDesc& i_type);
	/* Returns the value of the specified metadata name. */
	std::string GetMetaDataValue(
		const DlShaderInfo::Parameter &i_parameter,
		const std::string& i_name);

	/* Adds path to the environment variable if it's not there yet. */
	void EnsurePathInEnvironment(const char* i_env, const std::string& i_path);
};

#endif

