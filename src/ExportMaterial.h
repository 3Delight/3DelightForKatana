/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __ExportMaterial_h
#define __ExportMaterial_h

#include "nsi.hpp"
#include "SGLocation.h"

class ExportContext;

/*
	This namespace is for code which handles exporting shaders and shader
	networks.
*/
namespace ExportMaterial
{
	namespace FK = Foundry::Katana;

	/*
		This provides several attribute names for a given shader type (eg.
		surface, displacement) together in a convenient place.
	*/
	struct ShaderInfo
	{
		const char *m_name; /* eg. "dlSurface" */
		const char *m_shader_name; /* eg. "dlSurfaceShader" */
		const char *m_nsi_attribute; /* eg. "surfaceshader" */
	};

	enum eShaderType
	{
		shader_surface,
		shader_light,
		shader_displacement,
		shader_environment,
		shader_volume
	};

	/* Information about each type listed above. */
	extern const ShaderInfo kShaderTypeInfo[5];

	void ExportOneMaterial(
		ExportContext &i_ctx,
		const SGLocation &i_materialLocation,
		const std::string &i_material_group );

	void ExportLightFilters(
		ExportContext &i_ctx,
		const SGLocation &i_location,
		eShaderType i_light_type );

	void CreateShader(
		ExportContext &i_ctx,
		const std::string &i_handle,
		const NSI::ArgumentList &i_attributes );

	void ExportOneShader(
		ExportContext &i_ctx,
		const SGLocation &i_location,
		const std::string &i_material_group,
		const std::string &i_terminal_name,
		std::string i_handle_name );

	void ExportDefaultMaterial(
		ExportContext &i_ctx,
		const SGLocation &i_location );
}

#endif
