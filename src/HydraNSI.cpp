#include "HydraNSI.h"

#include <stdio.h>

#ifdef _WIN32
#   include <windows.h>
#else
#	include <dlfcn.h>
#endif

const char *registration_script =
R"WOOF(
import os

def _Get3DfKRoot():
    import platform
    if platform.system() == "Windows":
        separator = ";"
        dynlib = "dll"
    else:
        separator = ":"
        dynlib = "so"

    katana_resources = os.environ["KATANA_RESOURCES"]

    for i in katana_resources.split(separator):
        if os.path.isfile(i + "/Libs/3Delight_for_Katana." + dynlib):
            return i

# Load the Hydra plugin
try:
    import fnpxr.Plug
    import NodegraphAPI
    import re
    version = ''
    # This is present in all verions we need it (4.5 and up) but not older ones.
    if hasattr(NodegraphAPI.Version, 'KatanaInfo'):
        m = re.match('\\d+\\.\\d', NodegraphAPI.Version.KatanaInfo.version)
        if m:
            version = m.group(0)
    p = None
    if version:
        # Try loading a version specific plugin.
        _hdNSI = os.path.join(_Get3DfKRoot(), 'usd', version, 'hdNSI', 'resources')
        p = fnpxr.Plug.Registry().RegisterPlugins(str(_hdNSI))
    if not p:
        # Try without version number. When building for a single version.
        _hdNSI = os.path.join(_Get3DfKRoot(), 'usd', 'hdNSI', 'resources')
        p = fnpxr.Plug.Registry().RegisterPlugins(str(_hdNSI))
except ImportError:
    pass
)WOOF";

/*
    The purpose of this is to register our HydraNSI plugin with the USD plugin
    system. We do it with a python script because that's the easiest way to get
    to the required USD APIs.
*/
void HydraNSI::RegisterPlugin()
{
#ifdef _WIN32
    const char *module_names[] =
    {
        "python311.dll",
        "python310.dll",
        "python39.dll",
        "python37.dll",
        "python27.dll",
    };
    void *sym = nullptr;
    for( const char *name : module_names )
    {
        HMODULE mod = GetModuleHandle(name);
        if( NULL == mod )
            continue;
        sym = GetProcAddress(mod, "PyRun_SimpleStringFlags");
        if( sym )
            break;
    }
#else
    void *sym = dlsym(RTLD_DEFAULT, "PyRun_SimpleStringFlags");
#endif
    auto pyrun = (int(*)(const char *, void*))sym;
    if( pyrun )
    {
        pyrun(registration_script, nullptr);
    }
    else
    {
        fprintf(stderr, "3Delight: Unable to register Hydra plugin.\n");
    }
}
// vim: set softtabstop=4 expandtab shiftwidth=4:
