/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2016                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include <algorithm>
#include <cstring>
#include <stdio.h>
#include <string>
#include <vector>
#include <XGen/XgRenderAPI.h>
#include <XGen/XgRenderAPIUtils.h>

#ifdef _WIN32
#include <windows.h>
#else
#define HMODULE void*
#endif

using namespace XGenRenderAPI;

struct Archive
{
	std::string name;

	int npolys;
	std::vector<int> nvertices;
	std::vector<int> vertices;
	std::vector<float> points;
	std::vector<float> normals;

	float bbox[6];

	bool valid;
};

struct Hair
{
	std::string file;
	std::string palette;
	std::string desc;
	std::string patch;
	std::string geom;
	int frame;

	std::vector<vec3> points;
	std::vector<int> npoints;
	std::vector<float> random;

	std::vector<int> archives;
	std::vector<mat44> transforms;
	std::vector<float> archive_randoms;

	float bbox[6];

	bool valid;
};

class RendermanCallbacks : public ProceduralCallbacks
{
public:
	RendermanCallbacks(Hair& i_cache);
	~RendermanCallbacks();

	/* XGen asks the Procedural Callback to flush the current primitive cache */
	virtual void flush(  const char* in_geom, PrimitiveCache* in_cache );

	/* XGen will log messages using the following callback */
	virtual void log( const char* in_str );

	/* XGen can query data using the get functions. The get methods should
	   return global render option or render attributes from the current
	   rendered scene. */
	virtual bool get( EBoolAttribute ) const;
	virtual const char* get( EStringAttribute i_attr ) const;
	virtual float get( EFloatAttribute ) const;
	virtual const float* get( EFloatArrayAttribute ) const;
	virtual unsigned int getSize( EFloatArrayAttribute ) const;

	/* A user render attribute can override the expression of an xgen attribute.
	   For example, if you code this method to return “2.0*( $originalExpr )”
	   when in_name==”length” then length is going to be multiply by 2. Return
	   an override expression for the in_name attribute. Return NULL to avoid
	   overriding the in_name attribute. */
	virtual const char* getOverride( const char* in_name ) const;

	/* Returns the bounding box for the archive file in out_bbox. ( min_x,
	   max_x, min_y, max_y, min_z, max_z ).  Returns true if it succeeded, false
	   otherwise. */
	virtual bool getArchiveBoundingBox(
			const char* in_filename, bbox& out_bbox ) const;

	/* Returns the current object transform in world space at the given
	   normalized shutter time */
	virtual void getTransform( float in_time, mat44& out_mat ) const;

private:
	void FlushSplines( const char* in_geom, PrimitiveCache* in_cache );
	void FlushCards( const char* in_geom, PrimitiveCache* in_cache );

	Hair& m_cache;

	float m_shutter;
};

class XGenLoader
{
public:
	/* Loads the library and the symbols */
	static bool Setup(const std::string& i_xgen_dir);

	/* Checks if we already have the object in the cache. And if not, it
	   generates the geometry with XGen. */
	static int GetGeoID(
			const std::string& i_file,
			const std::string& i_palette,
			const std::string& i_desc,
			const std::string& i_patch,
			const std::string& i_geom,
			int i_frame);

	static void GetBBox(int id, double o_bbox[6]);
	static const Hair& GetGeo(int id);
	/* Clear the hair from the cache */
	static void Clear(int id);
	static void Clear(
			const std::string& i_file,
			const std::string& i_palette,
			const std::string& i_desc,
			const std::string& i_patch,
			const std::string& i_geom );

	static int GetArchiveID(
			const std::string& i_archive);
	static Archive& GetArchive(int id);

private:
	/* The cache. All the loaded geo is here. */
	static std::vector<Hair> gs_cache;
	static std::vector<Archive> gs_archives;

public:
	/* XGen external libraries and symbols */
	static HMODULE gs_libPtex;
	static HMODULE gs_libXGen;
	static PatchRenderer* (*gs_patchrender_init)(
			ProceduralCallbacks*, char const*);
	static FaceRenderer* (*gs_facerender_init)(
			PatchRenderer*, unsigned int, ProceduralCallbacks*);
};

