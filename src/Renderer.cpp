/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2016                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "Renderer.h"

#include "AttributeUtils.h"
#include "ExportContext.h"
#include "ExportCamera.h"
#include "ExportGeo.h"
#include "ExportMaterial.h"
#include "ExportOptions.h"
#include "ExportOutputs.h"
#include "ExportSG.h"
#include "KatanaMonitorDriver.h"
#include "LogReporting.h"
#include "SGLocation.h"
#include "System.h"
#include "utils.h"

#include <3Delight/ProcessingInfo.h>
#include <ndspy.h>

#include <chrono>
#include <mutex>
#include <thread>
#include <stdio.h>

#include <FnAPI/FnAPI.h>
#include <FnAsset/FnDefaultAssetPlugin.h>
#include <FnAsset/FnDefaultFileSequencePlugin.h>
#include <FnAttribute/FnConstVector.h>
#include <FnConfig/FnConfig.h>
#include <FnRendererInfo/plugin/RenderMethod.h>

using namespace FnKat;

Renderer::Renderer(
		FnKat::FnScenegraphIterator rootIterator,
		FnAttribute::GroupAttribute arguments )
:
	RenderBase( rootIterator, arguments ),
	m_is_live_editing( false ),
	m_has_pending_updates( false ),
	m_export_ctx( 0x0 )
{
	m_render_method = getRenderMethodName();
	// For batch mode, getRenderMethodName is useless, need to deal with this:
	// See Recognized Environment Variables in KATANA On Line Help
	if( getenv("KATANA_BATCH_MODE") )
	{
		m_render_method = FnKat::RendererInfo::DiskRenderMethod::kBatchName;
	}
}

Renderer::~Renderer()
{
	assert( !m_export_ctx );
	delete m_export_ctx;

	if( m_sequence_context )
	{
		/* We've sent the last frame. */
		m_sequence_context->RenderControl(
			NSI::CStringPArg("action", "endsequence"));
		/* Wait for rendering to complete. */
		m_sequence_context->RenderControl(
			NSI::CStringPArg("action", "wait"));
		m_sequence_context->End();
		m_sequence_context.reset();
	}
}

/*
	Create a sequence context to export a sequence of frames.

	This needs the --reuse-render-process command line option to be of any real
	use.
*/
void Renderer::DelayedCreateSequenceContext()
{
	/*
		Run this code just once. It is not called from the constructor because
		it is wasteful to create a sequence context for instances of the plugin
		which don't end up rendering anything. And there are a lot of these.
	*/
	if( m_sc_created )
		return;
	m_sc_created = true;

	/* This is only used for batch mode rendering to the cloud. */
	if( IsBatch() &&
	    (getenv("_3DELIGHT_FORCE_CLOUD") || DlIsRenderUsingCloud()) )
	{
		/* Create sequence context. */
		NSI::DynamicArgumentList args;
		args.Add(new NSI::IntegerArg("createsequencecontext", 1));
		/* As in start(). */
		args.Add(new NSI::IntegerArg( "readpreferences", 1 ));
		/* Cloud options also need to be set on sequence. */
		AddCloudArgs(args);

		m_sequence_context.reset(new NSI::Context);
		m_sequence_context->Begin(args);
	}
}

int Renderer::start()
{
	//printf("--start\n");

	DelayedCreateSequenceContext();

	RegisterKatanaMonitorDriver();

	Utils::ClearBoundingSphereCache();

	auto export_start = std::chrono::high_resolution_clock::now();
	FnScenegraphIterator rootIterator = getRootIterator();

	std::string rib_file = getRenderOutputFile();

	NSI::DynamicArgumentList beginArgs;

	if( rib_file != "" )
	{
		beginArgs.Add(
			new NSI::CStringPArg( "streamfilename", rib_file.c_str() ) );
	}
	else
	{
		beginArgs.Add(
			new NSI::IntegerArg( "readpreferences", 1 ) );
	}

	/* If we're rendering a sequence, add this frame to it. */
	if( m_sequence_context )
	{
		int h = m_sequence_context->Handle();
		beginArgs.Add(new NSI::IntegerArg("sequencecontext", h));
	}

	/* Set cloud option if needed */
	AddCloudArgs(beginArgs);

	/* This is useful if you want to attach gdb to renderboot process. While
	   sleeping, use : gdb --pid `ps -C renderboot -o pid --no-headers` */
	//std::this_thread::sleep_for( std::chrono::seconds( 10 ) );

	m_export_ctx = new ExportContext( beginArgs, rootIterator, *this );

	/* Export notes */
	ExportOptions::ExportRenderNotes(*m_export_ctx);

	/* Export rendering options */
	ExportOptions::ExportOptions(*m_export_ctx);

	/* Init ID sender. */
	ExportOutputs::InitIDPass( *m_export_ctx );

	/* Export atmosphere object. */
	ExportGeo::ExportAtmosphere( *m_export_ctx, rootIterator );

	/*
		Export the scene. Handling only /root/materials and /root/world is an
		artificial restriction, mostly because it has always been this way.
		Most live render filters are also restricted to /root/world and would
		need to be changed if this export is changed to just "/root".
	*/
	ExportSG::ExportSGSubtree(
		*m_export_ctx, rootIterator.getChildByName( "materials" ) );
	ExportSG::ExportSGSubtree(
		*m_export_ctx, rootIterator.getChildByName( "world" ) );

	ExportOutputs::ExportAllOutputs( *m_export_ctx, rootIterator );

	/*
		We don't flush the cache at every traversal step so give it a flush
		here to make sure we don't start rendering with too much memory wasted
		in Katana's cache. I do not know a proper API for this and getting an
		invalid iterator or the same one by name does not do it either.
	*/
	rootIterator.getFirstChild( true );

	NSI::DynamicArgumentList renderArgs;
	renderArgs.Add( new NSI::CStringPArg( "action", "start" ) );
	if( IsLive() )
	{
		renderArgs.Add( new NSI::IntegerArg( "interactive", 1 ) );
	}
	else if( IsPreview() )
	{
		std::string value =
			FnConfig::Config::get("3Delight/progressiveRefinement");
		int progressive = value == "True" ? 1 : 0;
		renderArgs.Add( new NSI::IntegerArg( "progressive", progressive ) );
	}

	auto export_end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<float> export_time = export_end - export_start;
	printf( "3DL INFO Scene Graph traversal done in %.2f s\n", export_time.count() );

	m_export_ctx->m_nsi.RenderControl( renderArgs );

#if KATANA_VERSION_MAJOR > 3 \
    || (KATANA_VERSION_MAJOR == 3 && KATANA_VERSION_MINOR >= 5)
	/* This is supposed to make all iterators invalid. Beware. */
	rootIterator.finalizeRuntime();
#endif

	if(!IsLive())
	{
		m_export_ctx->m_nsi.RenderControl(NSI::CStringPArg("action", "wait"));
	}

	//printf("--start complete (initial export is done)\n");

	return 0;
}

int Renderer::stop()
{
	//printf("--stop\n");
	if( IsDisk() || IsBatch() )
		WriteInfoAboutFilePathNames();
	
	if( !m_export_ctx )
		return 1;

	delete m_export_ctx;
	m_export_ctx = 0x0;

	return 0;
}

int Renderer::pause()
{
	if( !m_export_ctx )
		return 1;

	// TODO: call RenderControl

	return 0;
}

int Renderer::resume()
{
	if( !m_export_ctx )
		return 1;

	// TODO: call RenderControl

	return 0;
}

int Renderer::startLiveEditing()
{
	//printf("--startLiveEditing\n");

	if( !m_export_ctx )
		return 1;

	m_is_live_editing = true;

	return 0;
}

int Renderer::stopLiveEditing()
{
	//printf("--stopLiveEditing\n");

	if( !m_export_ctx )
		return 1;

	m_is_live_editing = false;

	/*
		I'm not sure if we should delete the context here. I can't reach this
		function anyway, cancelling a live render in katana just kills the
		renderboot process and I don't know of another way to stop it.
	*/
	return 0;
}

/*
	I think this is some sort of extension mechanism. We don't use it.
*/
int Renderer::processControlCommand(const std::string & command)
{
	//printf("--processControlCommand '%s'\n", command.c_str());
	return 0;
}

/*
	This is used to sort updates in the order we must apply them.

	For example, when adding new geo, we'll get at least 3 updates:
	- The geo.
	- The transform (the matrix itself).
	- The attributes.

	We must apply the geo update first or the attributes won't have a node to
	connect to and the xform won't have a node to be set onto.

	Also, if there are updates for an entire hierarchy, we could use this to
	apply parents before children. This does not seem required so far.

	We reverse the order when deleting, although it probably does not matter
	that much in practice.
*/
struct UpdatePriority
{
	int m_child_idx;
	int m_priority;

	bool operator<( const UpdatePriority &rhs ) const
	{
		return m_priority < rhs.m_priority;
	}

	UpdatePriority(
		int i_idx,
		const FnAttribute::GroupAttribute &i_update )
	:
		m_child_idx( i_idx ),
		m_priority( 100 )
	{
		if( !i_update.isValid() )
			return;

		std::string type;
		AttributeUtils::GetString( i_update, "type", type );

		/* Increase priority of updates which might add geo. */
		if( type == "light" )
			m_priority = 90;
		else if( type == "polymesh" || type == "subdmesh" )
			m_priority = 90;
		else if( type == "openvdb" || type == "yeti" )
			m_priority = 90;
		else if( type == "XGen" )
			m_priority = 90;
		else if( type == "instance array" || type == "curves" ||
			type == "pointcloud" )
		{
			m_priority = 90;
		}
		else if( type == "camera" )
			m_priority = 90;
		else if( type == "group" || type == "assembly" || type == "component" )
			m_priority = 80;
		else if( type == "rig" )
			m_priority = 80;
		else if( type == "instance source" || type == "usd point instancer" )
			m_priority = 80;
		/*
			Decrease priority of render settings update so the new camera is
			created before the camera change is applied.
		*/
		else if( type == "renderSettings" )
			m_priority = 110;

		/* Reverse priority for deletion. */
		int deleted = 0;
		AttributeUtils::GetInteger( i_update, "attributes.deleted", deleted );
		if( deleted != 0 )
		{
			m_priority = -m_priority;
		}
	}
};

/*
	This is called by katana when a live render update happens.

	Note that these updates are not applied to the view of the scene graph we
	get through getRootIterator().

	The group may contain several subgroups, each representing a change at a
	different location in the scene graph.

	The name of each subgroup is sometimes the scene graph path but not always.
	It is usually a mangled version of it, with the / replaced by _ and the
	type appended. It is probably useless. It contains these attributes:
	- location : A string with the scene graph location of the update.
	- type : A string with the type of the location or the "typeAlias" of the
	  LiveRenderFilter which triggered this update.
	- attributes : A group which contains the attributes normally present on
	  the scene graph location and listed in the LiveRenderFilter's
	  "attributeNames" list. If the update is a deletion, this contains a
	  single int attribute named "deleted" with value 1.
*/
int Renderer::queueDataUpdates(FnAttribute::GroupAttribute updateAttribute)
{
	/*
		We could handle several queueDataUpdates() calls in ||, just like we do
		|| export. But applyPendingDataUpdates() must not run while we're in
		here. NSI does not allow it and even if it did, it could lead to
		updates being partially applied and the output being nonsense.
	*/
	std::shared_lock update_lock(m_update_mutex);

	if( !m_export_ctx )
		return 1;

	/*
		We must check this as katana sends some updates while start() is
		still executing, before startLiveEditing() is called. We can't apply
		them because we don't have a scene to apply them to. They are useless
		anyway: they contain the same data we're currently exporting.
	*/
	if( !m_is_live_editing )
		return 1;

	//ExportSG::ListAttributes( updateAttribute, "update", 1 );

	/* Reorder the updates. See UpdatePriority above. */
	std::vector<UpdatePriority> updates;
	updates.reserve( updateAttribute.getNumberOfChildren() );
	for( int c = 0; c < updateAttribute.getNumberOfChildren(); ++c )
	{
		updates.push_back(
			UpdatePriority( c, updateAttribute.getChildByIndex( c ) ) );
	}
	std::stable_sort( updates.begin(), updates.end() );

	/* Export the updates. */
	for( size_t c = 0; c < updates.size(); ++c )
	{
		FnAttribute::GroupAttribute update_item(
			updateAttribute.getChildByIndex( updates[c].m_child_idx ) );

		ExportSG::ExportLiveUpdateItem( *m_export_ctx, update_item );
	}

	m_has_pending_updates = true;
	return 0;
}

int Renderer::applyPendingDataUpdates()
{
	std::unique_lock update_lock(m_update_mutex);

	if( !m_export_ctx )
		return 1;

	m_has_pending_updates = false;

	m_export_ctx->m_nsi.RenderControl(
		(
			NSI::CStringPArg( "action", "synchronize" )
		) );

	return 0;
}

bool Renderer::hasPendingDataUpdates() const
{
	return m_has_pending_updates;
}

void Renderer::configureDiskRenderOutputProcess(
		FnKatRender::DiskRenderOutputProcess& diskRenderOutputProcess,
		const std::string& outputName,
		const std::string& outputPath,
		const std::string& renderMethodName,
		const float& frameTime) const
{
	/* Apply tile prefix to output path. Nop if tile rendering is not used. */
	std::string location = RenderOutputUtils::buildTileLocation(
		getRootIterator(), outputPath );

	/* Basic action: render to target location. */
	FnKatRender::RenderAction *render_action =
		new FnKatRender::RenderAction( location );

	/* The render action used for this render output */
	diskRenderOutputProcess.setRenderAction(
		FnKatRender::DiskRenderOutputProcess::RenderActionPtr(
			render_action ) );
}

bool Renderer::IsLive() const
{
	return m_render_method ==
			FnKat::RendererInfo::LiveRenderMethod::kDefaultName;
}

bool Renderer::IsPreview() const
{
	return m_render_method ==
			FnKat::RendererInfo::PreviewRenderMethod::kDefaultName;
}

bool Renderer::IsDisk() const
{
	return m_render_method ==
			FnKat::RendererInfo::DiskRenderMethod::kDefaultName;
}

bool Renderer::IsBatch() const
{
	return m_render_method ==
			FnKat::RendererInfo::DiskRenderMethod::kBatchName;
}

void Renderer::AddCloudArgs(NSI::DynamicArgumentList& o_beginArgs)
{
	const char *katana[] = { "KATANA" };
	const char *katana_batch[] = { "KATANA_BATCH" };
	const char *katana_live[] = { "KATANA_LIVE" };
	const char **software = katana;

	if( getenv("_3DELIGHT_FORCE_CLOUD") )
	{
		o_beginArgs.Add( new NSI::IntegerArg( "cloud", 1 ) );
	}

	if( IsBatch() )
	{
		software = katana_batch;
	}
	else if( IsLive() )
	{
		software = katana_live;
	}

	o_beginArgs.Add(
		new NSI::CStringPArg( "software", *software ) );

	const char* info = getenv("DLC_KATANA_INFO");
	if( info && IsBatch() )
	{
		o_beginArgs.Add(new NSI::StringArg("sequenceid", info));
	}
}

void Renderer::WriteInfoAboutFilePathNames()
{
	printf("\n");
	typedef std::multimap<std::string,std::string> mm;
	mm filenames;
	std::vector<std::string> roNames =
		m_export_ctx->m_renderSettings.getRenderOutputNames();
	for (std::vector<std::string>::const_iterator it = roNames.begin();
		it != roNames.end(); ++it)
	{
		RenderSettings::RenderOutput ro =
			m_export_ctx->m_renderSettings.getRenderOutputByName(*it);
		filenames.insert(std::pair<std::string,std::string>(ro.renderLocation, *it));
	}
	// Writes which layers are saved on the same file.
	std::string key;
	for (mm::iterator it = filenames.begin(); it != filenames.end(); ++it)
	{
		if ((*it).first == key)
			continue;
		key = (*it).first;
		size_t nb = filenames.count(key);
		std::pair<mm::iterator, mm::iterator> ret = filenames.equal_range(key);
		printf("%s", (nb>1) ? "Image layers " : "Image layer ");
		size_t n = 0;
		for (mm::iterator it2 = ret.first; it2 != ret.second; ++it2)
		{
			n++;
			printf("\"%s\"%s ", it2->second.c_str(), (n<nb) ? "," : "");
		}
		std::string fullpathname = System::FullPathName(key);
		printf(
			"%s saved as \"%s\".\n",
			(nb>1) ? "are" : "is",
			fullpathname.c_str());
	}
}
