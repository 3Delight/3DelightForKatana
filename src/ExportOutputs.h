/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __ExportOutputs_h
#define __ExportOutputs_h

class ExportContext;

#include "FnScenegraphIterator/FnScenegraphIterator.h"

#include <string>
#include <vector>

namespace ExportOutputs
{
	void ExportAllOutputs(
		ExportContext &i_ctx,
		const FnKat::FnScenegraphIterator &i_root );

	bool WantIDPass(
		ExportContext &i_ctx );

	void InitIDPass(
		ExportContext &i_ctx );

	void ExportIDPassOutput(
		ExportContext &i_ctx,
		const FnKat::FnScenegraphIterator &i_root );

	void ExportOneOutput(
		ExportContext &i_ctx,
		const FnKat::FnScenegraphIterator &i_root,
		const std::string &i_output_name,
		const std::string &i_light_name,
		const std::string &i_feedback_data,
		int& io_sort_key );

	/* This specifies which of the possible output drivers to produce. */
	enum eOutputDriver
	{
		od_file,
		od_idisplay,
		od_katana_monitor,
		od_jpeg
	};

	void ExportOutputDriverForLayer(
		ExportContext &i_ctx,
		const FnKat::FnScenegraphIterator &i_root,
		const std::string &i_output_name,
		const std::string &i_layer_handle,
		eOutputDriver i_driver );

	std::string ProcessOutputFileName(
		ExportContext &i_ctx,
		const std::string &i_filename );

	void GetLight(
		ExportContext &i_ctx,
		const FnKat::FnScenegraphIterator &i_root,
		std::string i_output_name,
		std::string& o_light_set,
		std::string& o_feedback_data );

	void AppendLightFeedbackData(
		FnKat::FnScenegraphIterator& i_light,
		std::string& o_objects,
		std::string& o_attributes,
		std::string& o_values,
		std::string& o_color_attributes,
		std::string& o_color_values );
}

#endif
