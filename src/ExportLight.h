/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __ExportLight_h
#define __ExportLight_h

#include "SGLocation.h"

class ExportContext;

/*
	This namespace contains code to export lights.
*/
namespace ExportLight
{
	void ExportLightLocation(
		ExportContext &i_ctx,
		const SGLocation &i_location );
}

#endif
