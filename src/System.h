#ifndef __System_h
#define __System_h

#include <string>

namespace System
{

/**
	\returns The absolute path to the dynamic library which contains this
	function.
*/
std::string LibraryPath();

/**
	\returns The login name of the user running the process or empty string
	if it can't be retrieved.
*/
std::string UserLoginName();

/*
	\brief Returns an absolute path to a file or directory.

	This function resolves:
	- "." (uses current directory)
	- ".."
	- symbolic links (unix only)
	- "\" (windows only, uses current drive)

	It does not resolve short (8.3) file names (windows only).

	\param i_path
		The path to resolve.
	\returns
		The resolved path or an empty string if there is an error.

*/
std::string FullPathName( const std::string &i_path );

/**
	\param i_path
		The path to check.
	\returns
		True if the given path is a directory.
*/
bool FileIsDir( const char *i_path );

/**
	Calls the given function with each files found within the given directory.
	The function is not called for directories found within the directories.

	\param i_path
		name of the directory to be treated.
	\param User_Fct
		User function to call for each file found.
	\param User_Arg
		An argument to the user function. Can be NULL.
	\param i_recurse
		If true, a recursive scan in subdirectories is done.

	\returns
		true if successful, false if there is an error reading the directory

	NOTES
	The function is called the following way:

		User_Fct( File_Path, File_Name, User_Arg );

	where 'File_Path' is the complete path to file found while scanning the
	directory and 'File_Name' is only the name of the file.
*/
bool ScanDir(
	const char *i_path,
	void (*i_userFct)(const char*, const char*, void *),
	void *i_userArg,
	bool i_recurse=false );

} /* namespace System */

#endif
