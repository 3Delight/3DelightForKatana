/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2016                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "HydraNSI.h"
#include "Renderer.h"
#include "RendererInfo.h"
#include "LightLocator.h"
#include "utils.h"
/* 3Delight header to get version number. */
#include "delight.h"

#ifdef ENABLE_VMP
#	include "LightViewerModifier.h"
#	include "LightFilterViewerModifier.h"
#	include "LightFilterReferenceViewerModifier.h"
#	include "XGenViewerModifier.h"
#endif

// Plugin Registration code
DEFINE_RENDERERINFO_PLUGIN(RendererInfo)
DEFINE_RENDER_PLUGIN( Renderer )
#ifdef ENABLE_VMP
DEFINE_VMP_PLUGIN(XGenViewerModifier)
DEFINE_VMP_PLUGIN(LightViewerModifier)
DEFINE_VMP_PLUGIN(LightFilterReferenceViewerModifier)
DEFINE_VMP_PLUGIN(LightFilterViewerModifier)
#endif
namespace Foundry
{
    namespace Katana
    {
        namespace ViewerUtils
        {
            DEFINE_VIEWPORT_LAYER_PLUGIN(FnBaseLocatorViewportLayer);
        }  // namespace ViewerUtils
    }  // namespace Katana
}  // namespace Foundry

DEFINE_VIEWER_DELEGATE_COMPONENT_PLUGIN(LightLocatorVDC);

void registerPlugins()
{
	REGISTER_PLUGIN(
		RendererInfo, _3DELIGHT_RENDERER_NAME "RendererInfo", 0, 1);
	REGISTER_PLUGIN( Renderer, _3DELIGHT_RENDERER_NAME, 0, 1 );
#ifdef ENABLE_VMP
	REGISTER_PLUGIN(
		XGenViewerModifier,
		_3DELIGHT_RENDERER_NAME "XGenViewerModifier", 0, 1);
	REGISTER_PLUGIN(
		LightViewerModifier,
		_3DELIGHT_RENDERER_NAME "LightViewerModifier", 0, 1);
	REGISTER_PLUGIN(
		LightFilterViewerModifier,
		_3DELIGHT_RENDERER_NAME "LightFilterViewerModifier", 0, 1);
	REGISTER_PLUGIN(
		LightFilterReferenceViewerModifier,
		_3DELIGHT_RENDERER_NAME "LightFilterReferenceViewerModifier", 0, 1);
#endif

    REGISTER_PLUGIN( LightLocatorVDC, "LightLocatorVDC", 0, 1);

    REGISTER_PLUGIN(
    	Foundry::Katana::ViewerUtils::FnBaseLocatorViewportLayer,
        "LightLocatorViewportLayer", 0, 1);

	REGISTER_LOCATOR(LightLocator);

	HydraNSI::RegisterPlugin();

	std::string message = "3Delight for Katana (" __DATE__ ") loaded, using ";
	message += DlGetLibNameAndVersionString();
	printf("[INFO 3Delight]: %s\n", message.c_str());
}
