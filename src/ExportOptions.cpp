/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "ExportOptions.h"

#include "AttributeUtils.h"
#include "ExportCamera.h"
#include "ExportContext.h"
#include "ExportMaterial.h"
#include "ExportOutputs.h"
#include "Renderer.h"
#include "System.h"

#include <FnConfig/FnConfig.h>

#include <algorithm>
#include <iomanip>
#include <sstream>

namespace ExportOptions
{

void ExportRenderNotes( ExportContext &i_ctx )
{
	/* DLC_KATANA_INFO is set by event handler onRenderStarted in NodeGraph */
	const char* info = getenv("DLC_KATANA_INFO");
	std::string name = info ? info : "No scene name | unknown settings | ";

	float time = i_ctx.m_renderer.getRenderTime();
	std::ostringstream frame_buf;
	frame_buf << std::setw(4) << std::setfill('0') << static_cast<int>(time);
	std::string notes = name + " | Frame " + frame_buf.str();

	i_ctx.m_nsi.SetAttribute( NSI_SCENE_GLOBAL, (
		NSI::StringArg("statistics.notes", notes),
		NSI::StringArg("statistics.project", name),
		NSI::StringArg("statistics.frameid", frame_buf.str())) );
}

/**
	\brief Export rendering options.

	Export the various options that are not tied to a specific scene object.
*/
void ExportOptions(	ExportContext &i_ctx )
{
	/* Low priorty for non batch mode */
	if( !i_ctx.m_renderer.IsBatch() )
	{
		i_ctx.m_nsi.SetAttribute( NSI_SCENE_GLOBAL,
			NSI::IntegerArg("renderatlowpriority", 1) );
	}

	/* Show progress in the render log. */
	i_ctx.m_nsi.SetAttribute( NSI_SCENE_GLOBAL,
		NSI::IntegerArg("statistics.progress", 1) );

	bool interactiveRender =
		i_ctx.m_renderer.IsPreview() || i_ctx.m_renderer.IsLive();

	/* Shading Samples */
	int shadingsamples = i_ctx.m_renderSettings.GetShadingSamples();
	if( interactiveRender )
	{
		shadingsamples =
			std::max(1, (int)(shadingsamples *
					i_ctx.m_renderSettings.GetSamplingReduceFactor()+0.5));
	}
	i_ctx.m_nsi.SetAttribute( NSI_SCENE_GLOBAL,
		NSI::IntegerArg("quality.shadingsamples", shadingsamples));

	/* Volume Samples */
	int volumesamples = i_ctx.m_renderSettings.GetVolumeSamples();
	if( interactiveRender )
	{
		volumesamples =
			std::max(1, (int)(volumesamples *
					i_ctx.m_renderSettings.GetSamplingReduceFactor()+0.5));
	}
	i_ctx.m_nsi.SetAttribute( NSI_SCENE_GLOBAL,
		NSI::IntegerArg("quality.volumesamples", volumesamples));

	NSI::ArgumentList nsi_attributes;

	if( interactiveRender )
	{
		/* Show Displacement */
		nsi_attributes.push( new NSI::IntegerArg( "show.displacement", 
			i_ctx.m_renderSettings.GetEnableDisplacement() ) );

		/* Show OSL Subsurface */
		nsi_attributes.push( new NSI::IntegerArg( "show.osl.subsurface", 
			i_ctx.m_renderSettings.GetEnableSubsurface() ) );
	}

	/* Max Depth */
	nsi_attributes.push( new NSI::IntegerArg( "maximumraydepth.diffuse", 
		i_ctx.m_renderSettings.GetDiffuseDepth() ) );

	nsi_attributes.push( new NSI::IntegerArg( "maximumraydepth.reflection", 
		i_ctx.m_renderSettings.GetReflectionDepth() ) );

	nsi_attributes.push( new NSI::IntegerArg( "maximumraydepth.refraction", 
		i_ctx.m_renderSettings.GetRefractionDepth() ) );

	nsi_attributes.push( new NSI::IntegerArg( "maximumraydepth.hair", 
			i_ctx.m_renderSettings.GetHairDepth() ) );

	/* Max Ray Length */
	double max_ray_length = i_ctx.m_renderSettings.GetMaxRayLength();
	nsi_attributes.push( new NSI::DoubleArg( "maximumraylength.diffuse", 
		max_ray_length ) );

	nsi_attributes.push( new NSI::DoubleArg( "maximumraylength.specular", 
		max_ray_length ) );

	nsi_attributes.push( new NSI::DoubleArg( "maximumraylength.reflection", 
		max_ray_length ) );

	nsi_attributes.push( new NSI::DoubleArg( "maximumraylength.refraction", 
		max_ray_length ) );

	nsi_attributes.push( new NSI::DoubleArg( "maximumraylength.hair", 
		max_ray_length ) );

	i_ctx.m_nsi.SetAttribute( NSI_SCENE_GLOBAL, nsi_attributes );
}

/**
	\brief Handles a live update to render settings.

	This receives the region of interest. Katana appears to always send
	updates to this, regardless of what we do in RendererInfo. I could not get
	proper updates elsewhere either, which is why we have this weird system to
	update all cameras.

	It also receives camera changes when the "Live Render from Viewer Camera"
	pin is toggled in the viewer. This is hardcoded into Katana. Note that a
	camera change requires that we stop the render, change the connections and
	then restart the render. This is a limitation of 3Delight which should
	really be fixed (it should handle the restart itself).
*/
void UpdateRenderSettings(
	ExportContext &i_ctx,
	const SGLocation &i_location )
{
	FnAttribute::Attribute ROI_attr =
		i_location.getAttribute( "renderSettings.ROI" );
	if( ROI_attr.isValid() )
	{
		i_ctx.m_renderSettings.UpdateROI( ROI_attr );

		/* Apply this to all screens. */
		ExportContext::tScreenList::const_iterator
			it = i_ctx.ScreenList().begin(),
			end = i_ctx.ScreenList().end();
		for( ; it != end; ++it )
		{
			ExportCamera::ExportPriorityWindow( i_ctx, *it );
		}
	}

	FnAttribute::StringAttribute cameraName_attr =
		i_location.getAttribute( "renderSettings.cameraName" );
	if( cameraName_attr.isValid() )
	{
		std::string newCam = cameraName_attr.getValue();
		std::string previousCam = i_ctx.m_renderSettings.UpdateCamera( newCam );
		/*
			This check is important: we receive updates every time the viewer
			camera moves, even if it remains the viewer camera.
		*/
		if( newCam != previousCam )
		{
			i_ctx.m_nsi.RenderControl( NSI::CStringPArg( "action", "stop" ) );

			i_ctx.ChangeScreenForLayers(
				previousCam + "|screen", newCam + "|screen" );

			i_ctx.m_nsi.RenderControl( (
					NSI::CStringPArg( "action", "start" ),
					NSI::IntegerArg( "interactive", 1 )
				) );
		}
	}
}

}
