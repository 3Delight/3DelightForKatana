/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "GlobalSettings.h"

#include "utils.h"

#include <FnConfig/FnConfig.h>

#include <stdlib.h>

GlobalSettings::GlobalSettings(
	const FnKat::FnScenegraphIterator &i_rootIterator )
:
	FnKat::Render::GlobalSettings( i_rootIterator, _3DELIGHT_RENDERER_NAME )
{
	/*
		Default to katana monitor for preview/live renders before a DlSettings
		node. I think this is the expected behavior in katana.
	*/
	int which_fb = 1;
	if( FnConfig::Config::has("3Delight/renderView") )
	{
		std::string value =	FnConfig::Config::get("3Delight/renderView");
		which_fb = atoi(value.c_str());
	}
	m_want_idisplay = which_fb == 0 || which_fb == 2;
	m_want_monitor = which_fb == 1 || which_fb == 2;

}
