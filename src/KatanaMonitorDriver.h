#ifndef __KatanaMonitorDriver_h
#define __KatanaMonitorDriver_h

inline const char* KatanaMonitorDriverName()
	{ return "katana_render"; }

void RegisterKatanaMonitorDriver();

#endif
