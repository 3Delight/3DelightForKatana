/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __LightFilterViewerModifier_h
#define __LightFilterViewerModifier_h

#include <FnViewerModifier/plugin/FnViewerModifier.h>

class LightFilterViewerModifier : public FnKat::ViewerModifier
{
public:
	/* DEFINE_VMP_PLUGIN expects these. */
	static FnKat::ViewerModifier* create( FnKat::GroupAttribute args );
	static FnKat::GroupAttribute getArgumentTemplate();
	static const char* getLocationType();
	static void flush();
	static void onFrameBegin();
	static void onFrameEnd();


	LightFilterViewerModifier( FnKat::GroupAttribute args );

	/* Required virtual methods. */
	virtual void setup( FnKat::ViewerModifierInput& input );
	virtual void cleanup( FnKat::ViewerModifierInput& input );

	virtual void deepSetup( FnKat::ViewerModifierInput& input );
	virtual void draw( FnKat::ViewerModifierInput& input );
	virtual void deepCleanup( FnKat::ViewerModifierInput& input );

	/* Optional virtual methods. */
	FnKat::DoubleAttribute getLocalSpaceBoundingBox(
		FnKat::ViewerModifierInput& input );

protected:
	void FilterSetup(
		FnKat::ViewerModifierInput &input,
		bool is_reference,
		const FnAttribute::GroupAttribute &i_material );

private:
	void drawGobo();

protected:
	enum eType
	{
		type_unknown, type_gobo
	};

	eType m_type;
};

#endif
