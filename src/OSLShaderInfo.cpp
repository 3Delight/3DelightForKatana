/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "OSLShaderInfo.h"

#include <algorithm>

bool OSLShaderInfo::open( const char *i_name )
{
	DlShaderInfo *info = DlGetShaderInfo( i_name );
	if( !info )
		return false;

	DlShaderInfo::operator=(*info);

	/* Find 'tags' string array shader metadata. */
	for( unsigned i = 0; i < metadata().size(); ++i )
	{
		if( metadata()[i].type.IsStringArray() &&
			metadata()[i].name == "tags" )
		{
			for( unsigned j = 0; j < metadata()[i].sdefault.size(); ++j )
				m_tags.push_back( metadata()[i].sdefault[j].c_str() );
		}
	}

	/* Sort our copy for binary search. */
	std::sort( m_tags.begin(), m_tags.end() );

	return true;
}

bool OSLShaderInfo::HasTag( const std::string &i_tag ) const
{
	return std::binary_search( m_tags.begin(), m_tags.end(), i_tag );
}
