/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2016                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __renderer_h
#define __renderer_h 

#include <FnRender/plugin/RenderBase.h>
#include <FnRender/plugin/CameraSettings.h>
#include <FnRender/plugin/GlobalSettings.h>

#include <FnRenderOutputUtils/FnRenderOutputUtils.h>

#include "RenderSettings.h"
#include "nsi.hpp"

#include <memory>
#include <shared_mutex>

class ExportContext;

namespace FnKatRender = Foundry::Katana::Render;

class Renderer : public Foundry::Katana::Render::RenderBase
{
	public:
		Renderer(
				FnKat::FnScenegraphIterator rootIterator,
				FnAttribute::GroupAttribute arguments );

		virtual ~Renderer();

		void DelayedCreateSequenceContext();

		virtual int start();
		virtual int stop();

		virtual int pause();
		virtual int resume();

		virtual int startLiveEditing();
		virtual int stopLiveEditing();

		virtual int processControlCommand(const std::string & command);

		virtual int queueDataUpdates(FnAttribute::GroupAttribute updateAttribute);
		virtual int applyPendingDataUpdates();
		virtual bool hasPendingDataUpdates() const;

		static Foundry::Katana::Render::RenderBase* create(
				FnKat::FnScenegraphIterator rootIterator,
				FnAttribute::GroupAttribute args )
		{
			return new Renderer( rootIterator, args );
		}
		static void flush() {};

		/**
		   Provide Katana with information on how to process a given render
		   output (AOV) which has been defined in a Katana scene using the
		   RenderOutputDefine node. This is only applicable during disk render
		   where the function is called for each render output (port) on the
		   render node.
		 */
		void configureDiskRenderOutputProcess(
				FnKatRender::DiskRenderOutputProcess& diskRenderOutputProcess,
				const std::string& outputName,
				const std::string& outputPath,
				const std::string& renderMethodName,
				const float& frameTime) const;

		bool IsLive() const;
		bool IsPreview() const;
		bool IsDisk() const;
		bool IsBatch() const;

		/* Make some useful stuff public. */
		using RenderBase::getRenderTime;
		using RenderBase::getKatanaHost;
		using RenderBase::useRenderPassID;
		using RenderBase::applyRenderThreadsOverride;
		using RenderBase::getRenderOutputFile;

	private:
		typedef FnKatRender::GlobalSettings GlobalSettings;

		void AddCloudArgs(NSI::DynamicArgumentList& o_beginArgs);
		void WriteInfoAboutFilePathNames();

		std::string m_render_method;

		/* true when we're accepting live updates. */
		bool m_is_live_editing;

		/* true when we've accepted a live update but not yet applied it. */
		bool m_has_pending_updates;

		/* Ensures update queueing and application are exclusive. */
		std::shared_mutex m_update_mutex;

		ExportContext *m_export_ctx;

		/* Context used to group together sequence export. */
		std::unique_ptr<NSI::Context> m_sequence_context;
		/* Flag for delayed creation of above context. */
		bool m_sc_created{false};
};

#endif
