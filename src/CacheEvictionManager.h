#ifndef __CacheEvictionManager_h
#define __CacheEvictionManager_h

#include <cstddef>

/**
	\brief Class to track when we should evict Katana's scene graph cache.

	The thresholds used here are a rough guess and may require further tuning.
	They should provide better behavior than always evicting and never
	evicting.

	What I've found so far through experimentation is that letting the cache
	grow too large can make the export slower as more time is spent allocating
	memory. Of course, that balance could change if the data we're flushing was
	expensive to compute and would have been reused. This can only be an
	imperfect solution until the cache is improved inside Katana.
*/
class CacheEvictionManager
{
public:
	CacheEvictionManager()
	:
		m_data_size{0},
		m_location_count{0}
	{
	}

	/**
		\brief Count an amount of exported data.
	*/
	void CountData( size_t i_size ) { m_data_size += i_size; }

	/**
		\brief Count an exported location.

		\returns true if we're due for a cache eviction.
	*/
	bool EvictNextLocation()
	{
		if( ++m_location_count >= 100 ||
		    m_data_size >= 100*1000000 )
		{
			m_data_size = 0;
			m_location_count = 0;
			return true;
		}
		return false;
	}

private:
	/* Counters reset at every eviction. */
	size_t m_data_size;
	size_t m_location_count;
};

#endif
