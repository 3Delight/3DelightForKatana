/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "ExportSG.h"

#include "AttributeUtils.h"
#include "ExportAttributes.h"
#include "ExportCamera.h"
#include "ExportContext.h"
#include "ExportGeo.h"
#include "ExportInstance.h"
#include "ExportLight.h"
#include "ExportOptions.h"
#include "FnGeolibServices/FnXFormUtil.h"
#include "FnAttribute/FnAttributeUtils.h"
#include "tbb/parallel_for.h"

#include <assert.h>
#include <stdio.h>
#include <stack>

/*
	I'm putting this here because it's important and there is no better
	place. Here's what Nick Hutchinson says on 'evict':

	Apologies for the lack of documentation on ScenegraphIterator's evict
	parameter. In the current implementation, Katana maintains a cache of
	computed scene data on a per-location basis, and the evict parameter
	controls when this cache is pruned. When you pass evict=true, we throw
	away any data for scene graph locations not 'relevant' to the location
	currently pointed to by the iterator. (Currently this means that we
	throw away results for locations not referenced since the last time you
	passed evict=true that are not ancestors of the current location.)

	If you never pass true for this parameter (the default is false), the
	cache will grow in an unbounded fashion, and you likely don't want
	this. The RenderMan and Arnold plug-ins currently pass evict=true on
	each call to FnScenegraphIterator::getFirstChild(), and have reported
	success with this, so we think this is a good rule of thumb.
*/

namespace ExportSG
{

/*
	Debug utility to recursively list attributes.
*/
void ListAttributes(
	const FnAttribute::Attribute &i_attr,
	const char *i_attr_name,
	unsigned i_depth )
{
	if( !i_attr.isValid() )
		return;

	FnAttribute::GroupAttribute group( i_attr );
	FnAttribute::DataAttribute data( i_attr );
	FnAttribute::IntAttribute integer( i_attr );
	FnAttribute::StringAttribute str( i_attr );
	FnAttribute::FloatAttribute flt( i_attr );
	FnAttribute::DoubleAttribute dbl( i_attr );
	for( unsigned i = 0; i < i_depth; ++i )
	{
		printf( "  " );
	}
	printf( "%s", i_attr_name );

	FnKatAttributeType type = i_attr.getType();
	switch( type )
	{
		case kFnKatAttributeTypeNull: printf( " (null)" ); break;
		case kFnKatAttributeTypeInt: printf( " (int)" ); break;
		case kFnKatAttributeTypeFloat: printf( " (float)" ); break;
		case kFnKatAttributeTypeDouble: printf( " (double)" ); break;
		case kFnKatAttributeTypeString: printf( " (string)" ); break;
		case kFnKatAttributeTypeGroup: printf( " (group)" ); break;
	}

	if( group.isValid() )
	{
		printf( "\n" );
		for( int c = 0; c < group.getNumberOfChildren(); ++c )
		{
			ListAttributes(
				group.getChildByIndex( c ),
				group.getChildName( c ).c_str(),
				i_depth + 1 );
		}
	}
	else if( data.isValid() )
	{
		printf( " with %d time samples",
			int( data.getNumberOfTimeSamples() ) );

		if( integer.isValid() )
		{
			/* Print single integer values. */
			FnAttribute::IntAttribute::array_type v =
				integer.getNearestSample( 0.0f );
			if( !v.empty() )
				printf( " ('%d')", v[0] );
		}
		if( str.isValid() )
		{
			/* Print single string values. */
			FnAttribute::StringAttribute::array_type v =
				str.getNearestSample( 0.0f );
			if( !v.empty() )
				printf( " ('%s')", v[0] );
		}
		if( flt.isValid() )
		{
			/* Print short float arrays (eg. transforms). */
			FnAttribute::FloatAttribute::array_type v =
				flt.getNearestSample( 0.0f );
			if( v.size() <= 4 )
			{
				printf( " ( " );
				for( unsigned i = 0; i < v.size(); ++i )
				{
					printf( "%.7f ", v[i] );
				}
				printf( ")" );
			}
		}
		if( dbl.isValid() )
		{
			/* Print short double arrays (eg. transforms). */
			FnAttribute::DoubleAttribute::array_type v =
				dbl.getNearestSample( 0.0f );
			if( v.size() <= 4 )
			{
				printf( " ( " );
				for( unsigned i = 0; i < v.size(); ++i )
				{
					printf( "%.10f ", v[i] );
				}
				printf( ")" );
			}
		}

		printf( "\n" );
	}
}

/*
	Debug utility to list all attributes on a scene graph location.
	Only lists local attributes.
*/
void ListAllNodeAttributes(
	const SGLocation &i_location )
{
	FnAttribute::StringAttribute names = i_location.getAttributeNames();
	FnAttribute::StringAttribute::array_type names_v =
		names.getNearestSample( 0.0f );

	printf( "-- %s\n", i_location.getFullName().c_str() );
	for( unsigned i = 0; i < names_v.size(); ++i )
	{
		ListAttributes( i_location.getAttribute( names_v[i] ), names_v[i], 1 );
	}
}

/**
	\brief Export a subtree of the scene graph.

	\param i_ctx
		The exporting context.

	\param i_iterator
		The root of the subtree to export.

	This has two actual implementations:
	- A simple recursive traversal.
	- A multithread traversal.

	A note about the 'evict' parameter: from testing with the katana instance
	demo scene, it seems that it does not actually cause a clear of the cache
	if the returned iterator is not valid (ie. there is no child/sibling).
	That's why we set it to true for siblings as well as children. It is needed
	to get some clears when there are many siblings without any children.
	Otherwise, the cache can grow to several GB.
*/
void ExportSGSubtree(
	ExportContext &i_ctx,
	const FnKat::FnScenegraphIterator &i_iterator )
{
	bool multithread_export = true;

	if( multithread_export )
	{
		ExportSGSubtreeMultithread( i_ctx, i_iterator );
	}
	else
	{
		ExportSGSubtreeBasic( i_ctx, i_iterator );
	}
}

/**
	\brief Simple single thread export.

	\sa ExportSGSubtree()
*/
void ExportSGSubtreeBasic(
	ExportContext &i_ctx,
	const FnKat::FnScenegraphIterator &i_iterator )
{
	CacheEvictionManager &cem = i_ctx.EvictionManager();
	std::stack<FnKat::FnScenegraphIterator> traversal_stack;
	traversal_stack.push( i_iterator );
	bool first = true;

	while( !traversal_stack.empty() )
	{
		FnKat::FnScenegraphIterator it = traversal_stack.top();
		traversal_stack.pop();

		if( !it.isValid() )
			continue;

		ExportSGLocation( i_ctx, it );

		/* Don't traverse siblings of the root. */
		bool do_evict = cem.EvictNextLocation();
		if( !first )
		{
			traversal_stack.push( it.getNextSibling( do_evict ) );
		}
		/* Always traverse children. */
		traversal_stack.push( it.getFirstChild( do_evict ) );
		first = false;
	}
}

/**
	\brief Multithread export.

	\sa ExportSGSubtree()
*/
void ExportSGSubtreeMultithread(
	ExportContext &i_ctx,
	const FnKat::FnScenegraphIterator &i_iterator )
{
	if( !i_iterator.isValid() )
		return;

	ExportSGLocation( i_ctx, i_iterator );

	FnAttribute::StringAttribute::array_type children =
		i_iterator.getPotentialChildren().getNearestSample( 0.0f );

	tbb::blocked_range<size_t> children_range( 0, children.size() );
	if( children_range.empty() )
		return;

	tbb::parallel_for(
		children_range,
		[ & ]( const tbb::blocked_range<size_t> &r )
			{
				CacheEvictionManager &cem = i_ctx.EvictionManager();
				for( size_t i = r.begin(); i != r.end(); ++i )
				{
					bool do_evict = cem.EvictNextLocation();
					ExportSGSubtreeMultithread(
						i_ctx,
						i_iterator.getChildByName( children[i], do_evict ) );
				}
			}
		);
}

/**
	\brief Export one location in the scene graph.

	Some of the types handled here are not real katana location types. They are
	manufactured in RendererInfo::fillLiveRenderTerminalOps to handle updates
	to specific attributes. They are documented as needed below.
*/
void ExportSGLocation(
	ExportContext &i_ctx,
	const SGLocation &i_location )
{
	//ListAllNodeAttributes( i_location );

	const std::string type = i_location.getType();
	if( type == "group" || type == "assembly" || type == "component" )
	{
		/* Assemblies and components are just different names for group to help
		   organize the scene (the scene graph tab knows about them). */
		ExportGroup( i_ctx, i_location );

		/* Export the default material on /root/world */
		if( i_location.getFullName() == "/root/world" )
			ExportMaterial::ExportDefaultMaterial( i_ctx, i_location );
	}
	else if( type == "camera" )
	{
		ExportCamera::ExportCamera( i_ctx, i_location );
		// For now, we simply export one screen per camera
		ExportCamera::ExportScreen( i_ctx, i_location );
	}
	else if( type == "polymesh" )
	{
		ExportGeo::ExportPolymesh( i_ctx, i_location );
	}
	else if( type == "subdmesh" )
	{
		ExportGeo::ExportSubdivmesh( i_ctx, i_location );
	}
	else if( type == "curves" )
	{
		ExportGeo::ExportCurves( i_ctx, i_location );
	}
	else if( type == "pointcloud" )
	{
		ExportGeo::ExportPointCloud( i_ctx, i_location );
	}
	else if( type == "faceset" )
	{
		ExportGeo::ExportFaceSet( i_ctx, i_location );
	}
	else if( type == "light" )
	{
		ExportLight::ExportLightLocation( i_ctx, i_location );
	}
	else if( type == "rig" )
	{
		/* A rig is what the GafferThree calls its groups. */
		ExportGroup( i_ctx, i_location );
	}
	else if( type == "light filter" )
	{
		/* Most of the work is done by a terminal OpScript but we still want
		   to export the coordinate system here. */
		ExportGroup( i_ctx, i_location );
	}
	else if( type == "light filter reference" )
	{
		/* Nothing to do here. This is handled by a terminal OpScript. */
	}
	else if( type == "material" )
	{
		ExportMaterial::ExportOneMaterial( i_ctx, i_location, "material" );
	}
	else if( type == "local_material" )
	{
		/*
			This is a fake type introduced in Live Render to grab only the
			material updates and only from locations where we actually use the
			local material.
		*/
		ExportMaterial::ExportOneMaterial(
			i_ctx, i_location, "dl_local_material" );
	}
	else if( type == "transform" )
	{
		/*
			This is a fake type introduced in Live Render to grab only 'xform'
			updates and not update the entire group/mesh/light/etc.
		*/
		ExportTransform( i_ctx, i_location );
	}
	else if( type == "attributes" )
	{
		/*
			This is a fake Live Render type to grab attribute changes and not
			update the entire group/mesh/light/etc.
		*/
		ExportAttributes::ExportAttributes( i_ctx, i_location );
	}
	else if( type == "XGen" )
	{
		ExportGeo::ExportXGen( i_ctx, i_location );
	}
	else if( type == "instance source" )
	{
		/* Like a group except it is not rendered. See in ExportGroup(). */
		ExportGroup( i_ctx, i_location );
	}
	else if( type == "instance" )
	{
		ExportInstance::ExportHierarchicalInstance( i_ctx, i_location );
	}
	else if( type == "renderSettings" )
	{
		/* A special live update, hardcoded in katana. */
		ExportOptions::UpdateRenderSettings( i_ctx, i_location );
	}
	else if( type == "dlGlobalStatements" )
	{
		/*
			This is another fake Live Render type to get global options
			updates. It also receives renderSettings because a few useful
			things are stored there.
		*/
		i_ctx.m_renderSettings.ParseSettings( i_location );
		ExportOptions::ExportOptions( i_ctx );
		/* Some overrides options can modify the camera. */
		ExportCamera::ExportOptions( i_ctx );
		/* Atmosphere is unfortunately stuffed into the options so we must
		   update here for live render to work well. */
		ExportGeo::ExportAtmosphere( i_ctx, i_location );
	}
	else if( type == "yeti" )
	{
		ExportGeo::ExportYeti( i_ctx, i_location );
	}
	else if( type == "nsiarchive" )
	{
		ExportGeo::ExportNSIArchive( i_ctx, i_location );
	}
	else if( type == "openvdb" )
	{
		ExportGeo::ExportOpenVDB( i_ctx, i_location );
	}
	else if( type == "locator" )
	{
		/* All we need is a transform node, like groups. */
		ExportGroup( i_ctx, i_location );
	}
	else if( type == "usd point instancer" )
	{
		/* Seems to be just a group too. */
		ExportGroup( i_ctx, i_location );
	}
	else if( type == "instance array" )
	{
		ExportInstance::ExportInstanceArray( i_ctx, i_location );
	}
	else if( type == "lookfile" )
	{
		/*
			Nothing to do here. LookFileManager creates this and there will be
			materials under it but they're not geo and thus don't connect to
			this location.
		*/
	}
	else if( type == "error" )
	{
		/* Some kind of error. Happens with invalid material location. */
		std::string message;
		AttributeUtils::GetString( i_location, "errorMessage", message );
		printf(
			"error on scene graph location '%s' : %s\n",
			i_location.getFullName().c_str(), message.c_str() );
	}
	else
	{
		printf(
			"'%s' is unsupported scene graph type: '%s'\n",
			i_location.getFullName().c_str(), type.c_str() );
	}
}

/**
	\brief Export one item from a live update group.

	\param i_ctx
		The export context.
	\param i_update_item
		The group for the update item to export. This must have "location",
		"type" and "attributes" attributes.
*/
void ExportLiveUpdateItem(
	ExportContext &i_ctx,
	FnAttribute::GroupAttribute &i_update_item )
{
	using AttributeUtils::GetString;

	if( !i_update_item.isValid() )
	{
		assert( false ); /* investigate what it is */
		return;
	}

	std::string location, type;
	GetString( i_update_item, "location", location );
	GetString( i_update_item, "type", type );

	FnAttribute::GroupAttribute item_attributes(
		i_update_item.getChildByName( "attributes" ) );

	if( !item_attributes.isValid() || location.empty() || type.empty() )
	{
		assert( false );
		return;
	}

	/*
		This is a special update Katana sends when "Live Render from Viewer
		Camera" is enabled and the viewer camera is moved. We fix its type
		and send it through the usual update code. We leave the weird name
		alone as it is given as the render camera by a separate render
		settings update.
	*/
	if( type == "__VIEWERTYPE__" && location == "__VIEWERPATH__" )
		type = "camera";

	SGLocation sgl( location, type, item_attributes );
	ExportSG::ExportSGLocation( i_ctx, sgl );
}

/**
  	\brief Export a group node in the scene graph.

	This produces a NSI transform node from a scene graph location and attaches
	it to its parent. Note that the scene graph location does not actually need
	to be a group. This can just as well produce a transform node from a
	primitive, which we need to export its transform.

	There is a special case for an "instance source" type location which is not
	supposed to be rendered (so say the Katana docs and common sense). It is
	simply not connected to its parent.
*/
void ExportGroup(
	ExportContext &i_ctx,
	const SGLocation &i_location )
{
	using namespace FnKat;

	std::string name = i_location.getFullName();
	std::string parent_name = i_location.getParentFullName();

	/*
		NSI's root is ".root" so we'll substitute that for katana's "/root".
		In practice, this is what makes "/root/world" connect to ".root".
		parent_name is the same as name when using __VIEWERPATH__ for
		katana's camera.
	*/
	if( parent_name == "/root" || name == "__VIEWERPATH__" )
		parent_name = NSI_SCENE_ROOT;

	if( i_location.IsDeleted() )
	{
		i_ctx.m_nsi.Delete( name );
		return;
	}

	i_ctx.m_nsi.Create( name, "transform" );
	if( i_location.getType() == "instance source" )
	{
		/* Actively disconnect it for live render location type change. */
		i_ctx.m_nsi.Disconnect( name, "", parent_name, "objects" );
	}
	else
	{
		i_ctx.m_nsi.Connect( name, "", parent_name, "objects" );
	}

	/*
		These come with separate updates so don't bother trying to export
		them in a live update.

		We make an exception for cameras as Katana has built-in updates for its
		"Live Render from Viewer Camera" toggle which send camera and xform
		together as a "camera" update. This happens for the special
		__VIEWERPATH__ camera when the feature is enabled (after the first
		move, which seems like a bug) and for the render camera when the
		feature is disabled.
	*/
	if( !i_location.IsLiveUpdate() )
	{
		ExportTransform( i_ctx, i_location );
		ExportLocalMaterialAndAttributes( i_ctx, i_location );
	}
	else if( i_location.getType() == "camera" )
	{
		ExportTransform( i_ctx, i_location );
	}
}

/**
	\brief Export the transform matrix of a node in the scene graph.

	\param i_ctx
		The export context.
	\param i_location
		The scene graph location to export the transform of.

	This assumes the NSI node already exists and is a transform. All it does is
	write the transformationmatrix attribute if needed.
*/
void ExportTransform(
	ExportContext &i_ctx,
	const SGLocation &i_location )
{
	/* Produce the transformation if there is one. */
	FnAttribute::GroupAttribute xformAttr( i_location.getAttribute( "xform" ) );
	if( xformAttr.isValid() )
	{
		std::pair<FnAttribute::DoubleAttribute, bool> matrices =
			FnGeolibServices::FnXFormUtil::CalcTransformMatrixAtExistingTimes(
				xformAttr );

		/*
			We don't support absolute transform for now. I can't generate one
			with built-in katana transform nodes anyway. It only occurs with
			xform.<group>.identity but even the TransformEdit node in "replace
			global transform" mode will output a "parentInverse" transform
			instead of using identity. I suspect it is rarely used.

			Update: As of about 3.0v1.001402a, viewer camera live updates send
			an absolute transform. We can't process those yet so I've adjusted
			the assert :(
		*/
		assert( !matrices.second || i_location.IsLiveUpdate() );

		/* Output 'transformationmatrix' nsi attribute. */
		ExportFloatAttribute(
			i_ctx,
			matrices.first,
			i_location.getFullName().c_str(),
			"transformationmatrix",
			NSITypeDoubleMatrix, 0, 0 );
	}
}

/**
	\brief Initial export of attributes which are separate in live updates.

	\param i_ctx
		The export context.
	\param i_location
		The scene graph location to export.
*/
void ExportLocalMaterialAndAttributes(
	ExportContext &i_ctx,
	const SGLocation &i_location )
{
	/*
		Material before attributes as attributes may connect to material.
		Note that this is only local materials. They are generated by
		RendererInfo::AddOpUseLocalMaterial().
	*/
	ExportMaterial::ExportOneMaterial(
		i_ctx, i_location, "dl_local_material" );
	ExportAttributes::ExportAttributes( i_ctx, i_location );
}

/**
  	\brief Export a set.

	This produces a NSI set node from a scene graph location.
*/
void ExportSet(
	ExportContext &i_ctx,
	const SGLocation &i_location )
{
	using namespace FnKat;

	std::string name = i_location.getFullName();
	name += "|set";

	i_ctx.m_nsi.Create( name, "set" );
}


static unsigned NSITypeChannels( NSIType_t i_type )
{
	/* turn off the 'double' bit so this handles both float and double */
	unsigned base_type = i_type & ~0x10;
	if(
		base_type == NSITypeColor ||
		base_type == NSITypePoint ||
		base_type == NSITypeVector ||
		base_type == NSITypeNormal )
	{
		return 3;
	}
	else if( base_type == NSITypeMatrix )
	{
		return 16;
	}
	else
	{
		return 1;
	}
}

/**
	\brief Export a katana float or double attribute as an NSI attribute.

	\param i_ctx
		The export context.
	\param i_attribute
		The katana attribute to export.
	\param i_nsi_node
		The NSI node handle on which to set the attribute.
	\param i_nsi_attribute_name
		The name of the NSI attribute to set.
	\param i_nsi_type
		The NSI type to give the attribute.
	\param i_arraylength
		The array length of the the NSI type. Ignored unless i_nsi_flags
		contains NSIParamIsArray.
	\param i_nsi_flags
		The NSI flags to set on the attribute.

	This supports motion blur and allows outputting the katana attribute as
	any NSI type as long as its base type is either float or double.
*/
void ExportFloatAttribute(
	ExportContext &i_ctx,
	const FnAttribute::Attribute &i_attribute,
	const char *i_nsi_node,
	const char *i_nsi_attribute_name,
	NSIType_t i_nsi_type,
	unsigned i_arraylength,
	int i_nsi_flags )
{
	/* Remove motion blur if the data is all the same. */
	FnAttribute::Attribute attr(
		FnAttribute::RemoveTimeSamplesIfAllSame( i_attribute ) );

	FnAttribute::FloatAttribute float_attr( attr );
	FnAttribute::DoubleAttribute double_attr( attr );
	FnAttribute::DataAttribute &data_attr = float_attr.isValid()
		? static_cast<FnAttribute::DataAttribute&>( float_attr )
		: static_cast<FnAttribute::DataAttribute&>( double_attr );

	if( !data_attr.isValid() )
		return;

	bool is_array = 0 != (i_nsi_flags & NSIParamIsArray);
	unsigned type_channels = NSITypeChannels( i_nsi_type );
	unsigned type_nv = is_array ? type_channels * i_arraylength : type_channels;
	size_t nv = data_attr.getNumberOfValues();
	int n = data_attr.getNumberOfTimeSamples();
	/*
		Don't bother with the overhead of tracking small stuff like matrices.
		The size will be dominated by overhead we don't know about inside
		Katana anyway.
	*/
	if( nv > 16 )
	{
		size_t sz = nv * n;
		sz *= float_attr.isValid() ? sizeof(float) : sizeof(double);
		i_ctx.EvictionManager().CountData( sz );
	}

	for( int i = 0; i < n; ++i )
	{
		float t = data_attr.getSampleTime( i );

		NSI::Argument arg( i_nsi_attribute_name );
		if( is_array )
		{
			arg.SetArrayType( i_nsi_type, i_arraylength );
		}
		else
		{
			arg.SetType( i_nsi_type );
		}
		arg.SetCount( nv / type_nv );
		arg.SetFlags( i_nsi_flags );

		if( float_attr.isValid() )
		{
			FnAttribute::FloatAttribute::array_type float_data =
				float_attr.getNearestSample( t );

			if( 0 == (i_nsi_type & 0x10) )
			{
				arg.SetValuePointer( float_data.data() );
			}
			else
			{
				/* Needs float -> double conversion. */
				double *buffer = (double*)arg.AllocValue( sizeof(double) * nv );
				for( size_t j = 0; j < nv; ++j )
					buffer[j] = float_data[j];
			}
		}
		else
		{
			assert( double_attr.isValid() );

			FnAttribute::DoubleAttribute::array_type double_data =
				double_attr.getNearestSample( t );

			if( 0 != (i_nsi_type & 0x10) )
			{
				arg.SetValuePointer( double_data.data() );
			}
			else
			{
				/* Needs double -> float conversion. */
				float *buffer = (float*)arg.AllocValue( sizeof(float) * nv );
				for( size_t j = 0; j < nv; ++j )
					buffer[j] = double_data[j];
			}
		}

		if( n == 1 )
		{
			i_ctx.m_nsi.SetAttribute( i_nsi_node, arg );
		}
		else
		{
			i_ctx.m_nsi.SetAttributeAtTime( i_nsi_node, t, arg );
		}
	}
}

/**
	\brief Export a katana data attribute as an NSI attribute.

	\param i_ctx
		The export context.
	\param i_attribute
		The katana attribute to export.
	\param i_nsi_node
		The NSI node handle on which to set the attribute.
	\param i_nsi_attribute_name
		The name of the NSI attribute to set.
	\param i_nsi_type
		The NSI type to give the attribute. For integer or string types, the
		provided data must match exactly. Other types will accept either float
		or double data.
	\param i_arraylength
		The array length of the the NSI type. Ignored unless i_nsi_flags
		contains NSIParamIsArray.
	\param i_nsi_flags
		The NSI flags to set on the attribute.

	This outputs integer and string data directly and forwards export of
	floating point data to ExportFloatAttribute.

	Motion blur is not supported for integer and string data.
*/
void ExportAnyTypeAttribute(
	ExportContext &i_ctx,
	const FnAttribute::Attribute &i_attribute,
	const char *i_nsi_node,
	const char *i_nsi_attribute_name,
	NSIType_t i_nsi_type,
	unsigned i_arraylength,
	int i_nsi_flags )
{
	if( i_nsi_type == NSITypeInvalid )
	{
		/* There was likely a problem figuring out what this is. */
		return;
	}

	if( i_nsi_type != NSITypeInteger && i_nsi_type != NSITypeString )
	{
		ExportFloatAttribute(
			i_ctx, i_attribute, i_nsi_node, i_nsi_attribute_name,
			i_nsi_type, i_arraylength, i_nsi_flags );
		return;
	}

	FnAttribute::DataAttribute data_attr( i_attribute );
	if( !data_attr.isValid() )
		return;

	size_t nv = data_attr.getNumberOfValues();
	bool is_array = 0 != (i_nsi_flags & NSIParamIsArray);
	unsigned type_channels = NSITypeChannels( i_nsi_type );
	unsigned type_nv = is_array ? type_channels * i_arraylength : type_channels;

	NSI::Argument arg( i_nsi_attribute_name );
	if( is_array )
	{
		arg.SetArrayType( i_nsi_type, i_arraylength );
	}
	else
	{
		arg.SetType( i_nsi_type );
	}
	arg.SetCount( nv / type_nv );
	arg.SetFlags( i_nsi_flags );

	if( i_nsi_type == NSITypeInteger )
	{
		FnAttribute::IntAttribute integer_attr( i_attribute );
		if( integer_attr.isValid() )
		{
			auto const &sample = integer_attr.getNearestSample( 0.0f );
			arg.SetValuePointer( sample.data() );
			i_ctx.m_nsi.SetAttribute( i_nsi_node, arg );
		}
	}
	else if( i_nsi_type == NSITypeString )
	{
		FnAttribute::StringAttribute string_attr( i_attribute );
		if( string_attr.isValid() )
		{
			/* This is conveniently already an array of 'char*' */
			auto const &sample = string_attr.getNearestSample( 0.0f );
			arg.SetValuePointer( sample.data() );
			i_ctx.m_nsi.SetAttribute( i_nsi_node, arg );
		}
	}
}

}
