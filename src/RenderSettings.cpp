/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2016                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "RenderSettings.h"

#include <FnRender/plugin/IdSenderFactory.h>

#include <vector>
#include <list>
#include <sstream>

#include "AttributeUtils.h"
#include "SGLocation.h"
#include "utils.h"

int64_t RenderSettings::m_next_id = 0;

/* Katana doesn't allow to access attributes if they were not set in the UI by
   the user. If an attribute has a default value, this attribute is absent for
   the rendering API. That's why we need to set the default value in both here
   and XML. */
RenderSettings::RenderSettings(FnKat::FnScenegraphIterator rootIterator) :
	FnKatRender::RenderSettings(rootIterator),
	m_shading_samples( 64 ),
	m_pixel_samples( 9 ),
	m_volume_samples( 16 ),
	m_pixel_filter( "blackman-harris" ),
	m_filter_width( 3.0f ),
	m_diffuse_depth( 3 ),
	m_reflection_depth( 2 ),
	m_refraction_depth( 4 ),
	m_hair_depth( 4 ),
	m_max_ray_length( 1000.0f ),
	m_layers_batch( 0 ),
	m_layers_interactive( 0 ),
	m_disable_motion_blur( 0 ),
	m_disable_depth_of_field( 0 ),
	m_disable_displacement( 0 ),
	m_disable_subsurface( 0 ),
	m_resolution_reduce_factor( 1 ),
	m_sampling_reduce_factor( 1.0 ),
	m_fstop( 2.0 ),
	m_id_sender(NULL)
{
	m_real_ROI[0] = m_real_ROI[1] = m_real_ROI[2] = m_real_ROI[3] = 0;
	m_screenWindow[0] = -1.0;
	m_screenWindow[1] = -1.0;
	m_screenWindow[2] =  1.0;
	m_screenWindow[3] =  1.0;

	initialise();
}

int RenderSettings::initialise()
{
	using AttributeUtils::GetFloat;
	using AttributeUtils::GetInteger;
	using AttributeUtils::GetString;

	FnAttribute::Attribute ROI_attr =
		_rootIterator.getAttribute( "renderSettings.ROI" );
	if( ROI_attr.isValid() )
	{
		UpdateROI( ROI_attr );
	}

	FnAttribute::GroupAttribute global_statements =
		_rootIterator.getAttribute(
				std::string(_3DELIGHT_RENDERER_NAME) + "GlobalStatements" );

	if(!global_statements.isValid())
	{
		return 1;
	}

	SGLocation location( _rootIterator );
	ParseSettings( location );

	FnAttribute::GroupAttribute quality =
		global_statements.getChildByName("quality");

	if( quality.isValid() )
	{
		FnAttribute::IntAttribute pixel_samples_attribute =
			quality.getChildByName("pixelSamples");
		if( pixel_samples_attribute.isValid() )
		{
			m_pixel_samples = pixel_samples_attribute.getValue();
		}

		FnAttribute::StringAttribute pixel_filter_attribute =
			quality.getChildByName("pixelFilter");
		if( pixel_filter_attribute.isValid() )
		{
			m_pixel_filter = pixel_filter_attribute.getValue();
		}

		FnAttribute::FloatAttribute filter_width_attribute =
			quality.getChildByName("filterWidth");
		if( filter_width_attribute.isValid() )
		{
			m_filter_width = filter_width_attribute.getValue();
		}

		GetFloat( global_statements, "quality.maximumraylength", m_max_ray_length );
	}

	FnAttribute::GroupAttribute layers =
		global_statements.getChildByName("layers");
	if( layers.isValid() )
	{
		FnAttribute::IntAttribute batch =
			layers.getChildByName("batch");
		if( batch.isValid() )
		{
			m_layers_batch = batch.getValue();
		}

		FnAttribute::IntAttribute interactive =
			layers.getChildByName("interactive");
		if( interactive.isValid() )
		{
			m_layers_interactive = interactive.getValue();
		}
	}

	FnAttribute::GroupAttribute overrides =
		global_statements.getChildByName("overrides");

	int interactive = 0;
	if( overrides.isValid() )
	{
		GetInteger( global_statements, "overrides.interactive", interactive );
		if( interactive == 1 )
		{
			std::string resolution = "Half";
			GetString( global_statements, "overrides.reduction.resolution", resolution );
			if ( resolution == "Full" )
			{
				m_resolution_reduce_factor = 1;
			}
			else if ( resolution == "Half" )
			{
				m_resolution_reduce_factor = 2;
			}
			else if ( resolution == "Quarter" )
			{
				m_resolution_reduce_factor = 4;
			}
			else //if ( resolution == "Eighth" )
			{
				m_resolution_reduce_factor = 8;
			}
		}
		else
		{
			m_resolution_reduce_factor = 1;
		}
	}

	/* Initialises screen window with current values at the start. */
	float sw[4];
	getCameraSettings()->getScreenWindow(sw);
	m_screenWindow[0] = sw[0];
	m_screenWindow[1] = sw[1];
	m_screenWindow[2] = sw[2];
	m_screenWindow[3] = sw[3];

	return 0;
}

/**
	Parse renderSettings and dlGlobalStatements from a location which is always
	/root but can be either a scene iterator or a live update.
*/
void RenderSettings::ParseSettings( const SGLocation &i_location )
{
	using AttributeUtils::GetInteger;
	using AttributeUtils::GetString;

	assert( i_location.getFullName() == "/root" );

	/* Convenient macro to keep down repetitive text. */
#define DGS _3DELIGHT_RENDERER_NAME "GlobalStatements"

	GetInteger( i_location, DGS ".quality.shadingSamples", m_shading_samples );
	GetInteger( i_location, DGS ".quality.volumeSamples", m_volume_samples );
	GetInteger(
		i_location, DGS ".quality.maxdepth.diffuse", m_diffuse_depth );
	GetInteger( 
		i_location, DGS ".quality.maxdepth.reflection", m_reflection_depth );
	GetInteger( 
		i_location, DGS ".quality.maxdepth.refraction", m_refraction_depth );
	GetInteger( 
		i_location, DGS ".quality.maxdepth.hair", m_hair_depth );

	int interactive = 0;
	GetInteger( i_location, DGS ".overrides.interactive", interactive );
	if( interactive == 1 )
	{
		GetInteger( 
			i_location, DGS ".overrides.disabling.disableMotionBlur",
			m_disable_motion_blur );
		GetInteger( 
			i_location, DGS ".overrides.disabling.disableDepthOfField",
			m_disable_depth_of_field );
		GetInteger(
			i_location, DGS ".overrides.disabling.disableDisplacement",
			m_disable_displacement );
		GetInteger(
			i_location, DGS ".overrides.disabling.disableSubsurface",
			m_disable_subsurface );

		std::string sampling = "10%";
		GetString( i_location, DGS ".overrides.reduction.sampling", sampling );
		if ( sampling == "100%" )
		{
			m_sampling_reduce_factor = 1.0;
		}
		else if ( sampling == "25%" )
		{
			m_sampling_reduce_factor = 0.25;
		}
		else if ( sampling == "10%" )
		{
			m_sampling_reduce_factor = 0.1;
		}
		else if ( sampling == "4%" )
		{
			m_sampling_reduce_factor = 0.04;
		}
		else //if ( sampling == "1%" )
		{
			m_sampling_reduce_factor = 0.01;
		}
	}
	else
	{
		m_disable_motion_blur = 0;
		m_disable_depth_of_field = 0;
		m_disable_displacement = 0;
		m_disable_subsurface = 0;
		m_sampling_reduce_factor = 1.0;
	}

#undef DGS
}

void RenderSettings::UpdateROI( const FnAttribute::Attribute &i_ROI_attr )
{
	assert( i_ROI_attr.isValid() );
	FnAttribute::IntAttribute int_ROI_attr(i_ROI_attr);
	if( int_ROI_attr.isValid() )
	{
		FnAttribute::IntAttribute::array_type ROI_value =
			int_ROI_attr.getNearestSample( 0.0f );

		if( ROI_value.size() >= 4u )
		{
			m_real_ROI[0] = ROI_value[0];
			m_real_ROI[1] = ROI_value[1];
			m_real_ROI[2] = ROI_value[2];
			m_real_ROI[3] = ROI_value[3];
			return;
		}
	}

	/*
		Live updates when ROI is turned off send a NullAttribute so we'll end
		up in this branch. Just zero everything. getROI() understands this as
		no ROI.
	*/
	m_real_ROI[0] = 0;
	m_real_ROI[1] = 0;
	m_real_ROI[2] = 0;
	m_real_ROI[3] = 0;
}

void RenderSettings::ComputeRenderResolution(
	bool i_interactive,
	int o_res[2] ) const
{
	o_res[0] = getResolutionX();
	o_res[1] = getResolutionY();
	if( i_interactive )
	{
		o_res[0] /= m_resolution_reduce_factor;
		o_res[1] /= m_resolution_reduce_factor;
	}
}

/*
	This returns overscan in the 3Delight order which is
		[left, top, right, bottom].
*/
void RenderSettings::ComputeOverscan(
	bool i_interactive,
	int o_overscan[4] ) const
{
	int katana_os[4];
	getOverscan(katana_os);
	o_overscan[0] = katana_os[0];
	o_overscan[1] = katana_os[3];
	o_overscan[2] = katana_os[2];
	o_overscan[3] = katana_os[1];
	if( i_interactive )
	{
		o_overscan[0] /= m_resolution_reduce_factor;
		o_overscan[1] /= m_resolution_reduce_factor;
		o_overscan[2] /= m_resolution_reduce_factor;
		o_overscan[3] /= m_resolution_reduce_factor;
	}
}

void RenderSettings::InitIDSender(const std::string& host, int64_t frameID)
{
	/* Get an instance of the ID sender interface from the factory */
	m_id_sender = FnKatRender::IdSenderFactory::getNewInstance(host, frameID);
	/*
		This is deprecated in katana 2.6v3 where all 64-bit values except 0 are
		valid IDs. We'll keep iterating from 1 for now in case older katana
		versions still need that.

	m_id_sender->getIds(&m_next_id, &m_max_id);
	*/
	m_next_id = 1;
}

int RenderSettings::GetObjectID(const char* const objectName)const
{
	if(!m_id_sender)
	{
		return -1;
	}

	int id = m_next_id++;
	m_id_sender->send(id, objectName);
	return id;
}

void RenderSettings::GetMotionSampleOffsets(
	std::vector<double>& o_offsets ) const
{
	o_offsets.clear();

	int num_motion_samples = getMaxTimeSamples();
	float shutter_open = getShutterOpen();
	float shutter_close = getShutterClose();

	if( num_motion_samples > 1 && shutter_close > shutter_open )
	{
		double num_motion_segments = num_motion_samples - 1.0;
		double increment = ( shutter_close - shutter_open ) / 
			num_motion_segments;

		for( int i = 0; i < num_motion_samples; i++ )
		{
			o_offsets.push_back( shutter_open + i * increment );
		}
	}
	else
	{
		o_offsets.push_back(0.0);
	}
}
