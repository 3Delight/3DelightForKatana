/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __GlobalSettings_h
#define __GlobalSettings_h

#include <FnRender/plugin/GlobalSettings.h>

class GlobalSettings : public FnKat::Render::GlobalSettings
{
public:
	GlobalSettings(
		const FnKat::FnScenegraphIterator &i_rootIterator );

	bool WantIDisplay() const { return m_want_idisplay; }
	bool WantMonitor() const { return m_want_monitor; }

private:
	bool m_want_idisplay;
	bool m_want_monitor;
};

#endif
