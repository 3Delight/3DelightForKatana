/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "ExportOutputs.h"

#include "AttributeUtils.h"
#include "ExportContext.h"
#include "KatanaMonitorDriver.h"
#include "Renderer.h"
#include "utils.h"

#include <algorithm>
#include <assert.h>
#include <stdio.h>

using AttributeUtils::AttrPath;

namespace ExportOutputs
{
	namespace FKR = FnKat::Render;

/**
	\brief Export all the required output drivers.

	\param i_ctx
		The export context.
	\param i_root
		The scene root iterator.

	TODO: I think we should change which outputs are enabled based Live/Preview
	vs disk render.
*/
void ExportAllOutputs(
	ExportContext &i_ctx,
	const FnKat::FnScenegraphIterator &i_root )
{
	using namespace std;
	vector<string> render_output_names;

	/*
		From what I see in the RenderSettings node documentation, this should
		be used for Preview renders. Probably Live renders as well.
	*/
	if( i_ctx.m_renderer.IsPreview() || i_ctx.m_renderer.IsLive() )
	{
		i_ctx.m_renderSettings.getInteractiveOutputs( render_output_names );
	}
	else
	{
		render_output_names = i_ctx.m_renderSettings.getRenderOutputNames();
	}

	/*
		There are sometimes duplicates in render_output_names, for unknown
		reasons. The one case I've seen so far is 'primary' being there twice
		in the list returned by getInteractiveOutputs(). So we make sure to
		skip them.
	*/
	int sort_key = 0;

	for(
		vector<string>::iterator it = render_output_names.begin();
		it != render_output_names.end(); ++it )
	{
		if( it == find( render_output_names.begin(), it, *it ) )
		{
			std::string light, feedback_data;
			GetLight( i_ctx, i_root, *it, light, feedback_data);

			ExportOneOutput(
				i_ctx, i_root, *it, light, feedback_data, sort_key );
		}
	}

	ExportIDPassOutput( i_ctx, i_root );
}

/**
	\brief Returns true if we should render an ID pass.

	\param i_ctx
		The export context.
	\returns
		true if we should render an ID pass, false if we should not.
*/
bool WantIDPass(
	ExportContext &i_ctx )
{
	/* No monitor, no ID pass. */
	if( !i_ctx.m_globalSettings.WantMonitor() )
		return false;

	/* This is also a check to see if we'll have the monitor. */
	if( !( i_ctx.m_renderer.IsPreview() || i_ctx.m_renderer.IsLive() ) )
		return false;

	/* This is Katana's own setting (eg. in the monitor's "3D" menu). */
	if( !i_ctx.m_renderer.useRenderPassID() )
		return false;

	return true;
}

/**
	\brief Returns the ID pass frame ID.

	The Katana dev guide on renderer plug-ins says to get the ID that way. I
	think I've read on the mailing list that primary is always first in the map
	because it gets 0 prepended to its name. This is done by katana in
	getChannelBuffers().
*/
static int IDPassFrameID( ExportContext &i_ctx )
{
	RenderSettings::ChannelBuffers interactive_buffers;
	i_ctx.m_renderSettings.getChannelBuffers( interactive_buffers );
	return atoi( interactive_buffers.begin()->second.bufferId.c_str() );
}

/**
	\brief Setup the ID generation for the ID pass.

	\param i_ctx
		The export context.

	This initializes the ID sender so it should be called before the geometry
	export.
*/
void InitIDPass(
	ExportContext &i_ctx )
{
	if( !WantIDPass( i_ctx ) )
		return;

	i_ctx.m_renderSettings.InitIDSender(
		i_ctx.m_renderer.getKatanaHost(), IDPassFrameID( i_ctx ) );
}

/**
	\brief Exports the katana monitor ID pass.

	\param i_ctx
		The export context.
	\param i_root
		The scene root iterator.
*/
void ExportIDPassOutput(
	ExportContext &i_ctx,
	const FnKat::FnScenegraphIterator &i_root )
{
	if( !WantIDPass( i_ctx ) )
		return;

	std::string layer_handle = i_ctx.GenOutputLayerHandle();
	i_ctx.m_nsi.Create( layer_handle, "outputlayer" );
	i_ctx.m_nsi.SetAttribute(
		layer_handle,
		(
			NSI::StringArg( "layername", "__id" ),
			NSI::StringArg( "variablename", "__id" ),
			NSI::StringArg( "variablesource", "attribute" ),
			NSI::StringArg( "frameName", "__id" ),
			NSI::IntegerArg( "frameID", IDPassFrameID( i_ctx ) ),
			NSI::StringArg( "scalarformat", "float" ),
			NSI::StringArg( "layertype", "scalar" ),
			NSI::StringArg( "filter", "zmin" ),
			NSI::DoubleArg( "filterwidth", 1.0 )
		) );

	FKR::RenderSettings::RenderOutput ro =
		i_ctx.m_renderSettings.getRenderOutputByName( "primary" );
	/* And then connect the layer to the screen. */
	std::string cameraName = ro.cameraName;
	if( cameraName == "" )
		cameraName = i_ctx.m_renderSettings.getCameraName();
	// Add "|screen" to the camera name to identify to get the screen node
	std::string screen_name = cameraName + "|screen";

	i_ctx.m_nsi.Connect( layer_handle, "", screen_name, "outputlayers" );

	/* Finally, export the driver. */
	ExportOutputDriverForLayer(
		i_ctx, i_root, "__id", layer_handle, od_katana_monitor );
}

/**
*/

std::string GetOutputDriverName(
	const FKR::RenderSettings::RenderOutput& i_ro,
	eOutputDriver i_driver)
{
	switch(i_driver)
	{
		case od_file :
		{
			RenderSettings::AttributeSettings attrSettings =
				i_ro.rendererSettings;
			FnAttribute::Attribute driverAttr = attrSettings["driver"];
			std::string driver;
			AttributeUtils::GetString( driverAttr, driver );
			return driver;
		}
		case od_idisplay :
			return "idisplay";
		case od_katana_monitor :
			return KatanaMonitorDriverName();
		case od_jpeg :
			return "jpeg";
		default :
			assert(false);
	}

	return "";
}

/**
	\brief Export one render output

	\param i_ctx
		The export context.
	\param i_root
		The scene root iterator.
	\param i_output_name
		The render output's name. As in renderSettings.outputs.<name> on /root
	\param io_sort_key
		The layer's order, will be incremented automatically according to the
		number of layers actually produced.

	This will produce the NSI output layer, output drivers.
*/
void ExportOneOutput(
	ExportContext &i_ctx,
	const FnKat::FnScenegraphIterator &i_root,
	const std::string &i_output_name,
	const std::string &i_light_set,
	const std::string &i_feedback_data,
	int& io_sort_key )
{
	using AttributeUtils::GetString;
	using AttributeUtils::GetInteger;

	FKR::RenderSettings::RenderOutput ro =
		i_ctx.m_renderSettings.getRenderOutputByName( i_output_name );

	/* Export the outputlayer node. */
	std::string layer_handle = i_ctx.GenOutputLayerHandle();
	i_ctx.m_nsi.Create( layer_handle, "outputlayer" );

	NSI::DynamicArgumentList layer_attr;

	/* Channel name is i_output_name, except for "primary" which has none. */
	if( i_output_name != "primary" )
		layer_attr.Add( new NSI::StringArg( "layername", i_output_name ) );

	/* Sort key to keep same order as this layer is defined. */
	layer_attr.Add( new NSI::IntegerArg( "sortkey", io_sort_key++ ) );

	/* Katana monitor driver needs the actual output name as 'frameName' */
	// FIXME: driver could probably use layername (above)
	layer_attr.Add( new NSI::StringArg( "frameName", i_output_name ) );

	/*
		Frame ID. This identifies the AOV in the list of requested
		interactive outputs (from RenderSettings's interactiveOutputs).
		From what I can gather, it's a for an older version of the
		monitor protocol. Anyway, the monitor display driver expects it.
	*/
	RenderSettings::ChannelBuffers interactive_buffers;
	i_ctx.m_renderSettings.getChannelBuffers( interactive_buffers );
	int frame_id = -1;
	RenderSettings::ChannelBuffers::const_iterator frameid_it =
		interactive_buffers.find( i_output_name );
	if( frameid_it == interactive_buffers.end() )
	{
		/* FnKat::Render::RenderSettings::getChannelBuffers() puts a "0" in
		   front of the key of the first channel. So search that way too. */
		frameid_it = interactive_buffers.find( "0" + i_output_name );
	}
	if( frameid_it != interactive_buffers.end() )
	{
		frame_id = atoi( frameid_it->second.bufferId.c_str() );
		layer_attr.Add( new NSI::IntegerArg( "frameID", frame_id ) );
	}

	/*
		Variable name is stored in the RenderOutput. Katana fills that from the
		renderSettings.outputs.<i_output_name>.rendererSettings.channel
		attribute on /root
	*/
	std::string variablename = ro.channel;

	/*
		"rgba" is the default value of the RenderOutputDefine if there has not
		been a DlSettings yet. Make it render "Ci" (which is our "rgb") and
		force that to have alpha.
	*/
	bool forcealpha = false;
	if( variablename == "rgba" )
	{
		variablename = "Ci";
		forcealpha = true;
	}

	layer_attr.Add( new NSI::StringArg( "variablename", variablename ) );

	/* Produce layertype and variable source attributes */
	RenderSettings::AttributeSettings attrSettings = ro.rendererSettings;

	FnAttribute::Attribute layertypeAttr = attrSettings["layerType"];
	std::string layertype;
	GetString( layertypeAttr, layertype );

	layer_attr.Add( new NSI::StringArg( "layertype", layertype ) );

	FnAttribute::Attribute variablesourceAttr = attrSettings["variableSource"];
	std::string variablesource;
	GetString( variablesourceAttr, variablesource );

	layer_attr.Add( new NSI::StringArg( "variablesource", variablesource ) );

	FnAttribute::Attribute withAlphaAttr = attrSettings["withAlpha"];
	int withAlpha = 0;
	GetInteger( withAlphaAttr, withAlpha );
	if( forcealpha )
	{
		withAlpha = 1;
	}
	layer_attr.Add( new NSI::IntegerArg( "withalpha", withAlpha ) );

	/* Draw outlines in Ci and the 'outlines' AOV. */
	if( variablename == "Ci" || variablename == "outlines" )
	{
		layer_attr.Add( new NSI::IntegerArg( "drawoutlines", 1 ) );
	}

	unsigned cryptomatte_layers = 0;
	if( i_ctx.m_renderer.IsPreview() || i_ctx.m_renderer.IsLive() )
	{
		/*
			We always want to send i-display 'half' data.
			We *have* to use either 'half' or 'float' for the monitor as we'll
			be stuck with a bad quantization scale otherwise. To handle this
			correctly, we'll need a new display driver API which can correctly
			change the data type and have quantization follow.
		*/
		layer_attr.push( new NSI::StringArg( "scalarformat", "half" ) );
	}
	else
	{
		/* Read exrBitDepth/tifBitDepth/... as appropriate. */
		std::string channel_depth{"16"};
		auto bd_it = ro.convertSettings.find(ro.fileExtension + "BitDepth");
		if( bd_it != ro.convertSettings.end() )
		{
			AttributeUtils::GetString(bd_it->second, channel_depth);
		}
		/*
			Get the old global attribute for channel depth. If this is set,
			it's an older DlSettings node and we must use that value. The node
			is updated only when values are changed in the UI.
		*/
		int legacy_depth = 0;
		GetInteger( i_root,
			AttrPath("dlGlobalStatements.layers.depth"), legacy_depth );
		if( legacy_depth != 0 )
		{
			channel_depth = std::to_string(legacy_depth);
		}

		if( channel_depth == "8" )
		{
			layer_attr.push( new NSI::StringArg( "scalarformat", "uint8" ) );
		}
		else if( channel_depth == "32" )
		{
			layer_attr.push( new NSI::StringArg( "scalarformat", "float" ) );
		}
		else
		{
			layer_attr.push( new NSI::StringArg( "scalarformat", "half" ) );
		}

		/*
			Automatically add Cryptomatte layers when appropriate.
			We chose to always add 2 layers, which provides room for 2x2=4 distinct
			values per pixel. It could be user-specified if this is ever requested.
		*/
		std::string driver_name = GetOutputDriverName(ro, od_file);
		if(	std::string(variablename, 0, 3) == "id." &&
			variablesource == "builtin" &&
			(driver_name == "exr" || driver_name == "deepalphaexr" ||
			 driver_name == "dwaaexr" || driver_name == "deepalphadwaaexr" ||
			 driver_name == "deepexr" ) )
		{
			cryptomatte_layers = 2;
		}
	}

	/* Don't send feedback info when exporting into a nsi file. */
	if( i_ctx.m_renderer.getRenderOutputFile() == "" )
    {
	if( i_feedback_data != std::string("") )
	{
		layer_attr.Add( new NSI::StringArg( "feedbackdata", i_feedback_data ) );
	}
    }

	/* Pixel Filter */
	if(variablename == "relighting_multiplier")
	{
		layer_attr.Add( new NSI::StringArg( "filter", "box" ) );
		layer_attr.Add( new NSI::DoubleArg( "filterwidth", 1.0 ) );
		// Setting "maximumvalue" is probably not a good idea in this case
	}
	else
	{
		std::string filter = i_ctx.m_renderSettings.GetPixelFilter();
		double filterwidth = i_ctx.m_renderSettings.GetFilterWidth();
		layer_attr.Add( new NSI::StringArg( "filter", filter ) );
		layer_attr.Add( new NSI::DoubleArg( "filterwidth", filterwidth ) );
		if(layertype == "color")
		{
			layer_attr.Add( new NSI::DoubleArg( "maximumvalue", 50.0) );
		}
	}

	i_ctx.m_nsi.SetAttribute( layer_handle, layer_attr );

	/* And then connect the layer to the camera. */
	std::string cameraName = ro.cameraName;
	if( cameraName == "" )
		cameraName = i_ctx.m_renderSettings.getCameraName();
	// Add "|screen" to the camera name to identify to get the screen node
	std::string screen_name = cameraName + "|screen";

	i_ctx.m_nsi.Connect( layer_handle, "", screen_name, "outputlayers" );

	/* Export the output drivers. */
	if( i_ctx.m_renderer.IsPreview() || i_ctx.m_renderer.IsLive() )
	{
		if( i_ctx.m_globalSettings.WantIDisplay() )
		{
			ExportOutputDriverForLayer(
				i_ctx, i_root, i_output_name, layer_handle, od_idisplay );
		}

		/*
			Only export monitor if the layer is one it expects.
		*/
		if( i_ctx.m_globalSettings.WantMonitor() && frame_id != -1 )
		{
			ExportOutputDriverForLayer(
				i_ctx, i_root, i_output_name, layer_handle, od_katana_monitor );
		}
	}
	else
	{
		if( cryptomatte_layers > 0 )
		{
			// Change the filter and output type to fit the Cryptomatte format
			i_ctx.m_nsi.SetAttribute(
				layer_handle,
				(
					NSI::StringArg( "filter", "cryptomatteheader" ),
					NSI::StringArg( "layertype", "color" ),
					NSI::IntegerArg( "withalpha", 0 )
				) );
		}

		ExportOutputDriverForLayer(
			i_ctx, i_root, i_output_name, layer_handle, od_file );

		if( cryptomatte_layers > 0 )
		{
			/*
				Export one additional layer per Cryptomatte level. Each will
				output 2 values from those present in each pixel's samples.
			*/
			char index[12];
			char filter[32];
			for(unsigned cl = 0; cl < cryptomatte_layers; cl++)
			{
				sprintf(index, "%u", cl);
				std::string cl_handle = layer_handle+index;
				i_ctx.m_nsi.Create(cl_handle, "outputlayer");

				i_ctx.m_nsi.SetAttribute(cl_handle, layer_attr);

				sprintf(filter, "cryptomattelayer%u", cl*2);
				i_ctx.m_nsi.SetAttribute(
					cl_handle,
					(
						NSI::StringArg( "filter", filter ),
						NSI::StringArg( "scalarformat", "float" ),
						NSI::StringArg( "layertype", "quad" ),
						NSI::IntegerArg( "withalpha", 0 ),
						NSI::IntegerArg( "sortkey", io_sort_key++ )
					) );

				i_ctx.m_nsi.Connect(cl_handle, "", screen_name, "outputlayers");

				ExportOutputDriverForLayer(
					i_ctx, i_root, i_output_name, cl_handle, od_file );
			}
		}
	}

	/* Connect the light set to the layer, if one was provided */
	if( i_light_set != std::string("") )
	{
		i_ctx.m_nsi.Connect( i_light_set, "", layer_handle, "lightset" );
	}
}

/**
	\brief Export one output driver for an output layer.

	\param i_ctx
		The export context.
	\param i_root
		The scene root iterator.
	\param i_output_name
		The render output's name. As in renderSettings.outputs.<name> on /root
	\param i_layer_handle
		The handle to the output layer we must connect.
	\param i_driver
		Specifies wich of several possible output drivers to produce.

	This will export the required output driver for an output layer, if it has
	not already been exported, and connect it to the layer. In simpler terms:
	it sends a layer somewhere (i-display, file, etc).
*/
void ExportOutputDriverForLayer(
	ExportContext &i_ctx,
	const FnKat::FnScenegraphIterator &i_root,
	const std::string &i_output_name,
	const std::string &i_layer_handle,
	eOutputDriver i_driver )
{
	FKR::RenderSettings::RenderOutput ro =
		i_ctx.m_renderSettings.getRenderOutputByName( i_output_name );

	std::string output_location;
	if( i_driver == od_katana_monitor )
	{
		/*
			This is unused by the monitor driver itself. The reason I set it to
			the output name is so we get a separate output driver for each
			layer as the monitor output driver does not currently handle all
			layers at once.
		*/
		output_location = i_output_name;
	}
	else
	{
		output_location = ro.renderLocation;
		if( output_location.empty() )
		{
			/*
				The above, for some reason, is not filled for preview renders.
				So we'll fetch the value directly from the attribute.
				Update: I suspect the "reason" is that preview renders are not
				supposed to write to files (at least not directly).
			*/
			GetString( i_root,
				AttrPath( "renderSettings.outputs", i_output_name.c_str(),
					"locationSettings.renderLocation" ),
				output_location );
		}

		output_location = ProcessOutputFileName( i_ctx, output_location );
	}

	std::string driver_type = GetOutputDriverName(ro, i_driver);

	/* Export the output driver node. */
	bool must_output_driver = false;
	/* Make sure to create only one driver for idisplay */
	std::string location_for_handle = 
		i_driver == od_idisplay ? driver_type : output_location;

	std::string driver_handle = i_ctx.OutputDriverHandle(
		location_for_handle,
		driver_type,
		&must_output_driver );

	if( must_output_driver )
	{
		NSI::ArgumentList args;
		args.push( new NSI::StringArg( "drivername", driver_type ) );
		args.push( new NSI::StringArg( "imagefilename", output_location ) );
		args.push( new NSI::IntegerArg( "embedstatistics", 1 ) );

		std::string feedbackhost;
		int feedbackport = -1;

		/* Get feedback host/port. This was set by the python server in the
		katana process. */
		const char *env_host = getenv( "DL_FEEDBACKHOST" );
		const char *env_port = getenv( "DL_FEEDBACKPORT" );
		if( env_host && env_port )
		{
			feedbackhost = env_host;
			feedbackport = atoi( env_port );
		}

		/* Always send it, i-display needs these to deal with re-render. */
		args.push( new NSI::StringArg( "sourceapp", "Katana" ) );

		/* Don't send feedback and pid info when exporting into a nsi file. */
		if( i_ctx.m_renderer.getRenderOutputFile() == "" )
		{
			args.push( new NSI::StringArg( "feedbackhost", feedbackhost ) );
			args.push( new NSI::IntegerArg( "feedbackport", feedbackport ) );
			/* Use feedbackPort as pid to ensure a unique pid for each Katana app. */
			if( env_port )
			{
				args.push( new NSI::StringArg( "pid", env_port ) );
			}
		}

		if( i_driver == od_katana_monitor )
		{
			/* Where to send data to katana. */
			args.push( new NSI::StringArg( "katana_host",
				i_ctx.m_renderer.getKatanaHost() ) );
			/* It also wants frame time. */
			args.push( new NSI::FloatArg( "frameTime",
				i_ctx.m_renderer.getRenderTime() ) );
		}

		i_ctx.m_nsi.Create( driver_handle, "outputdriver" );
		i_ctx.m_nsi.SetAttribute( driver_handle, args );
	}

	/* Connect output driver to the layer. */
	i_ctx.m_nsi.Connect( driver_handle, "", i_layer_handle, "outputdrivers" );
}

/**
	\brief Process placeholders in an output file name.

	\param i_ctx
		The export context.
	\param i_filename
		The filename to process.
	\return The processed filename.

	Replaces placeholders such as # for a frame number, <> tokens, etc.
*/
std::string ProcessOutputFileName(
	ExportContext &i_ctx,
	const std::string &i_filename )
{
	// Replace # (before extension) by a frame number (we use the render time)
	// (see InitImageLayersUI in Editor.py of DelightSettings)
	std::string filename;
	size_t pos = i_filename.rfind("_#.");
	if (pos != std::string::npos)
	{
		filename = i_filename.substr(0, pos+1);
		filename += std::to_string( int(i_ctx.m_renderer.getRenderTime()) );
		filename += i_filename.substr(pos+2);
	}
	else
	{
		filename = i_filename;
	}

	return filename;
}

/**
	\brief Returns the multi-light set name for a given output.

	\param i_ctx
		The export context
	\param i_root
		The scene root iterator.
	\param i_output_name
		The render output's name. As in renderSettings.outputs.<name> on /root
	\param o_light_set
		The light set name.
*/

void GetLight(
	ExportContext &i_ctx,
	const FnKat::FnScenegraphIterator &i_root,
	std::string i_output_name,
	std::string& o_light_set,
	std::string& o_feedback_data )
{
	using AttributeUtils::GetString;

	/* Handle the independent lights selected for multi-light output */
	RenderSettings::RenderOutput output =
		i_ctx.m_renderSettings.getRenderOutputByName(i_output_name);

	RenderSettings::AttributeSettings attrSettings = output.rendererSettings;
	FnAttribute::Attribute lightSetAttr = attrSettings["lightSet"];

	std::string lightSet;
	GetString( lightSetAttr, lightSet );

	if (!lightSet.empty())
	{
		/* Use the transform as a light set. This is needed for mesh light
		   which does not have a |leaf. */
		o_light_set = lightSet;

		FnKat::FnScenegraphIterator lightSetLocation =
			i_root.getByPath(lightSet.c_str());

		FnAttribute::StringAttribute gafferName =
			lightSetLocation.getAttribute( "info.gaffer.gafferName" );

		std::string gaffer;
		if( gafferName.isValid() )
		{
			gaffer = gafferName.getValue();
		}

		if( !gaffer.empty() )
		{
			std::string type = "single light";
			std::string objects;
			std::string attributes;
			std::string values;
			std::string color_attributes;
			std::string color_values;

			/* 
				If the light set location is a rig, iterate on its children and
				append their feedback data.
			*/
			if( lightSetLocation.getType() == std::string("rig") )
			{
				type = "multiple lights";

				FnKat::FnScenegraphIterator child = 
					lightSetLocation.getFirstChild();

				while( child.isValid() )
				{
					AppendLightFeedbackData( 
						child, objects, attributes, values,
						color_attributes, color_values );
					
					child = child.getNextSibling();
				}
			}
			else
			{
				AppendLightFeedbackData(
					lightSetLocation, objects, attributes, values,
					color_attributes, color_values);
			}

			/* Make the feedback message */
			/* Name */
			std::string name = lightSet.c_str();
			o_feedback_data = "{\"name\":\"";
			o_feedback_data += name.substr(strlen("/root/world/"));
			o_feedback_data += "\",";

			/* Type */
			o_feedback_data += "\"type\":\"" + type + "\",";

			/* Renderer */
			o_feedback_data += "\"gaffer\":\"";
			o_feedback_data += gaffer;
			o_feedback_data += "\",";

			/* Attribute. Save the name of the object  */
			o_feedback_data += "\"objects\":[";
			o_feedback_data += objects;
			o_feedback_data += "],";

			/* Attribute. Save the name of intensity attribute  */
			o_feedback_data += "\"attributes\":[";
			o_feedback_data += attributes;
			o_feedback_data += "],";

			/* Value */
			o_feedback_data += "\"values\":[";
			o_feedback_data += values;
			o_feedback_data += "],";

			/* Color attribute. Save the name of color attribute  */
			o_feedback_data += "\"color_attributes\":[";
			o_feedback_data += color_attributes;
			o_feedback_data += "],";

			/* Color values */
			o_feedback_data += "\"color_values\":[";
			o_feedback_data += color_values;
			o_feedback_data += "]}";
		}
	}
}

void AppendLightFeedbackData(
	FnKat::FnScenegraphIterator& i_light,
	std::string& o_objects,
	std::string& o_attributes,
	std::string& o_values,
	std::string& o_color_attributes,
	std::string& o_color_values )
{
	std::string light_shader_attr_name =
		"material."
		_3DELIGHT_RENDERER_NAME
		_3DELIGHT_LIGHT_TERMINAL
		_3DELIGHT_SHADER;

	std::string env_shader_attr_name =
		"material."
		_3DELIGHT_RENDERER_NAME
		_3DELIGHT_ENVIRONMENT_TERMINAL
		_3DELIGHT_SHADER;

	FnAttribute::StringAttribute shader_attr =
		i_light.getAttribute( light_shader_attr_name );

	/* Figure intensity attribute name based on the shader type. */
	std::string intensity_attr_name;
	if( shader_attr.isValid() )
	{
		/* Light shader. */
		intensity_attr_name =
			_3DELIGHT_RENDERER_NAME
			_3DELIGHT_LIGHT_TERMINAL
			_3DELIGHT_PARAMS
			".intensity";			
	}
	else
	{
		shader_attr = i_light.getAttribute( env_shader_attr_name );
		if( shader_attr.isValid() )
		{
			/* Environment shader */
			intensity_attr_name =
				_3DELIGHT_RENDERER_NAME
				_3DELIGHT_ENVIRONMENT_TERMINAL
				_3DELIGHT_PARAMS
				".intensity";			
		}
	}

	float intensity = 1.0f;
	if( !intensity_attr_name.empty() )
	{
		FnAttribute::FloatAttribute intensity_attr =
			i_light.getAttribute( "material." + intensity_attr_name );

		if( intensity_attr.isValid() )
		{
			intensity = intensity_attr.getValue();
		}
	}

	/* Build color attribute from intensity attribute. */
	std::string color_attr_name = intensity_attr_name;
	if( color_attr_name.size() > 10 )
	{
		color_attr_name.replace(
			color_attr_name.end() - 10, color_attr_name.end(), ".color");
	}

	/* Gets color value from color attribute. */
	float color[3] = {1.0f, 1.0f, 1.0f};
	if( !color_attr_name.empty() )
	{
		FnAttribute::FloatAttribute color_attr =
			i_light.getAttribute( "material." + color_attr_name );

		if( color_attr.isValid() )
		{
			color_attr.fillInterpSample(color, 3, 0);
		}
	}

	if( !o_objects.empty() )
	{
		o_objects += ",";
	}
	if( !o_attributes.empty() )
	{
		o_attributes += ",";
	}
	if( !o_values.empty() )
	{
		o_values += ",";
	}
	if( !o_color_attributes.empty() )
	{
		o_color_attributes += ",";
	}
	if( !o_color_values.empty() )
	{
		o_color_values += ",";
	}

	o_objects += "\"" + i_light.getFullName() + "\"";
	o_attributes += "\"" + intensity_attr_name + "\"";
	o_values += Utils::Float2Str( intensity );
	o_color_attributes += "\"" + color_attr_name + "\"";
	o_color_values += Utils::Color2Str( color );
}

}

