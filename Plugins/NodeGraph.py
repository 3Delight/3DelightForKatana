from Katana import NodegraphAPI, Callbacks, Utils
import os

def onSceneLoad(**kwargs):
	nodes = NodegraphAPI.GetAllNodesByType('DlSettings')
	for node in nodes:
		Utils.EventModule.RegisterCollapsedHandler(
			node.UpdateCamLocation1EH, 'port_connect')
		Utils.EventModule.RegisterCollapsedHandler(
			node.UpdateCamLocation2EH, 'parameter_setValue')
		Utils.EventModule.RegisterCollapsedHandler(
			node.ValidateEH, 'parameter_setValue')

def onRenderStarted(eventType, eventId, node, renderMethodName, renderMethodType, renderer):
	notesName = NodegraphAPI.GetKatanaSceneName()
	notesName += ".katana"
	notesName += " | "
	# node is None when rendering only dependencies of a Render node.
	nodeName = node.getName() if node else 'unknown'
	notesName += nodeName
	os.environ['DL_KATANA_ROOT_NAME'] = nodeName
	os.environ['DLC_KATANA_INFO'] = notesName

Callbacks.addCallback(Callbacks.Type.onSceneLoad, onSceneLoad)
Utils.EventModule.RegisterEventHandler(onRenderStarted, "renderStarted")
