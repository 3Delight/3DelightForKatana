import ConfigurationAPI_cmodule as Configuration
import os

def PrefChanged(eventType, eventID, prefKey, prefValue):
	if '3Delight/' not in prefKey[0:9]: return
	Configuration.set(prefKey, str(prefValue))

def _settingToString(v):
	if hasattr(v, 'toString'):
		# Katana 3.0 returns a QVariant.
		return str(v.toString())
	else:
		# Katana 3.1 returns the primitive type directly (eg. bool)
		return str(v)

def buildPrefs3DelightUI():
	"""
	Builds UI for the 3Delight preferences section.
	"""

	try:
		from Katana import KatanaPrefs, Utils
	except ImportError:
		return

	Utils.EventModule.RegisterEventHandler(PrefChanged, 'pref_changed')

	KatanaPrefs.declareGroupPref('3Delight')
	Hints3 = {'widget': 'mapper', 'options': [('3delight Display', '0'),
		('Katana Monitor', '1'), ('Both', '2')]}
	KatanaPrefs.declareStringPref('3Delight/renderView', '2',
		'Specifies which Render View is used or are used.', hints=Hints3)

	settings = KatanaPrefs._KatanaPrefsObject__sessionSettings
	# KatanaPrefs seems to store first value of mapper (i.e. 'Both'
	# insted of '2') if no Prefs file has been read...
	value = _settingToString(settings.value('3Delight/renderView'))
	if not value.isdigit(): value = '2'
	Configuration.set('3Delight/renderView', value)

	value = _settingToString(settings.value('3Delight/textureDirectory'))
	Configuration.set('3Delight/textureDirectory', value)

buildPrefs3DelightUI()
