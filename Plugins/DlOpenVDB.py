# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2017                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

from Nodes3DAPI.Manifest import NodegraphAPI
from Katana import Utils
import ConfigurationAPI_cmodule as Configuration

import platform
from ctypes import *

# Load 3Delight
if platform.system() == "Windows":
    _lib3delight = cdll.LoadLibrary('3Delight')
else:
    _lib3delight = cdll.LoadLibrary('lib3delight.so')

def registerDlOpenVDB():
    """
    Registers a new DlOpenVDB node type using the NodeTypeBuilder utility class.
    """

    from Katana import FnAttribute
    from Katana import Nodes3DAPI
    from Nodes3DAPI.Manifest import FnGeolibServices

    def _getGridName(node, frame_time, param_name):
        """
            Fetch a grid name from a parameter and handle the special value
            used when no grid is selected.
        """
        g = node.getParameter(param_name).getValue(frame_time)
        if g == '<none>':
            g = ''
        return g

    def buildDlOpenVDBOpChain(node, interface):
        """
        Callback function that creates the Op chain for a NSIArchive node.

        @type node: C{Nodes3DAPI.NodeTypeBuilder.NSIArchive}
        @type interface: C{Nodes3DAPI.NodeTypeBuilder.BuildChainInterface}
        @param node: The node for which to define the Ops chain
        @param interface: The interface providing the functions needed to set
            up the Ops chain for the given node.
        """

        # Get the current frame time
        frameTime = interface.getGraphState().getTime()

        # Set the minimum number of input ports
        interface.setMinRequiredInputs(0)

        # Scene state
        sscb = FnGeolibServices.OpArgsBuilders.StaticSceneCreate()

        # Figure the scene graph location
        nameParam = node.getParameter('name')
        locationPath = nameParam.getValue(frameTime)

        # Create an object at that location
        sscb.createEmptyLocation(locationPath, "openvdb")

        # Copy parameters to the group
        args_group = FnAttribute.GroupBuilder()
        filename = node.getParameter('file.filename').getValue(frameTime)
        args_group.set('filename', filename)

        grids_group = FnAttribute.GroupBuilder()
        grids_group.set(
            'smokeDensityGrid',
            _getGridName(node, frameTime, 'grids.smokeDensityGrid'))
        grids_group.set(
            'smokeColorGrid',
            _getGridName(node, frameTime, 'grids.smokeColorGrid'))
        grids_group.set(
            'temperatureGrid',
            _getGridName(node, frameTime, 'grids.temperatureGrid'))
        grids_group.set(
            'emissionIntensityGrid',
            _getGridName(node, frameTime, 'grids.emissionIntensityGrid'))
        args_group.set('grids', grids_group.build())

        motion_group = FnAttribute.GroupBuilder()
        motion_group.set(
            'velocityGrid',
            _getGridName(node, frameTime, 'motion.velocityGrid'))
        motion_group.set(
            'velocityScale',
            FnAttribute.DoubleAttribute(
                node.getParameter('motion.velocityScale').getValue(frameTime)))
        motion_group.set(
            'fps',
            FnAttribute.DoubleAttribute(
                node.getParameter('motion.fps').getValue(frameTime)))
        args_group.set('motion', motion_group.build())

        viewport_group = FnAttribute.GroupBuilder()
        drawing = node.getParameter('viewport.drawing').getValue(frameTime)
        viewport_group.set('drawing', drawing)

        args_group.set('viewport', viewport_group.build())

        bbox_attr = [ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ]

        # Retrieve metadata for bounding box and filename
        nameParam = node.getParameter('name')
        nameHints = eval(nameParam.getHintString())
        oldBbox_attr = eval(nameHints['bbox_attr'])

        filenameParam = node.getParameter('file.filename')
        filenameHints = eval(filenameParam.getHintString())
        oldFilename = filenameHints['filename']
        
        if filename and (filename != oldFilename or oldBbox_attr == bbox_attr):

            # Read bounding box for all grids
            VDBGetFileBBox = _lib3delight.DlVDBGetFileBBox
            VDBGetFileBBox.restype = c_bool
            VDBGetFileBBox.argtypes = [c_char_p, POINTER(c_double)]

            arg1 = create_string_buffer(filename.encode('utf-8'))
            SixDoubles = c_double * 6
            arg2 = SixDoubles(0, 0, 0, 0, 0, 0)
            res = VDBGetFileBBox(arg1, arg2)

            if not res:
                sscb.setAttrAtLocation(
                    locationPath,
                    'errorMessage',
                    FnAttribute.StringAttribute(
                        "No bounding box read because file is empty or cannot be open."))
                interface.appendOp('StaticSceneCreate', sscb.build())
                return

            # 3Delight returns [min vec, max vec].
            # The attribute expects [xmin, xmax, ymin, ymax, zmin, zmax].
            bbox_value = [arg2[0], arg2[3], arg2[1], arg2[4], arg2[2], arg2[5]]

            bbox_attr = FnAttribute.DoubleAttribute(bbox_value).getData()
            oldBbox_attr = bbox_attr
            nameHints['bbox_attr'] = repr(oldBbox_attr);
            nameParam.setHintString(repr(nameHints))

        else:
            if not filename and oldBbox_attr != bbox_attr:
                oldBbox_attr = bbox_attr
                nameHints['bbox_attr'] = repr(oldBbox_attr);
                nameParam.setHintString(repr(nameHints))

        if oldFilename != filename:
            filenameHints['filename'] = filename;
            filenameParam.setHintString(repr(filenameHints))

        # Put the group to the object
        sscb.setAttrAtLocation(locationPath, 'openvdb', args_group.build())

        sscb.setAttrAtLocation(locationPath, 'bound', FnAttribute.DoubleAttribute(oldBbox_attr, 2))

        # Add transforms attributes.
        argsGb = FnAttribute.GroupBuilder()
        xformGroup = interface.getTransformAsAttribute()

        # The transform attribute group name depends on the value of 
        # "makeInteractive".
        interactiveParam = node.getParameter('makeInteractive')
        interactive = interactiveParam.getValue(frameTime)

        xformGroupName = 'group0'
        if interactive == 'Yes':
            xformGroupName = 'interactive'

            # Also add the exclusiveTo, which appears to be expected with an
            # interactive transform
            exclusiveTo = interface.getExclusiveToNameAndAttribute()
            sscb.setAttrAtLocation(
                locationPath, 
                exclusiveTo.name,
                exclusiveTo.attr)

        argsGb.set(xformGroupName, xformGroup)

        # Put the interactive attribute group under 'xform' on the SG
        # location
        sscb.setAttrAtLocation(locationPath, 'xform', argsGb.build())

        # Save the scene state
        interface.appendOp('StaticSceneCreate', sscb.build())

    def buildParameters( node ):
        '''
        Callback function that may create or alter node parameters before its
        creation is completed.
        '''
        name_param = node.getParameter('name')

        node_name = node.getName()
        name = '/root/world/geo/' + node_name[2:].lower()
        name_param.setValue(name, 0)

        # Should set the hint string to be able to modify it later with
        # UpdateOptionsHints
        InitOptionsHints(node, 'grids.smokeDensityGrid')
        InitOptionsHints(node, 'grids.smokeColorGrid')
        InitOptionsHints(node, 'grids.temperatureGrid')
        InitOptionsHints(node, 'grids.emissionIntensityGrid')
        InitOptionsHints(node, 'motion.velocityGrid')

        # Store metadata for bbox and filename
        name_hints = {'bbox_attr': '[0.0, 0.0, 0.0, 0.0, 0.0, 0.0]'}
        hints = name_param.getHintString()
        if not hints:
            name_param.setHintString(repr(name_hints))

        filename_hints = {'filename': ''}
        filename_param = node.getParameter('file.filename')
        hints = filename_param.getHintString()
        if not hints:
            filename_param.setHintString(repr(filename_hints))

    def returnScenegraphLocation( node, frameTime ):
        '''
        Callback function used to return scene graph location path
        for drag and drop
        '''
        name_param = node.getParameter('name')

        path = name_param.getValue(frameTime)
        return path

    def InitOptionsHints(node, parametername):
        gridParam = node.getParameter(parametername)
        hints = gridParam.getHintString()
        if not hints:
            hints = {'options': '<none>'}
            gridParam.setHintString(repr(hints))

    def UpdateOptionsHints(node, parametername, options):
        gridParam = node.getParameter(parametername)
        hints = eval(gridParam.getHintString())
        hints['options'] = options;
        gridParam.setHintString(repr(hints))
        if options == '<none>':
            gridParam.setValue(options, 0)

    # Update options in hints of the 3 different gridnames
    def UpdateEH(args):
        if 'param' not in args[0][2]:
            return

        param = args[0][2]['param']
        fullName = param.getFullName()

        # Filename has been modified: we must update options in hints
        if '.file.filename' in fullName:

            frameTime = NodegraphAPI.GetCurrentGraphState().getTime()
            filename = param.getValue(frameTime)

            nodeArg = args[0][2]['node']
            nodeName = nodeArg.getName()
            node = NodegraphAPI.GetNode(nodeName)

            filenameParam = node.getParameter('file.filename')
            filenameHints = eval(filenameParam.getHintString())
            oldFilename = filenameHints['filename']

            if filename or oldFilename:

                res = c_bool(False)
                if filename:

                    # Read all grids in vdb file
                    VDBGetGridNames = _lib3delight.DlVDBGetGridNames
                    VDBGetGridNames.restype = c_bool
                    VDBGetGridNames.argtypes = [
                        c_char_p, POINTER(c_int), POINTER(POINTER(c_char_p))]

                    arg1 = create_string_buffer(filename.encode('utf-8'))
                    arg2 = c_int()
                    arg3 = (POINTER(c_char_p))()
                    res = VDBGetGridNames(arg1, byref(arg2), byref(arg3))

                    # Reinitialize metadata of bbox_attr to ensure that bounding
                    # box will be update in buildDlOpenVDBOpChain
                    nameParam = node.getParameter('name')
                    bbox_attr = [ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ]
                    nameHints = eval(nameParam.getHintString())
                    nameHints['bbox_attr'] = repr(bbox_attr);
                    nameParam.setHintString(repr(nameHints))

                # Build new options for each grid name
                options = '<none>'
                nb = 0
                if res:
                    nb = arg2.value
                for i in range(nb):
                    options = options + '|'
                    options = options + arg3[i].decode('utf-8')

                # Cleanup buffer.
                if res:
                    _lib3delight.DlVDBFreeGridNames(arg3)

                # Update hints for all gridnames
                UpdateOptionsHints(node, 'grids.smokeDensityGrid', options)
                UpdateOptionsHints(node, 'grids.smokeColorGrid', options)
                UpdateOptionsHints(node, 'grids.temperatureGrid', options)
                UpdateOptionsHints(node, 'grids.emissionIntensityGrid', options)
                UpdateOptionsHints(node, 'motion.velocityGrid', options)

                if oldFilename != filename:
                    filenameHints['filename'] = filename;
                    filenameParam.setHintString(repr(filenameHints))

    # Create a NodeTypeBuilder to register the new type
    nodeTypeBuilder = Nodes3DAPI.NodeTypeBuilder('DlOpenVDB')

    # Build the node's parameters
    gb = FnAttribute.GroupBuilder()

    gb.set(
        'name',
        FnAttribute.StringAttribute('/root/world/geo/openvdb'))

    gb.set('file', FnAttribute.GroupAttribute())
    gb.set('file.filename', FnAttribute.StringAttribute(''))
    gb.set('grids', FnAttribute.GroupAttribute())
    gb.set('grids.smokeDensityGrid', FnAttribute.StringAttribute('<none>'))
    gb.set('grids.smokeColorGrid', FnAttribute.StringAttribute('<none>'))
    gb.set('grids.temperatureGrid', FnAttribute.StringAttribute('<none>'))
    gb.set('grids.emissionIntensityGrid', FnAttribute.StringAttribute('<none>'))
    gb.set('motion.velocityGrid', FnAttribute.StringAttribute('<none>'))
    gb.set('motion.velocityScale', FnAttribute.DoubleAttribute(1))
    gb.set('motion.fps', FnAttribute.DoubleAttribute(24))
    gb.set('viewport', FnAttribute.GroupAttribute())
    gb.set('viewport.drawing', FnAttribute.IntAttribute(0))

    # Add transform parameters
    nodeTypeBuilder.addTransformParameters(gb)
    nodeTypeBuilder.addInteractiveTransformCallbacks(gb)
    nodeTypeBuilder.addMakeInteractiveParameter(gb)

    # Set the parameters template
    nodeTypeBuilder.setParametersTemplateAttr(gb.build())

    # Set parameter hints
    nodeTypeBuilder.setHintsForParameter(
        'name',
        {
            'widget':'newScenegraphLocation'
        })

    nodeTypeBuilder.setHintsForParameter(
        'file', 
        {
            'label':'File',
            'open':'True'
        })

    nodeTypeBuilder.setHintsForParameter(
        'file.filename', 
        {
            'widget':'assetIdInput', 
            'fileTypes':'vdb',
            'help': "A path to some vdb file",
            'label':'File name'
        })

    nodeTypeBuilder.setHintsForParameter(
        'grids', 
        {
            'label':'Grids'
        })

    nodeTypeBuilder.setHintsForParameter(
        'grids.smokeDensityGrid', 
        {
            'widget':'popup', 
            'label':'Smoke Density Grid'
        })

    nodeTypeBuilder.setHintsForParameter(
        'grids.smokeColorGrid',
        {
            'widget':'popup',
            'label':'Smoke Color Grid'
        })

    nodeTypeBuilder.setHintsForParameter(
        'grids.temperatureGrid', 
        {
            'widget':'popup', 
            'label':'Temperature Grid'
        })

    nodeTypeBuilder.setHintsForParameter(
        'grids.emissionIntensityGrid', 
        {
            'widget':'popup', 
            'label':'Emission Intensity Grid'
        })

    nodeTypeBuilder.setHintsForParameter(
        'motion',
        {
            'label':'Motion'
        })

    nodeTypeBuilder.setHintsForParameter(
        'motion.velocityGrid',
        {
            'widget':'popup',
            'label':'Velocity Grid',
            'help':'A vector grid used to generate motion blur.'
                ' May also be the first of three scalar grids which are used'
                ' together to generate motion blur (eg. velocityX).'
        })

    nodeTypeBuilder.setHintsForParameter(
        'motion.velocityScale',
        {
            'label':'Velocity Scale',
            'help':'Adjusts the amount of motion blur.'
                ' Note that visible motion blur will also change with the camera'
                ' shutter angle.'
        })

    nodeTypeBuilder.setHintsForParameter(
        'motion.fps',
        {
            'label':'fps',
            'help':'Frames per second.'
                ' Used to scale velocity, which is defined per second.'
        })

    nodeTypeBuilder.setHintsForParameter(
        'viewport', 
        {
            'label':'Viewport'
        })

    nodeTypeBuilder.setHintsForParameter(
        'viewport.drawing', 
        {
            'widget':'mapper', 
            'label':'',
            'options':'Box:0|Points:1',
            'constant':'True'
        })

    # Set the callback responsible to build the Ops chain
    nodeTypeBuilder.setBuildOpChainFnc(buildDlOpenVDBOpChain)

    # Set the callback that allow gives us a chance to alter param values upon 
    # node creation. We use this to define a name param value based on the node
    # name.
    #
    nodeTypeBuilder.setBuildParametersFnc(buildParameters)

    # Set the callback to return scene graph location path
    nodeTypeBuilder.setGetScenegraphLocationFnc(returnScenegraphLocation)

    # Build the new node type
    nodeTypeBuilder.build()

    Utils.EventModule.RegisterCollapsedHandler(
        UpdateEH, 'parameter_finalizeValue')

def registerDlOpenVDBUI():
    """
    Registers UI for the NSIArchive node type.
    """
    try:
        from Katana import UI4
    except ImportError:
        return

    # Register a policy delegate to give all parameter widgets mini state badges
    parameterPolicyDelegate = UI4.FormMaster.EnableableScalarPolicyDelegate()

    UI4.FormMaster.ParameterPolicy.RegisterPolicyDelegate(
        'DlOpenVDB',
        parameterPolicyDelegate)

# Register the node
registerDlOpenVDB()

# UI stuff if available
if Configuration.get('KATANA_UI_MODE'):
    registerDlOpenVDBUI()

# vim: set softtabstop=4 expandtab shiftwidth=4:
