# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2017                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

import ConfigurationAPI_cmodule as Configuration

def registerDlNSIArchive():
    """
    Registers a new NSIArchive node type using the NodeTypeBuilder utility class.
    """

    from Katana import FnAttribute
    from Katana import Nodes3DAPI
    from Nodes3DAPI.Manifest import FnGeolibServices
    import os.path

    def buildDlNSIArchiveOpChain(node, interface):
        """
        Callback function that creates the Op chain for a NSIArchive node.

        @type node: C{Nodes3DAPI.NodeTypeBuilder.NSIArchive}
        @type interface: C{Nodes3DAPI.NodeTypeBuilder.BuildChainInterface}
        @param node: The node for which to define the Ops chain
        @param interface: The interface providing the functions needed to set
            up the Ops chain for the given node.
        """

        import json

        # Get the current frame time
        frameTime = interface.getGraphState().getTime()

        # Set the minimum number of input ports
        interface.setMinRequiredInputs(0)

        # Scene state
        sscb = FnGeolibServices.OpArgsBuilders.StaticSceneCreate()

        # Figure the scene graph location
        nameParam = node.getParameter('name')
        locationPath = nameParam.getValue(frameTime)

        # Create an object at that location
        sscb.createEmptyLocation(locationPath, "nsiarchive")

        # Copy node parameters to the attribute group
        archiveGb = FnAttribute.GroupBuilder()

        filename = node.getParameter('file').getValue(frameTime)

        if filename != "":

            # Initialize other attributes from complementary "sidecar" file
            nsj_name = ""
            if os.path.splitext(filename)[1] == ".nsi":
                nsj_name = os.path.splitext(filename)[0] + ".nsj"
            else:
                nsj_name = filename + ".nsj"

            try:
                nsj_file = open(nsj_name, "r")

                nsj = json.load(nsj_file)
                nsj_file.close()

                if "boundingbox" in nsj:
                    bbox = nsj["boundingbox"]

                    bbox_attr = [ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ]

                    if "min" in bbox:
                        bbmin = bbox["min"]
                        if len(bbmin) == 3:
                            bbox_attr[0] = bbmin[0]
                            bbox_attr[2] = bbmin[1]
                            bbox_attr[4] = bbmin[2]

                    if "max" in bbox:
                        bbmax = bbox["max"]
                        if len(bbmax) == 3:
                            bbox_attr[1] = bbmax[0]
                            bbox_attr[3] = bbmax[1]
                            bbox_attr[5] = bbmax[2]

                    sscb.setAttrAtLocation(
                        locationPath,
                        'bound',
                        FnAttribute.DoubleAttribute(bbox_attr, 2))

                archiveGb.set('file', filename)

            except IOError:
                # There will be no bounding box for the scene graph location,
                # but that is not a problem.
                pass


        # Put the attribute group to the object - this first group appears to
        # define the SG location type.
        sscb.setAttrAtLocation(locationPath, 'nsiarchive', archiveGb.build())

        # Add transforms attributes.
        argsGb = FnAttribute.GroupBuilder()
        xformGroup = interface.getTransformAsAttribute()

        # The transform attribute group name depends on the value of 
        # "makeInteractive".
        interactiveParam = node.getParameter('makeInteractive')
        interactive = interactiveParam.getValue(frameTime)

        xformGroupName = 'group0'
        if interactive == 'Yes':
            xformGroupName = 'interactive'

            # Also add the exclusiveTo, which appears to be expected with an
            # interactive transform
            exclusiveTo = interface.getExclusiveToNameAndAttribute()
            sscb.setAttrAtLocation(
                locationPath, 
                exclusiveTo.name,
                exclusiveTo.attr)

        argsGb.set(xformGroupName, xformGroup)

        # Put the interactive attribute group under 'xform' on the SG
        # location
        sscb.setAttrAtLocation(locationPath, 'xform', argsGb.build())

        # Save the scene state
        interface.appendOp('StaticSceneCreate', sscb.build())

    def buildParameters( node ):
        '''
        Callback function that may create or alter node parameters before its
        creation is completed.
        '''
        name_param = node.getParameter('name')

        name = '/root/world/geo/' + node.getName()
        name_param.setValue(name, 0)

    def returnScenegraphLocation( node, frameTime ):
        '''
        Callback function used to return scene graph location path
        for drag and drop
        '''
        name_param = node.getParameter('name')

        path = name_param.getValue(frameTime)
        return path

    # Create a NodeTypeBuilder to register the new type
    nodeTypeBuilder = Nodes3DAPI.NodeTypeBuilder('DlNSIArchive')

    # Build the node's parameters
    gb = FnAttribute.GroupBuilder()

    gb.set(
        'name',
        FnAttribute.StringAttribute('/root/world/geo/nsiarchive'))

    gb.set('file', FnAttribute.StringAttribute(''))

    # Add transform parameters
    nodeTypeBuilder.addTransformParameters(gb)
    nodeTypeBuilder.addInteractiveTransformCallbacks(gb)
    nodeTypeBuilder.addMakeInteractiveParameter(gb)

    # Set the parameters template
    nodeTypeBuilder.setParametersTemplateAttr(gb.build())

    # Set parameter hints
    nodeTypeBuilder.setHintsForParameter(
        'name',
        {'widget':'newScenegraphLocation', 'label':'Name'})

    nodeTypeBuilder.setHintsForParameter(
        'file', 
        {
            'widget':'assetIdInput', 
            'fileTypes':'nsi',
            'help': "A path to some NSI archive file",
            'label':'NSI Archive File'
        })

    # Set the callback responsible to build the Ops chain
    nodeTypeBuilder.setBuildOpChainFnc(buildDlNSIArchiveOpChain)

    # Set the callback that allow gives us a chance to alter param values upon 
    # node creation. We use this to define a name param value based on the node
    # name.
    #
    nodeTypeBuilder.setBuildParametersFnc(buildParameters)

    # Set the callback to return scene graph location path
    nodeTypeBuilder.setGetScenegraphLocationFnc(returnScenegraphLocation)

    # Build the new node type
    nodeTypeBuilder.build()

def registerDlNSIArchiveUI():
    """
    Registers UI for the NSIArchive node type.
    """
    try:
        from Katana import UI4
    except ImportError:
        return

    # Register a policy delegate to give all parameter widgets mini state badges
    parameterPolicyDelegate = UI4.FormMaster.EnableableScalarPolicyDelegate()

    UI4.FormMaster.ParameterPolicy.RegisterPolicyDelegate(
        'DlNSIArchive',
        parameterPolicyDelegate)

# Register the node
registerDlNSIArchive()

# UI stuff if available
if Configuration.get('KATANA_UI_MODE'):
    registerDlNSIArchiveUI()
