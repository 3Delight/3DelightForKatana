# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2017                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

import ConfigurationAPI_cmodule as Configuration

def registerDlYeti():
    """
    Registers a new DlYeti node type using the NodeTypeBuilder utility class.
    """

    from Katana import FnAttribute
    from Katana import Nodes3DAPI
    from Nodes3DAPI.Manifest import FnGeolibServices

    def buildDlYetiOpChain(node, interface):
        """
        Callback function that creates the Op chain for a DlYeti node.

        @type node: C{Nodes3DAPI.NodeTypeBuilder.DlYeti}
        @type interface: C{Nodes3DAPI.NodeTypeBuilder.BuildChainInterface}
        @param node: The node for which to define the Ops chain
        @param interface: The interface providing the functions needed to set
            up the Ops chain for the given node.
        """

        # Get the current frame time
        frameTime = interface.getGraphState().getTime()

        # Set the minimum number of input ports
        interface.setMinRequiredInputs(0)

        # Scene state
        sscb = FnGeolibServices.OpArgsBuilders.StaticSceneCreate()

        # Figure the scene graph location
        nameParam = node.getParameter('name')
        locationPath = nameParam.getValue(frameTime)

        # Create an object at that location
        sscb.createEmptyLocation(locationPath, "yeti")

        # Copy node parameters to the attribute group
        yetiGb = FnAttribute.GroupBuilder()

        filename = node.getParameter('file').getValue(frameTime)
        yetiGb.set('file', filename)

        density = node.getParameter('density').getValue(frameTime)
        yetiGb.set('density', density)

        width = node.getParameter('width').getValue(frameTime)
        yetiGb.set('width', width)

        length = node.getParameter('length').getValue(frameTime)
        yetiGb.set('length', length)

        # Also define a framenumber attribute, which lets the user override the
        # frame number used to expand the Yeti fur cache file name. This will
        # also probably be needed in a future viewer modifier plug-in.
        yetiGb.set('frame', frameTime)

        verbosity = node.getParameter('verbosity').getValue(frameTime)
        yetiGb.set('verbosity', verbosity)

        paths = node.getParameter('imagesearchpaths').getValue(frameTime)
        yetiGb.set('imagesearchpaths', paths)

        # Put the attribute group to the object - this first group appears to
        # define the SG location type.
        sscb.setAttrAtLocation(locationPath, 'yeti', yetiGb.build())

        # Add transforms attributes.
        argsGb = FnAttribute.GroupBuilder()
        xformGroup = interface.getTransformAsAttribute()

        # The transform attribute group name depends on the value of 
        # "makeInteractive".
        interactiveParam = node.getParameter('makeInteractive')
        interactive = interactiveParam.getValue(frameTime)

        xformGroupName = 'group0'
        if interactive == 'Yes':
            xformGroupName = 'interactive'

            # Also add the exclusiveTo, which appears to be expected with an
            # interactive transform
            exclusiveTo = interface.getExclusiveToNameAndAttribute()
            sscb.setAttrAtLocation(
                locationPath, 
                exclusiveTo.name,
                exclusiveTo.attr)

        argsGb.set(xformGroupName, xformGroup)

        # Put the interactive attribute group under 'xform' on the SG
        # location
        sscb.setAttrAtLocation(locationPath, 'xform', argsGb.build())

        # Save the scene state
        interface.appendOp('StaticSceneCreate', sscb.build())

    def buildParameters( node ):
        '''
        Callback function that may create or alter node parameters before its
        creation is completed.
        '''
        name_param = node.getParameter('name')

        name = '/root/world/geo/' + node.getName()
        name_param.setValue(name, 0)

    def returnScenegraphLocation( node, frameTime ):
        '''
        Callback function used to return scene graph location path
        for drag and drop
        '''
        name_param = node.getParameter('name')

        path = name_param.getValue(frameTime)
        return path

    # Create a NodeTypeBuilder to register the new type
    nodeTypeBuilder = Nodes3DAPI.NodeTypeBuilder('DlYeti')

    # Build the node's parameters
    gb = FnAttribute.GroupBuilder()


    gb.set(
        'name',
        FnAttribute.StringAttribute('/root/world/geo/yeti'))

    gb.set('file', FnAttribute.StringAttribute(''))
    gb.set('density', FnAttribute.FloatAttribute(10.0))
    gb.set('width', FnAttribute.FloatAttribute(1.0))
    gb.set('length', FnAttribute.FloatAttribute(1.0))
    gb.set('verbosity', FnAttribute.IntAttribute(1))
    gb.set('imagesearchpaths', FnAttribute.StringAttribute(''))

    # Add transform parameters
    nodeTypeBuilder.addTransformParameters(gb)
    nodeTypeBuilder.addInteractiveTransformCallbacks(gb)
    nodeTypeBuilder.addMakeInteractiveParameter(gb)

    # Set the parameters template
    nodeTypeBuilder.setParametersTemplateAttr(gb.build())

    # Set parameter hints
    nodeTypeBuilder.setHintsForParameter(
        'name',
        {'widget':'newScenegraphLocation', 'label':'Name'})

    help = (
        "<p>The Yeti fur cache file.</p><p>Use the same value as for the "
        "<i>Input Cache File Name</i> attribute of a Yeti shape in Maya, or the"
        " file argument of the Yeti Maya command:"
        "<pre>pgYetiCommand -writeCache filename_arg</pre> The frame"
        " number token uses C-style formatting, e.g. <b>%04d</b> for a 4-digit "
        "padded frame number.</p><b>Absolute paths should be used.</b>"
        )

    nodeTypeBuilder.setHintsForParameter(
        'file', 
        {
            'widget':'assetIdInput', 
            'fileTypes':'fur',
            'help': help,
            'label':'Yeti Cache File'
        })

    nodeTypeBuilder.setHintsForParameter( 'density', { 'label':'Density' } )
    nodeTypeBuilder.setHintsForParameter( 'width', { 'label':'Width' } )
    nodeTypeBuilder.setHintsForParameter( 'length', { 'label':'Length' } )

    nodeTypeBuilder.setHintsForParameter(
        'imagesearchpaths', 
        {
            'widget':'sortableDelimitedString',
            'isDynamicArray':'true',
            'delimiter':';',
            'label':'Image Search Path'
        })

    nodeTypeBuilder.setHintsForParameter(
        'verbosity',
        {
            'widget':'mapper',
            'options':'None:0|Some:1|All:2',
            'label':'Verbosity'
        })

    # Set the callback responsible to build the Ops chain
    nodeTypeBuilder.setBuildOpChainFnc(buildDlYetiOpChain)

    # Set the callback that allow gives us a chance to alter param values upon 
    # node creation. We use this to define a name param value based on the node
    # name.
    #
    nodeTypeBuilder.setBuildParametersFnc(buildParameters)

    # Set the callback to return scene graph location path
    nodeTypeBuilder.setGetScenegraphLocationFnc(returnScenegraphLocation)

    # Build the new node type
    nodeTypeBuilder.build()

def registerDlYetiUI():
    """
    Registers UI for the DlYeti node type.
    """
    try:
        from Katana import UI4
    except ImportError:
        return

    # Register a policy delegate to give all parameter widgets mini state badges
    parameterPolicyDelegate = UI4.FormMaster.EnableableScalarPolicyDelegate()

    UI4.FormMaster.ParameterPolicy.RegisterPolicyDelegate(
        'DlYeti',
        parameterPolicyDelegate)

# Register the node
registerDlYeti()

# UI stuff if available
if Configuration.get('KATANA_UI_MODE'):
    registerDlYetiUI()
