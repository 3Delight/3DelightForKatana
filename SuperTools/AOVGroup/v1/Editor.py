# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2020                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

from Katana import UI4, QT4FormWidgets

import KatanaInfo
katanaVersion = int(KatanaInfo.version.split('.')[0])
if katanaVersion >= 8:
    from PySide6 import QtCore, QtWidgets
else:
    from Katana import QtCore
    try:
        from PyQt5 import QtWidgets
    except ImportError:
        # Qt4 fallback
        from PyQt4 import QtGui as QtWidgets

from Katana import NodegraphAPI, Utils

# Grab style from DelightSettings
from ...DelightSettings.v1 import QDLStyle
# Also get this to avoid duplicating user AOV lookup code.
from ...DelightSettings.v1 import ScriptActions as SettingsSA

from . import ScriptActions as SA

_3DELIGHT_RENDERER_NAME = "dl"

# Suported types of AOVs are: color, float
# Each should have a dlAOVType shader to implement it
# and a DlAOVType node to specify it. This SuperTool basically:
# - Lists the available AOVs (all the DlAOVType nodes).
# - Adds/removes the corresponding shaders when they are selected/deselected.
# - Stitches the interfaces of all those shaders together onto a single node.

class AOVGroupEditor(QtWidgets.QWidget):
    def __init__(self, parent, node):
        super(AOVGroupEditor, self).__init__(parent)

        # Note that 'node' is incomplete here if it was saved as the currently
        # edited node. It will not have any of the AOVGroupNode methods.
        self.__node = node

        # Add layout
        QtWidgets.QVBoxLayout(self)

        # Set style before creating UI or some things won't refresh well.
        self.setStyleSheet(QDLStyle.GetBaseStyle())

        # Add various pieces of UI
        self.__initAOVSelectorUI()
        self.__initAOVShadersUI()

        # Start in frozen state, not handling events.
        self.__frozen = True

        # Used internally to disable handling of some events.
        self.__enableNodeGraphCB = True

    def showEvent(self, event):
        """
        Overrides the standard C{QWidget} event handler for "show" events.
        """
        QtWidgets.QWidget.showEvent(self, event)
        self.__thaw()

    def hideEvent(self, event):
        """
        Overrides the standard C{QWidget} event handler for "hide" events.
        """
        QtWidgets.QWidget.hideEvent(self, event)
        self.__freeze()

    def __thaw(self):
        if not self.__frozen:
            return
        self.__frozen = False
        
        Utils.EventModule.RegisterCollapsedHandler(
            self.NodeGraphChangedEH, 'nodegraph_changed')
        # We weren't getting the events while frozen so refresh the list.
        self.__updateAOVSelector()

    def __freeze(self):
        if self.__frozen:
            return
        self.__frozen = True
        
        Utils.EventModule.UnregisterCollapsedHandler(
            self.NodeGraphChangedEH, 'nodegraph_changed')

    def NodeGraphChangedEH(self, args):
        if not self.__enableNodeGraphCB:
            return
        self.__updateAOVSelector()

    def __initAOVList(self, aovLayout):
        """
        Create the list widget to select AOVs.
        """
        aovWidget = QtWidgets.QWidget()
        aov_layout = QtWidgets.QVBoxLayout()
        aovWidget.setLayout(aov_layout)

        aov_label = QtWidgets.QLabel('AOVs')
        aov_label.setAlignment(QtCore.Qt.AlignHCenter)
        aov_layout.addWidget(aov_label)
        aov_list = QtWidgets.QListWidget()
        aov_list.setMinimumHeight(100)
        aov_list.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        aov_list.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
        aov_list.itemSelectionChanged.connect(self.__onListSelectionChanged)
        aov_layout.addWidget(aov_list)
        self._aov_list = aov_list

        aovLayout.addWidget(aovWidget)

    def __updateAOVSelector(self):
        """
        Update the list of available AOVs in the widget to match what is
        defined by the scene.
        """
        # Disable the selection changed callback while we populate the list.
        self.__enableAOVSelectionCB = False

        networkNodes = dict(SA.GetShadingNodes(self.__node))
        # Find AOVs which should be in the list.
        self._aovShaderMap = {}
        availableAOVs = set()
        for comp in SettingsSA.GetUserComponents():
            compType = comp[3]
            compName = comp[1]
            if compType == 'color':
                availableAOVs.add(compName)
                self._aovShaderMap[compName] = 'dlAOVColor'
            if compType == 'scalar':
                availableAOVs.add(compName)
                self._aovShaderMap[compName] = 'dlAOVFloat'
        # Find AOVs actually in the list.
        lw = self._aov_list
        presentedAOVs = set([lw.item(i).text() for i in range(lw.count())])
        # Add missing ones.
        for aovName in availableAOVs.difference(presentedAOVs):
            item = QtWidgets.QListWidgetItem(aovName)
            lw.addItem(item)
            # Make selection match current network (for initial fill).
            if aovName in networkNodes:
                item.setSelected(True)
        # Remove extra ones.
        for aovName in presentedAOVs.difference(availableAOVs):
            for i in range(lw.count()):
                if lw.item(i).text() == aovName:
                    lw.takeItem(i)
                    break

        self.__enableAOVSelectionCB = True
        self.__updateListSelectionFromUI()

    def __initAOVSelectorUI(self):
        self.__initAOVList(self.layout())
        self.__updateAOVSelector() # also on nodegraph events

    def __initAOVShadersUI(self):
        for aovName, aovShader in SA.GetShadingNodes(self.__node):
            self.__addOneShaderUI(aovName, aovShader)

    def __addOneShaderUI(self, aovName, shaderNode):
        """
        Add a UI block for one AOV shader node. These simply follow the
        selection list.
        """
        if not shaderNode:
            return
        widget_factory = UI4.FormMaster.KatanaFactory.ParameterWidgetFactory

        # Generate the whole shader parameter UI. Sadly, I could not find a way
        # to grab only the page of parameters we want and still get the proper
        # controls generated automatically from metadata.
        #
        # Note that putting this in the UI is required to finish building the
        # shader parameters correctly. Otherwise, they remain under an __unused
        # group and the aov_name we set manually to an expression never
        # actually shows up in the network material. I could not manage to
        # *cough* find the documentation *cough* on how that happens to make it
        # happen directly without involving UI.
        param_policy = UI4.FormMaster.CreateParameterPolicy(None,
            shaderNode.getParameter('parameters'))
        param_widget = widget_factory.buildWidget(self, param_policy)
        # Rename the generic 'parameters' GroupFormWidget to our AOV name.
        param_widget.setLabelText(aovName)
        # Add the whole thing to this editor.
        self.layout().addWidget(param_widget)

    def __removeOneShaderUI(self, aovName):
        """
        Remove a UI block for one AOV shader node.
        """
        for w in self.children():
            if isinstance(w, QT4FormWidgets.GroupFormWidget):
                if w.getLabelText() == aovName:
                    w.deleteLater()

    def __onListSelectionChanged(self):
        if not self.__enableAOVSelectionCB:
            return

        old_selection = set(self.__aov_selection)
        self.__updateListSelectionFromUI()
        new_selection = set(self.__aov_selection)

        # This will mess with the node graph. Disable our callback while that
        # happens to avoid triggering a reparse of the internal network while
        # it is being altered.
        self.__enableNodeGraphCB = False
        for aovName in old_selection.difference(new_selection):
            self.__removeOneShaderUI(aovName)
            self.__node.removeAOV(aovName)
        for aovName in new_selection.difference(old_selection):
            shader = self.__node.addAOV(aovName, self._aovShaderMap[aovName])
            self.__addOneShaderUI(aovName, shader)
        self.__enableNodeGraphCB = True

    def __updateListSelectionFromUI(self):
        """
        Updates the internal list of currently selected AOVs in the UI.
        We keep track of this to be able to tell what changed.
        """
        self.__aov_selection = []
        for item in self._aov_list.selectedItems():
            self.__aov_selection.append(str(item.text()))

