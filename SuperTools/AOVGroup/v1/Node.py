# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2020                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

from Katana import Nodes3DAPI, NodegraphAPI

import KatanaInfo
katanaVersion = int(KatanaInfo.version.split('.')[0])
if katanaVersion >= 8:
    from PySide6 import QtWidgets
else:
    try:
        from PyQt5 import QtWidgets
    except ImportError:
        # Qt4 fallback
        from PyQt4 import QtGui as QtWidgets

import re
from . import ScriptActions as SA
import logging

_3DELIGHT_RENDERER_NAME = "dl"

log = logging.getLogger(_3DELIGHT_RENDERER_NAME.title() + "AOVGroup.Node")

class AOVGroupNode(NodegraphAPI.SuperTool):
    def __init__(self):
        super(AOVGroupNode, self).__init__()
        # Note that this init is not called when reading a file. polish() is
        # called but too late for the Editor if the node was saved as the
        # edited node.

        # Add the output port to connected to a shader's aovGroup
        p = self.addOutputPort('out')
        # Mark it to connect only to color ports.
        p.setTags(['color'])
        # Hide group UI.
        self.hideNodegraphGroupControls()
        # Hidden version parameter for eventual upgrade of networks.
        self.getParameters().createChildNumber('version', 1)

    def __connectShaderToNetwork(self, shaderNode):
        internalNodes = SA.GetShadingNodes(self) 
        if internalNodes:
            # Append to existing network
            shaderNode.getOutputPort('out').connect(
                internalNodes[-1][1].getInputPort('in'))
        else:
            # First node. Connect to group's output.
            shaderNode.getOutputPort('out').connect(
                self.getReturnPort('out'))
        # Add to internal list.
        self.__layoutNetwork()

    def __removeShaderFromNetwork(self, shaderNode):
        # Patch the connection over the node we're removing.
        from_ports = shaderNode.getInputPort('in').getConnectedPorts()
        to_ports = shaderNode.getOutputPort('out').getConnectedPorts()
        if from_ports and to_ports:
            from_ports[0].connect(to_ports[0])

    def __layoutNetwork(self):
        """
        Layout internal nodes so they're not all piled up. Useful to debug.
        """
        y = 0
        for name, node in SA.GetShadingNodes(self):
            NodegraphAPI.SetNodePosition(node, (0, y))
            y += 40

    def addAOV(self, aovName, shaderName):
        """
        Add (as in enable the use of) an AOV to this node.
        If there is a node in the scene with the name of the AOV, it is linked
        with an expression so the name will update dynamically.
        """
        # Create the shader for the internal network.
        shader = NodegraphAPI.CreateNode('DlShadingNode', self)
        shader.setName('AOVGroupInternalShader')
        shader.getParameter('nodeType').setValue(shaderName, 0)
        shader.checkDynamicParameters()

        # Connect to the internal chain.
        self.__connectShaderToNetwork(shader)

        # Link the 'aov_name' parameter of the shader to the AOV def node.
        # Or just set it if there is no such node.
        shader.getParameter('parameters.aov_name.enable').setValue(1, 0)
        nameParam = shader.getParameter('parameters.aov_name.value')
        if NodegraphAPI.GetNode(aovName):
            nameParam.setExpression("getNode('" + aovName + "').getNodeName()")
        else:
            nameParam.setValue(aovName, 0)

        # Export the shader's input ports as ports of the group.
        for shaderPort in shader.getInputPorts():
            label = shaderPort.getMetadata('label')
            # Don't export parameters without a label.
            if not label:
                continue
            # Try to give the port a decent name. Normally not user visible.
            groupPortName = aovName + '_' + shaderPort.getName()
            inPort = self.addInputPort(groupPortName)
            # Connect to the shader's port.
            self.getSendPort(groupPortName).connect(shaderPort)
            # Forward tags which define allowed connection types.
            inPort.setTags(shaderPort.getTags())
            # Forward the label to the port.
            inPort.addOrUpdateMetadata('label', label)
            # And the port color.
            if hasattr(inPort, 'setColor'):
                inPort.setColor(*shaderPort.getColor())
            # Also the page. Prepend the AOV name to it.
            # This part will unfortunately not update itself later.
            page = shaderPort.getMetadata('page')
            page = aovName + ('.' + page if page else '')
            inPort.addOrUpdateMetadata('page', page)
        return shader

    def removeAOV(self, aovName):
        """
        Remove an AOV from this node.
        """
        networkNodes = dict(SA.GetShadingNodes(self))
        shader = networkNodes.get(aovName, None)
        if not shader:
            return
        self.__removeShaderFromNetwork(shader)
        shader.delete()
        # Remove the group's input ports without an internal connection.
        for p in self.getInputPorts():
            if not self.getSendPort(p.getName()).getNumConnectedPorts():
                self.removeInputPort(p.getName())

# Make this available in network material groups.
if getattr(NodegraphAPI, 'Context', None):
    NodegraphAPI.AddNodeFlavor(
        _3DELIGHT_RENDERER_NAME.title() + "AOVGroup",
        NodegraphAPI.Context.NetworkMaterial)
    NodegraphAPI.AddNodeFlavor(
        _3DELIGHT_RENDERER_NAME.title() + "AOVGroup", _3DELIGHT_RENDERER_NAME)
