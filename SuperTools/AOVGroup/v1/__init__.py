# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2020                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

from .Node import AOVGroupNode

def GetEditor():
    from .Editor import AOVGroupEditor
    return AOVGroupEditor
