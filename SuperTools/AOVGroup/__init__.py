# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2020                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

import Katana
from . import v1 as AOVGroup

_3DELIGHT_RENDERER_NAME = "dl"

if AOVGroup:
    PluginRegistry = [ (
        "SuperTool",
        2,
        _3DELIGHT_RENDERER_NAME.title() + "AOVGroup",
        (AOVGroup.AOVGroupNode, AOVGroup.GetEditor) ) ]
