# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2017                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

# Copyright (c) 2015 The Foundry Visionmongers Ltd. All Rights Reserved.

from Katana import NodegraphAPI, Decorators, Plugins
import PackageSuperToolAPI.NodeUtils as NU
from PackageSuperToolAPI import Packages

import os
import textwrap
import logging
import math

# Our module with common light package code.
from . import LightBase;

log = logging.getLogger("GafferThree.MeshLightPackage")

_iconsDir = os.path.join(os.path.dirname(__file__), 'icons')

# Get base classes for our packages from the registered GafferThree packages
GafferThreeAPI = Plugins.GafferThreeAPI
LightPackage = GafferThreeAPI.PackageClasses.LightPackage
LightEditPackage = GafferThreeAPI.PackageClasses.LightEditPackage


class MeshLightBasePackage(LightBase.BasePackage):
    """
    Common code for MeshLightPackage and MeshLightEditPackage.
    """

    @classmethod
    def addLightAttributesNode(cls, packageNode, editing):
        node = NodegraphAPI.CreateNode('DlMeshLightAttributes', packageNode)
        cls.setGenericAssignCEL(node)
        if not editing:
            # Always set value locally.
            node.getParameter(
                'args.geometry.sourceMesh.enable').setValue(1, 0)
        NU.AddNodeRef(packageNode, 'lightAttrNode', node)
        cls.appendNode(packageNode, editing, node)

class MeshLightPackage(LightPackage,MeshLightBasePackage):
    """
	Implements a Mesh Light package. We inherit LightPackage, and override
	some functions to modify behaviour.
    """

    # Class Variables ---------------------------------------------------------

    # The name of the package type as it should be shown in the UI
    DISPLAY_NAME = 'Mesh Light'

    # The default name of a package when it is created. This also defines the
    # default name of the package's scene graph location
    DEFAULT_NAME = 'meshLight'

    # Class Functions ---------------------------------------------------------

    @classmethod
    def create(cls, enclosingNode, locationPath):
        """
        A factory method which returns an instance of the class.

        @type enclosingNode: C{NodegraphAPI.Node}
        @type locationPath: C{str}
        @rtype: L{LightPackage}
        @param enclosingNode: The parent node within which the new
            package's node should be created.
        @param locationPath: The path to the location to be created/managed
            by the package.
        @return: The newly-created package instance.
        """
        # Create the package node
        packageNode = cls.createPackageNode(enclosingNode, locationPath)
        # Add nodes to our package group.
        cls.addMasterMaterialPortNode(packageNode)
        # Create the location.
        createNode = cls.addCreateNode(packageNode)
        # Add some boilerplate.
        cls.addPostCreateStandardNodes(packageNode)
        # Add light Material.
        cls.addMaterialNodes(packageNode, False, 'Light', 'areaLight')
        # Add DlObjectSettings node.
        cls.addObjectSettingsNode(packageNode, False)
        # Add MeshLight attributes node.
        cls.addLightAttributesNode(packageNode, False)
        # Add other standard stuff.
        cls.addPruneOpScriptNode(packageNode)
        cls.addChildPackageMergeNode(packageNode)
        cls.addLightLinkingNodes(packageNode, False)

        # Create a package node instance
        result = cls(packageNode)
        Packages.CallbackMixin.executeCreationCallback(result)

        # Create a post-merge stack node for this package
        postMergeNode = result.createPostMergeStackNode()

        cls.addMeshCopyNode(postMergeNode)
        cls.addMeshHierarchyCleanupNode(postMergeNode)

        return result

    @classmethod
    def addMeshCopyNode(cls, postMergeNode):
        """
            Adds a script to the post merge stack to copy the mesh geometry
            from the desired source. The script also tweaks viewer attributes
            so the mesh is drawn by Katana's viewer like the other lights. This
            could be done earlier with ViewerObjectSettings and AttributeSet
            but there's little point as there is no mesh before then.
        """
        node = NodegraphAPI.CreateNode('OpScript', postMergeNode)
        node.setName('MeshLightGeometryCopy')
        # The post merge stack has a reference to the package node (the group
        # which creates the location and sets everything on it) but no direct
        # reference to the created location name. So I had to use some python
        # expression script to find my way to it.
        node.getParameter('CEL').setExpression(
            'getNode( str( self.getParent().node_package ) ).' +
            NU.GetPackageLocationParameterPath() )

        script = textwrap.dedent("""\
            --- Find source mesh location.
            local source_attr = Interface.GetAttr( 'geometry.sourceMesh' )
            if source_attr == nil then
                return
            end
            local source_loc = source_attr:getValue('', false)
            -- Remove existing geometry.
            Interface.DeleteAttr( 'geometry' )
            -- Nothing we can do without a source.
            if source_loc == '' then
                return
            end
            local source_type = Interface.GetInputLocationType( source_loc )
            -- For polymesh and subdmesh, replace the whole geometry group by
            -- the source mesh's. It will render as polymesh.
            if source_type == 'polymesh' or source_type == 'subdmesh' then
                Interface.CopyAttr( 'geometry', 'geometry', false, source_loc )
                Interface.SetAttr( 'viewer.locationType', StringAttribute( 'polymesh' ) )
            end
            -- For a group, copy its children. Another script will clean them up.
            if source_type == 'group' or source_type == 'assembly' or
               source_type == 'component' then
                Interface.ReplaceChildren( source_loc )
            end
            -- Tweak viewer attributes so the mesh looks like other lights.
            Interface.SetAttr( 'viewer.default.drawOptions.fill', StringAttribute( 'wireframe' ) )
            Interface.SetAttr( 'viewer.default.drawOptions.color', FloatAttribute( {1,1,0}, 3 ) )
            """)
        node.getParameter('script.lua').setValue(script, 0)
        postMergeNode.buildChildNode(adoptNode=node)

    @classmethod
    def addMeshHierarchyCleanupNode(cls, postMergeNode):
        """
            Adds a script to the post merge stack to clean up the children of a
            group used as a mesh light.
        """
        node = NodegraphAPI.CreateNode('OpScript', postMergeNode)
        node.setName('MeshLightHierarchyCleanup')
        # The post merge stack has a reference to the package node (the group
        # which creates the location and sets everything on it) but no direct
        # reference to the created location name. So I had to use some python
        # expression script to find my way to it.
        node.getParameter('CEL').setExpression(
            'getNode( str( self.getParent().node_package ) ).' +
            NU.GetPackageLocationParameterPath() +
            " + '//*'" )

        script = textwrap.dedent("""\
            local loc_type = Interface.GetInputLocationType()
            if loc_type == 'subdmesh' then
                -- Turn subdivs into polygon meshes for sampling.
                Interface.SetAttr( 'type', StringAttribute( 'polymesh' ) )
            elseif loc_type ~= 'polymesh' and loc_type ~= 'group' and
                   loc_type ~= 'assembly' and loc_type ~= 'component'  then
                -- Only allow group and polymesh in here for now.
                Interface.DeleteSelf()
                return
            end

            -- Prevent manipulators from affecting source geometry.
            Interface.DeleteAttr( 'attributeEditor' )
            -- Make sure the light material is not replaced by one from the source.
            Interface.DeleteAttr( 'material' )
            Interface.DeleteAttr( 'materialAssign' )
        """)
        node.getParameter('script.lua').setValue(script, 0)
        postMergeNode.buildChildNode(adoptNode=node)


class MeshLightEditPackage(LightEditPackage,MeshLightBasePackage):
    """
    The edit package that allows a GafferThree to edit an existing MeshLight
    package present in the input Scenegraph.

    This package uses a TransformEdit node to edit the MeshLight's transform.
    """

    # Class Variables ---------------------------------------------------------


    # Class Functions ---------------------------------------------------------

    @classmethod
    def create(cls, enclosingNode, locationPath):
        """
        Creates the contents of the EditStackNode that contains the edit nodes.
        This could be any other kind of node with at least one input and one
        output, but the createPackageEditStackNode() helper function does all
        of the configuration boilerplate code of an EditStackNode for you.
        The return value is a MeshLightEditPackage instance.

        This particular package node will contain a TransformEdit node on it,
        which will allow to edit the transform of a skyDome.
        """
        # Create the package node. Since this is an edit package we want to use
        # an EditStackNode instead of a GroupNode, since it already has an
        # input and an output by default. This also adds some necessary
        # parameters to this node.
        packageNode = cls.createPackageEditStackNode(enclosingNode,
                                                     locationPath)

        # Add the various nodes needed to edit the light.
        cls.addMaterialNodes(packageNode, True, 'Light', 'areaLight')
        cls.addTransformEditNode(packageNode)
        cls.addObjectSettingsNode(packageNode, True)
        # Left out because it makes no sense to change the mesh.
        #cls.addLightAttributesNode(packageNode, True)
        cls.addLightLinkingNodes(packageNode, True)

        # Instantiate a package with the package node
        return cls.createPackage(packageNode)

    @classmethod
    def getAdoptableLocationTypes(cls):
        """
        Returns the set of location types adoptable by this Package. In this
        case, the package can edit locations created by MeshLightPackages, which
        are of the type light.
        """
        return set(('light',))

    # Instance Functions ------------------------------------------------------

    @Decorators.undogroup('Delete MeshLightEdit Package')
    def delete(self):
        LightEditPackage.delete(self)


# Register the package classes, and associate the edit package class with the
# create package class
GafferThreeAPI.RegisterPackageClass(MeshLightPackage)
GafferThreeAPI.RegisterPackageClass(MeshLightEditPackage)
MeshLightPackage.setEditPackageClass(MeshLightEditPackage)
