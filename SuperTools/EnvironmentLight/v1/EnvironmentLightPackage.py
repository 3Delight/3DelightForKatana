# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2016                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

# Copyright (c) 2015 The Foundry Visionmongers Ltd. All Rights Reserved.

from Katana import Decorators, Plugins

import os
import textwrap
import logging

# Our module with common light package code.
from . import EnvironmentLightBase;

log = logging.getLogger("GafferThree.EnvironmentLightPackage")

_iconsDir = os.path.join(os.path.dirname(__file__), 'icons')

# Get base classes for our packages from the registered GafferThree packages
GafferThreeAPI = Plugins.GafferThreeAPI

class EnvironmentLightPackage(EnvironmentLightBase.EnvironmentLightPackage):
    """
    Implements a Environment Light package. We inherit EnvironmentLightPackage, and 
    override some class variables.
    """

    # Class Variables ---------------------------------------------------------

    # The name of the package type as it should be shown in the UI
    DISPLAY_NAME = 'Environment Light'

    # The default name of a package when it is created. This also defines the
    # default name of the package's scene graph location
    DEFAULT_NAME = 'envLight'

    # The icon to use to represent this package type in the UI
    DISPLAY_ICON = os.path.join(_iconsDir, 'out_delightEnvironment.png')

    SHADER_NAME = 'environmentLight'


class EnvironmentLightEditPackage(
    EnvironmentLightBase.EnvironmentLightEditPackage):
    """
    The edit package that allows a GafferThree to edit an existing EnvironmentLight
    package present in the input Scenegraph.

    This package uses a TransformEdit node to edit the EnvironmentLight's transform.
    """

    # Class Variables ---------------------------------------------------------

    DISPLAY_ICON = os.path.join(_iconsDir, 'out_delightEnvironment.png')

    SHADER_NAME = 'environmentLight'

    # Instance Functions ------------------------------------------------------

    @Decorators.undogroup('Delete EnvironmentLightEdit Package')
    def delete(self):
        LightEditPackage.delete(self)


# Register the package classes, and associate the edit package class with the
# create package class
GafferThreeAPI.RegisterPackageClass(EnvironmentLightPackage)
GafferThreeAPI.RegisterPackageClass(EnvironmentLightEditPackage)
EnvironmentLightPackage.setEditPackageClass(EnvironmentLightEditPackage)
