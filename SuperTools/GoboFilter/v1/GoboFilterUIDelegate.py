# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2017                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

from PackageSuperToolAPI import UIDelegate
from PackageSuperToolAPI import NodeUtils as NU
from PackageSuperToolAPI import Packages
from .GoboFilterPackage import (GoboFilterPackage, GoboFilterEditPackage)
from Katana import QT4FormWidgets, FormMaster, Plugins, UI4

# Our module with common light filter package code.
from . import LightFilterBase;

# Get the base classes for our UI delegate classes from the PackageSuperToolAPI
# using the base classes of our custom Package classes
GafferThreeAPI = Plugins.GafferThreeAPI
LightFilterUIDelegate = UIDelegate.GetUIDelegateClassForPackageClass(
    GafferThreeAPI.PackageClasses.LightFilterPackage)
LightFilterEditUIDelegate = UIDelegate.GetUIDelegateClassForPackageClass(
    GafferThreeAPI.PackageClasses.LightFilterEditPackage)

class GoboFilterCommonUI(LightFilterBase.BaseUIDelegate):
    """
    The common base class of the UIDelegate and the EditUIDelegate.

    This is used for code shared by both classes.
    """

class GoboFilterUIDelegate(LightFilterUIDelegate,GoboFilterCommonUI):
    """
    The UI delegate for the GoboFilter package.

    This class is responsible for exposing the parameters on each of the
    parameter tabs. This is done by creating parameter policies attached to the
    parameters on the package's nodes. We can also modify the appearance of the
    parameter tabs by modifying the hints dictionaries on the policies.
    """

    # The hash used to uniquely identify the action of creating a package
    # This was generated using:
    #     hashlib.md5('GoboFilter.AddGoboFilter').hexdigest()
    AddPackageActionHash = 'c7ef8fd7b061d8395649d6f567348483'

    # The keyboard shortcut for creating a package
    DefaultShortcut = 'B'


class GoboFilterEditUIDelegate(LightFilterEditUIDelegate,GoboFilterCommonUI):
    """
    The UI delegate for the GoboFilterEdit package.
    """


# Register the UI delegates

UIDelegate.RegisterUIDelegateClass(GoboFilterPackage, GoboFilterUIDelegate)
UIDelegate.RegisterUIDelegateClass(GoboFilterEditPackage, GoboFilterEditUIDelegate)

