# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2017                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

# Copyright (c) 2015 The Foundry Visionmongers Ltd. All Rights Reserved.

from Katana import NodegraphAPI, Decorators, Plugins
import PackageSuperToolAPI.NodeUtils as NU
from PackageSuperToolAPI import Packages

import os
import textwrap
import logging
import math

# Our module with common light package code.
from . import LightBase;

log = logging.getLogger("GafferThree.PointLightPackage")

_iconsDir = os.path.join(os.path.dirname(__file__), 'icons')

# Get base classes for our packages from the registered GafferThree packages
GafferThreeAPI = Plugins.GafferThreeAPI
LightPackage = GafferThreeAPI.PackageClasses.LightPackage
LightEditPackage = GafferThreeAPI.PackageClasses.LightEditPackage


class PointLightBasePackage(LightBase.BasePackage):
    """
    Common code for PointLightPackage and PointLightEditPackage.
    """

    @classmethod
    def addLightAttributesNode(cls, packageNode, editing):
        node = NodegraphAPI.CreateNode('DlPointLightAttributes', packageNode)
        cls.setGenericAssignCEL(node)
        if not editing:
            # Always set value locally.
            node.getParameter(
                'args.geometry.pointRadius.enable').setValue(1, 0)
        NU.AddNodeRef(packageNode, 'lightAttrNode', node)
        cls.appendNode(packageNode, editing, node)

class PointLightPackage(LightPackage,PointLightBasePackage):
    """
	Implements a Point Light package. We inherit LightPackage, and override
	some functions to modify behaviour.
    """

    # Class Variables ---------------------------------------------------------

    # The name of the package type as it should be shown in the UI
    DISPLAY_NAME = 'Point Light'

    # The default name of a package when it is created. This also defines the
    # default name of the package's scene graph location
    DEFAULT_NAME = 'pointLight'

    # Class Functions ---------------------------------------------------------

    @classmethod
    def create(cls, enclosingNode, locationPath):
        """
        A factory method which returns an instance of the class.

        @type enclosingNode: C{NodegraphAPI.Node}
        @type locationPath: C{str}
        @rtype: L{LightPackage}
        @param enclosingNode: The parent node within which the new
            package's node should be created.
        @param locationPath: The path to the location to be created/managed
            by the package.
        @return: The newly-created package instance.
        """
        # Create the package node
        packageNode = cls.createPackageNode(enclosingNode, locationPath)
        # Add nodes to our package group.
        cls.addMasterMaterialPortNode(packageNode)
        # Create the location.
        createNode = cls.addCreateNode(packageNode)
        # Add some boilerplate.
        cls.addPostCreateStandardNodes(packageNode)
        # Add light Material.
        cls.addMaterialNodes(packageNode, False, 'Light', 'pointLight')
        # Add DlObjectSettings node.
        cls.addObjectSettingsNode(packageNode, False)
        # Add PointLight attributes node.
        cls.addLightAttributesNode(packageNode, False)
        # Add other standard stuff.
        cls.addPruneOpScriptNode(packageNode)
        cls.addChildPackageMergeNode(packageNode)
        cls.addLightLinkingNodes(packageNode, False)

        # Create a package node instance
        result = cls(packageNode)
        Packages.CallbackMixin.executeCreationCallback(result)

        # Create a post-merge stack node for this package
        #postMergeNode = result.createPostMergeStackNode()

        return result


class PointLightEditPackage(LightEditPackage,PointLightBasePackage):
    """
    The edit package that allows a GafferThree to edit an existing PointLight
    package present in the input Scenegraph.

    This package uses a TransformEdit node to edit the PointLight's transform.
    """

    # Class Variables ---------------------------------------------------------


    # Class Functions ---------------------------------------------------------

    @classmethod
    def create(cls, enclosingNode, locationPath):
        """
        Creates the contents of the EditStackNode that contains the edit nodes.
        This could be any other kind of node with at least one input and one
        output, but the createPackageEditStackNode() helper function does all
        of the configuration boilerplate code of an EditStackNode for you.
        The return value is a PointLightEditPackage instance.

        This particular package node will contain a TransformEdit node on it,
        which will allow to edit the transform of a skyDome.
        """
        # Create the package node. Since this is an edit package we want to use
        # an EditStackNode instead of a GroupNode, since it already has an
        # input and an output by default. This also adds some necessary
        # parameters to this node.
        packageNode = cls.createPackageEditStackNode(enclosingNode,
                                                     locationPath)

        # Add the various nodes needed to edit the light.
        cls.addMaterialNodes(packageNode, True, 'Light', 'pointLight')
        cls.addTransformEditNode(packageNode)
        cls.addObjectSettingsNode(packageNode, True)
        cls.addLightAttributesNode(packageNode, True)
        cls.addLightLinkingNodes(packageNode, True)

        # Instantiate a package with the package node
        return cls.createPackage(packageNode)

    @classmethod
    def getAdoptableLocationTypes(cls):
        """
        Returns the set of location types adoptable by this Package. In this
        case, the package can edit locations created by PointLightPackages, 
        which are of the type light.
        """
        return set(('light',))

    # Instance Functions ------------------------------------------------------

    @Decorators.undogroup('Delete PointLightEdit Package')
    def delete(self):
        LightEditPackage.delete(self)


# Register the package classes, and associate the edit package class with the
# create package class
GafferThreeAPI.RegisterPackageClass(PointLightPackage)
GafferThreeAPI.RegisterPackageClass(PointLightEditPackage)
PointLightPackage.setEditPackageClass(PointLightEditPackage)
