# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2016                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

# Copyright (c) 2015 The Foundry Visionmongers Ltd. All Rights Reserved.

from Katana import NodegraphAPI, Decorators, Plugins
import PackageSuperToolAPI.NodeUtils as NU
from PackageSuperToolAPI import Packages

import os
import textwrap
import logging

# Our module with common light package code.
from . import LightBase;

log = logging.getLogger("GafferThree.AreaLightPackage")

_iconsDir = os.path.join(os.path.dirname(__file__), 'icons')

# Get base classes for our packages from the registered GafferThree packages
GafferThreeAPI = Plugins.GafferThreeAPI
LightPackage = GafferThreeAPI.PackageClasses.LightPackage
LightEditPackage = GafferThreeAPI.PackageClasses.LightEditPackage


class AreaLightBasePackage(LightBase.BasePackage):
    """
    Common code for AreaLightPackage and AreaLightEditPackage.
    """

    @classmethod
    def addLightAttributesNode(cls, packageNode, editing):
        node = NodegraphAPI.CreateNode('DlAreaLightAttributes', packageNode)
        cls.setGenericAssignCEL(node)
        if not editing:
            # Always set value locally.
            node.getParameter(
                'args.geometry.shape.enable').setValue(1, 0)
        NU.AddNodeRef(packageNode, 'lightAttrNode', node)
        cls.appendNode(packageNode, editing, node)

class AreaLightPackage(LightPackage,AreaLightBasePackage):
    """
    Implements an area light package. We inherit LightPackage, and override
    some functions to modify behaviour.
    """

    # Class Variables ---------------------------------------------------------

    # The name of the package type as it should be shown in the UI
    DISPLAY_NAME = 'Area Light'

    # The default name of a package when it is created. This also defines the
    # default name of the package's scene graph location
    DEFAULT_NAME = 'areaLight'

    # The icon to use to represent this package type in the UI
    DISPLAY_ICON = os.path.join(_iconsDir, 'out_delightClippingPlane.png')

    # The default size of the sky dome
    DEFAULT_SIZE = 1

    # Class Functions ---------------------------------------------------------

    @classmethod
    def create(cls, enclosingNode, locationPath):
        """
        A factory method which returns an instance of the class.

        @type enclosingNode: C{NodegraphAPI.Node}
        @type locationPath: C{str}
        @rtype: L{LightPackage}
        @param enclosingNode: The parent node within which the new
            package's node should be created.
        @param locationPath: The path to the location to be created/managed
            by the package.
        @return: The newly-created package instance.
        """
        locExpr = cls.getLocationExpression()
        # Create the package node
        packageNode = cls.createPackageNode(enclosingNode, locationPath)
        # Add nodes to our package group.
        cls.addMasterMaterialPortNode(packageNode)

        # Create geometry for the light - a polygon
        createNode = cls.addCreateNode(packageNode)

        createNode.getParameter('transform.scale.x').setValue(
            cls.DEFAULT_SIZE, 0)
        createNode.getParameter('transform.scale.y').setValue(
            cls.DEFAULT_SIZE, 0)
        createNode.getParameter('transform.scale.z').setValue(
            cls.DEFAULT_SIZE, 0)
        createNode.getParameters().createChildNumber('forceAsStaticScene', 1)

        # Add some boilerplate.
        cls.addPostCreateStandardNodes(packageNode)

        # Change the geometry
        geoAttrSetNode = NodegraphAPI.CreateNode('AttributeSet', packageNode)
        geoAttrSetNode.setName("SetGeoAttributeSet")
        geoAttrSetNode.getParameter('paths.i0').setExpression(locExpr)
        geoAttrSetNode.getParameter('attributeName').setValue(
                'geometry.point.P', 0)
        geoAttrSetNode.getParameter('attributeType').setValue('float', 0)
        p = geoAttrSetNode.getParameter('numberValue')
        p.setTupleSize(3)
        p.resizeArray(12)
        p.getChild('i0').setValue(-0.5, 0)
        p.getChild('i1').setValue( 0.5, 0)
        p.getChild('i2').setValue( 0.0, 0)
        p.getChild('i3').setValue( 0.5, 0)
        p.getChild('i4').setValue( 0.5, 0)
        p.getChild('i5').setValue( 0.0, 0)
        p.getChild('i6').setValue(-0.5, 0)
        p.getChild('i7').setValue(-0.5, 0)
        p.getChild('i8').setValue( 0.0, 0)
        p.getChild('i9').setValue( 0.5, 0)
        p.getChild('i10').setValue(-0.5, 0)
        p.getChild('i11').setValue( 0.0, 0)
        cls.appendNode(packageNode, False, geoAttrSetNode)

        # Add light Material.
        cls.addMaterialNodes(packageNode, False, 'Light', 'areaLight')
        # Add DlObjectSettings node.
        cls.addObjectSettingsNode(packageNode, False)
        # Add SpotLight attributes node.
        cls.addLightAttributesNode(packageNode, False)

        # Add other standard stuff.
        cls.addPruneOpScriptNode(packageNode)
        cls.addChildPackageMergeNode(packageNode)
        cls.addLightLinkingNodes(packageNode, False)

        # Create a package node instance
        result = cls(packageNode)
        Packages.CallbackMixin.executeCreationCallback(result)

        # Create a post-merge stack node for this package
        #postMergeNode = result.createPostMergeStackNode()

        return result


class AreaLightEditPackage(LightEditPackage,AreaLightBasePackage):
    """
    The edit package that allows a GafferThree to edit an existing AreaLight
    package present in the input Scenegraph.

    This package uses a TransformEdit node to edit the AreaLight's transform.
    """

    # Class Variables ---------------------------------------------------------

    DISPLAY_ICON = os.path.join(_iconsDir, 'out_delightClippingPlane.png')

    # Class Functions ---------------------------------------------------------

    @classmethod
    def create(cls, enclosingNode, locationPath):
        """
        Creates the contents of the EditStackNode that contains the edit nodes.
        This could be any other kind of node with at least one input and one
        output, but the createPackageEditStackNode() helper function does all
        of the configuration boilerplate code of an EditStackNode for you.
        The return value is a AreaLightEditPackage instance.

        This particular package node will contain a TransformEdit node on it,
        which will allow to edit the transform of a skyDome.
        """
        # Create the package node. Since this is an edit package we want to use
        # an EditStackNode instead of a GroupNode, since it already has an
        # input and an output by default. This also adds some necessary
        # parameters to this node.
        packageNode = cls.createPackageEditStackNode(enclosingNode,
                                                     locationPath)

        # Add the various nodes needed to edit the light.
        cls.addMaterialNodes(packageNode, True, 'Light', 'areaLight')
        cls.addTransformEditNode(packageNode)
        cls.addObjectSettingsNode(packageNode, True)
        cls.addLightAttributesNode(packageNode, True)
        cls.addLightLinkingNodes(packageNode, True)

        # Instantiate a package with the package node
        return cls.createPackage(packageNode)

    @classmethod
    def getAdoptableLocationTypes(cls):
        """
        Returns the set of location types adoptable by this Package. In this
        case, the package can edit locations created by AreaLightPackages, which
        are of the type light.
        """
        return set(('light',))

    # Instance Functions ------------------------------------------------------

    @Decorators.undogroup('Delete AreaLightEdit Package')
    def delete(self):
        LightEditPackage.delete(self)


# Register the package classes, and associate the edit package class with the
# create package class
GafferThreeAPI.RegisterPackageClass(AreaLightPackage)
GafferThreeAPI.RegisterPackageClass(AreaLightEditPackage)
AreaLightPackage.setEditPackageClass(AreaLightEditPackage)

