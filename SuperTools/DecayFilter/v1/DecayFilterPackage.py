# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2017                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

from Katana import NodegraphAPI, Decorators, Plugins
import PackageSuperToolAPI.NodeUtils as NU
from PackageSuperToolAPI import Packages

import os
import textwrap
import math

# Our module with common light filter package code.
from . import LightFilterBase;

_iconsDir = os.path.join(os.path.dirname(__file__), 'icons')

# Get base classes for our packages from the registered GafferThree packages
GafferThreeAPI = Plugins.GafferThreeAPI
LightFilterPackage = GafferThreeAPI.PackageClasses.LightFilterPackage
LightFilterEditPackage = GafferThreeAPI.PackageClasses.LightFilterEditPackage

def AddConnectionToEditor(packageNode):
    """
    Creates and appends AttributeSet in packageNode to connect attributeEditor
    to the material location
    """
    attrNode = NodegraphAPI.CreateNode('AttributeSet', packageNode)
    NU.WireInlineNodes( packageNode, packageNode.getChildren(), 0, 25 )
    param = attrNode.getParameter("paths").getChildByIndex(0)
    param.setExpression("=^/__gaffer.location")
    param = attrNode.getParameter("attributeName")
    param.setValue("attributeEditor.exclusiveTo", 0)
    param = attrNode.getParameter("attributeType")
    param.setValue("string", 0)
    num = packageNode.getNumChildren()
    matNode = packageNode.getChildByIndex(num-2)
    param = attrNode.getParameter("stringValue").getChildByIndex(0)
    param.setValue(matNode.getName(), 0)

class DecayFilterBasePackage(LightFilterBase.BasePackage):
    """
    Common code for DecayFilterPackage and DecayFilterEditPackage.
    """

class DecayFilterPackage(LightFilterPackage,DecayFilterBasePackage):
    """
    Implements a Decay Light Filter package. We inherit LightFilterPackage, and
    override some functions to modify behaviour.
    """

    # Class Variables ---------------------------------------------------------

    # The name of the package type as it should be shown in the UI
    DISPLAY_NAME = 'Decay Light Filter'

    # The default name of a package when it is created. This also defines the
    # default name of the package's scene graph location
    DEFAULT_NAME = 'decayFilter'

    # Class Functions ---------------------------------------------------------

    @classmethod
    def create(cls, enclosingNode, locationPath):
        """
        A factory method which returns an instance of the class.
        """
        # Create the package node.
        packageNode = cls.createPackageGroupNode(enclosingNode, locationPath)

        # Create the location.
        createNode = cls.addCreateNode(packageNode, 'rig')
        # Add some boilerplate.
        cls.addPostCreateStandardNodes(packageNode)
        # Add light Material.
        cls.addShaderNode(packageNode, False, 'LightFilter', 'decayFilter')

        # Connect material location to the editor (useful for view manipulator)
        AddConnectionToEditor(packageNode)

        # Create a package node instance
        result = cls(packageNode)

        return result


class DecayFilterEditPackage(LightFilterEditPackage,DecayFilterBasePackage):
    """
    The edit package that allows a GafferThree to edit an existing DecayFilter
    package present in the input Scenegraph.
    """

    # Class Variables ---------------------------------------------------------


    # Class Functions ---------------------------------------------------------

    @classmethod
    def create(cls, enclosingNode, locationPath):
        """
        Creates the contents of the EditStackNode that contains the edit nodes.
        This could be any other kind of node with at least one input and one
        output, but the createPackageEditStackNode() helper function does all
        of the configuration boilerplate code of an EditStackNode for you.
        The return value is a DistantLightEditPackage instance.
        """
        # Create the package node. Since this is an edit package we want to use
        # an EditStackNode instead of a GroupNode, since it already has an
        # input and an output by default. This also adds some necessary
        # parameters to this node.
        packageNode = cls.createPackageEditStackNode(enclosingNode,
                                                     locationPath)

        # Add the various nodes needed to edit the light filter.
        cls.addShaderNode(packageNode, True, 'LightFilter', 'decayFilter')
        cls.addTransformEditNode(packageNode)

        # Connect material location to the editor (useful for view manipulator)
        AddConnectionToEditor(packageNode)

        # Instantiate a package with the package node
        return cls.createPackage(packageNode)

    @classmethod
    def getAdoptableLocationTypes(cls):
        """
        Returns the set of location types adoptable by this Package. In this
        case, the package can edit locations created by GoboFilterPackage, which
        are of the type light filter.
        """
        return set(('light filter',))

    # Instance Functions ------------------------------------------------------

    @Decorators.undogroup('Delete DecayFilterEdit Package')
    def delete(self):
        LightFilterEditPackage.delete(self)


# Register the package classes, and associate the edit package class with the
# create package class
GafferThreeAPI.RegisterPackageClass(DecayFilterPackage)
GafferThreeAPI.RegisterPackageClass(DecayFilterEditPackage)
DecayFilterPackage.setEditPackageClass(DecayFilterEditPackage)
