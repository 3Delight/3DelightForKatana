# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2017                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

from PackageSuperToolAPI import UIDelegate
from PackageSuperToolAPI import NodeUtils as NU
from PackageSuperToolAPI import Packages
from .DecayFilterPackage import (DecayFilterPackage, DecayFilterEditPackage)
from Katana import QT4FormWidgets, FormMaster, Plugins, UI4

# Our module with common light filter package code.
from . import LightFilterBase;

# Get the base classes for our UI delegate classes from the PackageSuperToolAPI
# using the base classes of our custom Package classes
GafferThreeAPI = Plugins.GafferThreeAPI
LightFilterUIDelegate = UIDelegate.GetUIDelegateClassForPackageClass(
    GafferThreeAPI.PackageClasses.LightFilterPackage)
LightFilterEditUIDelegate = UIDelegate.GetUIDelegateClassForPackageClass(
    GafferThreeAPI.PackageClasses.LightFilterEditPackage)

class DecayFilterCommonUI(LightFilterBase.BaseUIDelegate):
    """
    The common base class of the UIDelegate and the EditUIDelegate.

    This is used for code shared by both classes.
    """

class DecayFilterUIDelegate(LightFilterUIDelegate,DecayFilterCommonUI):
    """
    The UI delegate for the DecayFilter package.

    This class is responsible for exposing the parameters on each of the
    parameter tabs. This is done by creating parameter policies attached to the
    parameters on the package's nodes. We can also modify the appearance of the
    parameter tabs by modifying the hints dictionaries on the policies.
    """

    # The hash used to uniquely identify the action of creating a package
    # This was generated using:
    #     hashlib.md5('DecayFilter.AddDecayFilter').hexdigest()
    AddPackageActionHash = '8657a9337462f8f74cf2cd80978059a3'

    # The keyboard shortcut for creating a package
    DefaultShortcut = 'N'


class DecayFilterEditUIDelegate(LightFilterEditUIDelegate,DecayFilterCommonUI):
    """
    The UI delegate for the DecayFilterEdit package.
    """


# Register the UI delegates

UIDelegate.RegisterUIDelegateClass(DecayFilterPackage, DecayFilterUIDelegate)
UIDelegate.RegisterUIDelegateClass(DecayFilterEditPackage, DecayFilterEditUIDelegate)

