# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2017                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

# Copyright (c) 2015 The Foundry Visionmongers Ltd. All Rights Reserved.

from Katana import NodegraphAPI, Decorators, Plugins
import PackageSuperToolAPI.NodeUtils as NU
from PackageSuperToolAPI import Packages

import os
import textwrap
import logging
import math

# Our module with common light package code.
from . import LightBase;

log = logging.getLogger("GafferThree.SpotLightPackage")

_iconsDir = os.path.join(os.path.dirname(__file__), 'icons')

# Get base classes for our packages from the registered GafferThree packages
GafferThreeAPI = Plugins.GafferThreeAPI
LightPackage = GafferThreeAPI.PackageClasses.LightPackage
LightEditPackage = GafferThreeAPI.PackageClasses.LightEditPackage


class SpotLightBasePackage(LightBase.BasePackage):
    """
    Common code for SpotLightPackage and SpotLightEditPackage.
    """

    @classmethod
    def addLightAttributesNode(cls, packageNode, editing):
        node = NodegraphAPI.CreateNode('DlSpotLightAttributes', packageNode)
        cls.setGenericAssignCEL(node)
        if not editing:
            # Always set value locally.
            node.getParameter(
                'args.geometry.spotRadius.enable').setValue(1, 0)
        NU.AddNodeRef(packageNode, 'lightAttrNode', node)
        cls.appendNode(packageNode, editing, node)

    @classmethod
    def addFovCopyNode(cls, packageNode, editing):
        """
        Appends a node to copy the spot light material's cone angle to
        geometry.fov. This makes the viewer show the right perspective.
        """
        return
        code = textwrap.dedent('''
        ''').strip()
        node = cls.addOpScriptNode(packageNode, editing, 'CopyFovScript', code)
        cls.appendNode(packageNode, editing, node)


class SpotLightPackage(LightPackage,SpotLightBasePackage):
    """
	Implements a Spot Light package. We inherit LightPackage, and override
	some functions to modify behaviour.
    """

    # Class Variables ---------------------------------------------------------

    # The name of the package type as it should be shown in the UI
    DISPLAY_NAME = 'Spot Light'

    # The default name of a package when it is created. This also defines the
    # default name of the package's scene graph location
    DEFAULT_NAME = 'spotLight'

    # The icon to use to represent this package type in the UI
    DISPLAY_ICON = os.path.join(_iconsDir, 'out_delightLightShader.png')

    # Class Functions ---------------------------------------------------------

    @classmethod
    def create(cls, enclosingNode, locationPath):
        """
        A factory method which returns an instance of the class.

        @type enclosingNode: C{NodegraphAPI.Node}
        @type locationPath: C{str}
        @rtype: L{LightPackage}
        @param enclosingNode: The parent node within which the new
            package's node should be created.
        @param locationPath: The path to the location to be created/managed
            by the package.
        @return: The newly-created package instance.
        """
        # Create the package node
        packageNode = cls.createPackageNode(enclosingNode, locationPath)
        # Add nodes to our package group.
        cls.addMasterMaterialPortNode(packageNode)
        # Create the location.
        createNode = cls.addCreateNode(packageNode)
        # Add some boilerplate.
        cls.addPostCreateStandardNodes(packageNode)
        # Add light Material.
        cls.addMaterialNodes(packageNode, False, 'Light', 'spotLight')
        cls.addFovCopyNode(packageNode, False)
        # Add DlObjectSettings node.
        cls.addObjectSettingsNode(packageNode, False)
        # Add SpotLight attributes node.
        cls.addLightAttributesNode(packageNode, False)
        # Add other standard stuff.
        cls.addPruneOpScriptNode(packageNode)
        cls.addChildPackageMergeNode(packageNode)
        cls.addLightLinkingNodes(packageNode, False)

        # Create a package node instance
        result = cls(packageNode)
        Packages.CallbackMixin.executeCreationCallback(result)

        # Create a post-merge stack node for this package
        #postMergeNode = result.createPostMergeStackNode()

        return result

    # Private Class Functions -------------------------------------------------

    @classmethod
    def __createOpScriptNode(cls, parentNode, nodeName, opScript):
        """
        Creates and returns an OpScript node with the given name, under the
        given parent node, and sets the text of the OpScript to the given Lua
        code. The OpScript node will be set to run at the location of this
        package.

        @type parentNode: C{NodegraphAPI.Node}
        @type nodeName: C{str}
        @type opScript: C{str}
        @rtype: C{NodegraphAPI.Node}
        @param parentNode: The enclosing Group node under which to create the
            new OpScript node.
        @param nodeName: The name to give the new OpScript node.
        @param opScript: The text of the Lua code for the OpScript node.
        @return: The newly created OpScript node.
        """
        opScriptNode = NodegraphAPI.CreateNode('OpScript', parentNode)
        opScriptNode.setName(nodeName)
        opScriptNode.getParameter('CEL').setExpression("=^/__gaffer.location")
        opScriptNode.getParameter('script.lua').setValue(opScript, 0)
        return opScriptNode


class SpotLightEditPackage(LightEditPackage,SpotLightBasePackage):
    """
    The edit package that allows a GafferThree to edit an existing SpotLight
    package present in the input Scenegraph.

    This package uses a TransformEdit node to edit the SpotLight's transform.
    """

    # Class Variables ---------------------------------------------------------

    # The icon to use to represent this package type in the UI
    DISPLAY_ICON = os.path.join(_iconsDir, 'out_delightLightShader.png')

    # Class Functions ---------------------------------------------------------

    @classmethod
    def create(cls, enclosingNode, locationPath):
        """
        Creates the contents of the EditStackNode that contains the edit nodes.
        This could be any other kind of node with at least one input and one
        output, but the createPackageEditStackNode() helper function does all
        of the configuration boilerplate code of an EditStackNode for you.
        The return value is a SpotLightEditPackage instance.

        This particular package node will contain a TransformEdit node on it,
        which will allow to edit the transform of a skyDome.
        """
        # Create the package node. Since this is an edit package we want to use
        # an EditStackNode instead of a GroupNode, since it already has an
        # input and an output by default. This also adds some necessary
        # parameters to this node.
        packageNode = cls.createPackageEditStackNode(enclosingNode,
                                                     locationPath)

        # Add the various nodes needed to edit the light.
        cls.addMaterialNodes(packageNode, True, 'Light', 'spotLight')
        cls.addFovCopyNode(packageNode, True)
        cls.addTransformEditNode(packageNode)
        cls.addObjectSettingsNode(packageNode, True)
        cls.addLightAttributesNode(packageNode, True)
        cls.addLightLinkingNodes(packageNode, True)

        # Instantiate a package with the package node
        return cls.createPackage(packageNode)

    @classmethod
    def getAdoptableLocationTypes(cls):
        """
        Returns the set of location types adoptable by this Package. In this
        case, the package can edit locations created by SpotLightPackages, which
        are of the type light.
        """
        return set(('light',))

    # Instance Functions ------------------------------------------------------

    @Decorators.undogroup('Delete SpotLightEdit Package')
    def delete(self):
        LightEditPackage.delete(self)


# Register the package classes, and associate the edit package class with the
# create package class
GafferThreeAPI.RegisterPackageClass(SpotLightPackage)
GafferThreeAPI.RegisterPackageClass(SpotLightEditPackage)
SpotLightPackage.setEditPackageClass(SpotLightEditPackage)
