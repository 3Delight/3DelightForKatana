# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2017                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

import Katana
from . import v1 as XGen

_3DELIGHT_RENDERER_NAME = "dl"

PluginRegistry = [ (
    "SuperTool",
    2,
    _3DELIGHT_RENDERER_NAME.title() + "XGen",
    (XGen.XGenNode, XGen.GetEditor) ) ]

