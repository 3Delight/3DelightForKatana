# vim: set softtabstop=4 expandtab shiftwidth=4:
"""
/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2016                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/
"""

from Katana import UI4, QT4FormWidgets

import KatanaInfo
katanaVersion = int(KatanaInfo.version.split('.')[0])
if katanaVersion >= 8:
    from PySide6 import QtWidgets
else:
    try:
        from PyQt5 import QtWidgets
    except ImportError:
        # Qt4 fallback
        from PyQt4 import QtGui as QtWidgets

from UI4.Manifest import AssetAPI
import os

class XGenEditor(QtWidgets.QWidget):
    def __init__(self, parent, node):
        super(XGenEditor, self).__init__(parent)

        # Save the node to use in future
        self.node = node

        # Start upgrading
        node.Upgrade()

        # UI
        self.setLayout(QtWidgets.QVBoxLayout())
        self.InitUI()
        self.layout().addStretch()

    def InitUI( self ):
        # Location widget
        location_policy = UI4.FormMaster.CreateParameterPolicy(
                None, self.node.getParameter('location'))
        factory = UI4.FormMaster.KatanaFactory.ParameterWidgetFactory
        location_widget = factory.buildWidget(self, location_policy)
        self.layout().addWidget(location_widget)

        # XGen File group
        group_widget = QT4FormWidgets.GroupFormWidget(
                self,
                QT4FormWidgets.PythonGroupPolicy('XGen File'),
                None )

        # A line fith filename
        self.fileopen_edit = QtWidgets.QLineEdit(
                self.node.GetXGenFile(), group_widget)
        self.fileopen_edit.setReadOnly(True)

        # A button to open file
        fileopen_button = QtWidgets.QPushButton("Open...", group_widget)
        fileopen_button.clicked.connect(self.OnFileOpen)

        # A layout with the line and the button
        fileopen_layout = QtWidgets.QHBoxLayout()
        fileopen_layout.addWidget(self.fileopen_edit)
        fileopen_layout.addWidget(fileopen_button)

        form = QtWidgets.QFormLayout()
        form.addRow("File", fileopen_layout)

        group_layout = group_widget.getPopdownWidget().layout()
        group_layout.addLayout(form)

        self.layout().addWidget( group_widget )

        # Viewer group
        group_widget = QT4FormWidgets.GroupFormWidget(
                self,
                QT4FormWidgets.PythonGroupPolicy('Viewer'),
                None )

        # Location widget
        density_policy = UI4.FormMaster.CreateParameterPolicy(
                None, self.node.getParameter('density'))
        density_widget = factory.buildWidget(self, density_policy)
        group_layout = group_widget.getPopdownWidget().layout()
        group_layout.addWidget(density_widget)

        self.layout().addWidget( group_widget )

    def OnFileOpen(self):
        # Standard way to open File Open window
        context = AssetAPI.kAssetTypeShader
        hints = {'fileTypes': 'xgen', 'context': context}
        result = UI4.Util.AssetId.BrowseForAsset(
                '', 'Open XGen file', False, hints)
        if result is None:
            return
        result = os.path.expandvars(result)
        result = os.path.expanduser(result)

        # Save the file to the node. It should rebuild the network.
        self.node.SetXGenFile(result)

        # Change the text in the UI
        self.fileopen_edit.setText(result)

