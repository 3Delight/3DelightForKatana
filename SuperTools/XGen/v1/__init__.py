# vim: set softtabstop=4 expandtab shiftwidth=4:

from .Node import XGenNode

def GetEditor():
    from . import Editor
    return Editor.XGenEditor

