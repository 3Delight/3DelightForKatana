################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2016                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

from Katana import Nodes3DAPI, UI4

import KatanaInfo
katanaVersion = int(KatanaInfo.version.split('.')[0])
if katanaVersion >= 8:
	from PySide6 import QtCore, QtWidgets
else:
	from Katana import QtCore
	try:
		from PyQt5 import QtWidgets
	except ImportError:
		# Qt4 fallback
		from PyQt4 import QtGui as QtWidgets

ITEM_TYPE_ROLE = 1

class QDLMulticamWidget(QtWidgets.QWidget):
	def __init__(self, parent, node):
		QtWidgets.QWidget.__init__(self, parent)

		self.__parent = parent
		self.__node = node
		# Number of visible items in the list
		self.nbitems = 10

		self.InitUI()

	def SetListHeight(self):
		item = QtWidgets.QListWidgetItem('RigsWhatever')
		# Need to set this to get the correct height!
		item.setData( ITEM_TYPE_ROLE, 'Separator' )
		self.list_widget.addItem( item )
		self.list_widget.setFixedHeight( 
			self.nbitems*self.list_widget.visualItemRect(item).height() )
		#self.list_widget.removeItemWidget( item )
		self.list_widget.clear()

	def CenterWidget(self, i_widget, i_layout):
		layout = QtWidgets.QHBoxLayout()
		layout.addStretch()
		layout.addWidget(i_widget)
		layout.addStretch()
		i_layout.addLayout( layout )

	def InitUI(self):
		self.main_layout = QtWidgets.QVBoxLayout()

		# A warning about missing cameras, to be shown when relevant.
		#
		icon = UI4.Util.ScenegraphIconManager.GetIcon('warning')

		self.missing_warning_icon = QtWidgets.QLabel( self )

		font_metrics = self.missing_warning_icon.fontMetrics()
		fm_line_spacing = font_metrics.lineSpacing()
		
		icon_size = int(2.5 * fm_line_spacing)

		margin = fm_line_spacing
		self.main_layout.setContentsMargins(margin, margin, margin, margin)

		self.missing_warning_icon.setPixmap( 
			icon.pixmap( icon_size, icon_size  ) )
		self.missing_warning_icon.setVisible(False)

		self.missing_warning_label = QtWidgets.QLabel( self )
		self.missing_warning_label.setText( 'Some previously selected ' +
			'cameras \nare not present in the incoming scene.')

		self.missing_warning_label.setVisible(False)
		self.missing_warning_layout = QtWidgets.QHBoxLayout()
		self.missing_warning_layout.addStretch()
		self.missing_warning_layout.addWidget(self.missing_warning_icon)
		self.missing_warning_layout.addWidget(self.missing_warning_label)
		self.missing_warning_layout.addStretch()
		self.main_layout.addLayout(self.missing_warning_layout)

		self.list_widget = QtWidgets.QListWidget( self )
		self.list_widget.setMinimumWidth( 22 * fm_line_spacing )

		self.list_widget.setSelectionMode(
				QtWidgets.QAbstractItemView.MultiSelection)

		# Override the disabled item stylesheet
		# The AOV selector uses the style sheet in QDLStyle.py
		# For the "category" item to be shown on a background that matches
		# the frame color, we must override the style sheet; trying to set
		# the background color will not work because the style sheet appears to 
		# be applied after.
		#
		self.list_widget.setStyleSheet( "::item:disabled { color: black; " +
			"padding-left: -10px; background-color: #404040 }")
		
		# Center widget
		self.CenterWidget(self.list_widget, self.main_layout)

		# Callback when selection changed (deals with click or
		# with press and drag)
		self.list_widget.itemSelectionChanged.connect(
			self.OnItemSelectionChanged)

		self.setLayout(self.main_layout)

	def GetCamerasFromCameraList(self, root, io_cameras):
		"""
		Fills camera set from the camera list at /root/world
		"""
		world = root.getChildByName('world')
		if not world:
			return
		camera_list = world.getAttribute('globals.cameraList')
		if not camera_list:
			return
		cameras = camera_list.getNearestSample(0.0)
		io_cameras |= set(cameras)

	# Clear then fill the multi-camera list.
	def FillCameraList(self):
		old_state = self.list_widget.blockSignals(True)

		# Clear the list
		self.list_widget.clear()
		self.list_widget.setVerticalScrollBarPolicy( 
			QtCore.Qt.ScrollBarAlwaysOff )

		# The camera list is built as follows:
		# - Missing cameras.
		#     Lists the cameras used by layers that are no longer defined as
		#     proper scene graph locations.
		# - Cameras.
		#     List the cameras defined in the scene. The cameras that are used
		#     by layers are set as selected.
		#
		# If a layer is set to use a camera, it must be shown in the list.
		#

		# Figure the list of cameras available in the scene.
		# Get the root node
		root = Nodes3DAPI.GetGeometryProducer(self.__node)
		if not root:
			self.list_widget.blockSignals( old_state )
			return

		cameras = set()

		# Parse the camera list to get cameras
		self.GetCamerasFromCameraList(root, cameras)

		# Remove the render camera
		render_cam = '/root/world/cam/camera'
		render_cam_att = root.getAttribute('renderSettings.cameraName')
		if render_cam_att:
			render_cam = render_cam_att.getValue()
		cameras.discard(render_cam)

		# Figure the list of cameras used by layers
		layer_cameras = self.__node.GetAltCameras()

		missing_layer_cameras = []
		for i in layer_cameras:
			# Find layer_cameras which refer to invalid scene graph locations
			camera_prod = root.getProducerByPath( i )
			if not camera_prod:
				# Invalid scene graph location. Add to the missing list.
				missing_layer_cameras.append( i )

		# Figure the list item height based on font metrics, like the AOV
		# selector.
		#
		font_metrics = self.list_widget.fontMetrics()
		item_height = int(font_metrics.lineSpacing() * 1.77)

		# Add the missing camera layers first
		if missing_layer_cameras:
			item = QtWidgets.QListWidgetItem('Missing')
			font = item.font()
			font.setBold(True)
			item.setFont(font)
			item.setFlags( QtCore.Qt.NoItemFlags )
			item.setData(ITEM_TYPE_ROLE, 'Separator' )
			AdjustItemHeight(item, item_height)
			self.list_widget.addItem( item )

			icon = UI4.Util.ScenegraphIconManager.GetIcon('warning')
			for i in missing_layer_cameras:
				item = QtWidgets.QListWidgetItem( i )
				item.setData(ITEM_TYPE_ROLE, 'Missing' )
				item.setIcon(icon)
				AdjustItemHeight(item, item_height)
				self.list_widget.addItem( item )
				item.setSelected( True )

			self.missing_warning_icon.setVisible( True )
			self.missing_warning_label.setVisible( True )
		else:
			self.missing_warning_icon.setVisible( False )
			self.missing_warning_label.setVisible( False )


		# Add the cameras
		if cameras:
			item = QtWidgets.QListWidgetItem('Cameras')
			font = item.font()
			font.setBold(True)
			item.setFont(font)
			item.setFlags( QtCore.Qt.NoItemFlags )
			item.setData(ITEM_TYPE_ROLE, 'Separator' )
			AdjustItemHeight(item, item_height)
			self.list_widget.addItem( item )

			camera_list = sorted(cameras)
			for i in camera_list:
				item = QtWidgets.QListWidgetItem( i )
				item.setData(ITEM_TYPE_ROLE, 'Camera' )
				AdjustItemHeight(item, item_height)
				self.list_widget.addItem( item )

				if i in layer_cameras:
					item.setSelected( True )

		self.list_widget.blockSignals(old_state);

		if self.list_widget.count() > self.nbitems:
			self.list_widget.setVerticalScrollBarPolicy( 
				QtCore.Qt.ScrollBarAlwaysOn )

	def OnItemSelectionChanged(self):
		selectedItems = self.list_widget.selectedItems()
		layer_cameras = self.__node.GetAltCameras()

		if len(selectedItems) > len(layer_cameras):
			for i in selectedItems:
				if i.text() not in layer_cameras:
					self.__node.AddMultiCamera(str(i.text()))
					break
		else:
			selectedText = []
			for i in selectedItems:
				selectedText.append(str(i.text()))
			for i in layer_cameras:
				if i not in selectedText:
					self.__node.RemoveMultiCamera(i)
					break

def AdjustItemHeight( item, height ):
	size_hint = item.sizeHint()
	size_hint.setHeight( height )
	item.setSizeHint( size_hint )

