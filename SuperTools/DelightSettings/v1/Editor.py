################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2016                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

from Katana import UI4, QT4Widgets, QT4FormWidgets

import KatanaInfo
katanaVersion = int(KatanaInfo.version.split('.')[0])
if katanaVersion >= 8:
	from PySide6 import QtCore, QtWidgets
else:
	from Katana import QtCore
	try:
		from PyQt5 import QtWidgets
	except ImportError:
		# Qt4 fallback
		from PyQt4 import QtGui as QtWidgets

from Katana import NodegraphAPI, Utils
from Katana import UniqueName, FormMaster, KatanaFile

from . import QDLStyle
from . import ScriptActions as SA
import os
from .QDLMulticamWidget import QDLMulticamWidget
from .QDLMultilightWidget import QDLMultilightWidget

_3DELIGHT_RENDERER_NAME = "dl"

aovs_minimum_rows = 4
selector_min_items = 9

class DelightSettingsEditor(QtWidgets.QWidget):
	def __init__(self, parent, node):
		node.Upgrade()

		self.__node = node

		QtWidgets.QWidget.__init__(self, parent)
		QtWidgets.QVBoxLayout(self)

		# UI
		self.InitNodeGroupUI('render_globals', 'quality')
		self.InitCameraUI()
		self.InitNodeGroupUI('render_globals', 'atmosphere')
		self.InitImageLayersUI()
		self.InitOverridesUI()
		self.layout().addStretch()

		self.InitTables()
		# Adjust multilight list height here to get correct item height
		self.multilight.SetListHeight()
		self.multicam.SetListHeight()

		self.__frozen = True

	def showEvent(self, event):
		"""
		Overrides the standard C{QWidget} event handler for "show" events.
		"""
		QtWidgets.QWidget.showEvent(self, event)
		self.__thaw()

	def hideEvent(self, event):
		"""
		Overrides the standard C{QWidget} event handler for "hide" events.
		"""
		QtWidgets.QWidget.hideEvent(self, event)
		self.__freeze()

	def __thaw(self):
		if not self.__frozen:
			return
		self.__frozen = False

		Utils.EventModule.RegisterCollapsedHandler(
			self.NodeGraphChangedEH, 'nodegraph_changed')
		Utils.EventModule.RegisterCollapsedHandler(
			self.SceneGraphLocationDeletedEH,
			'sceneGraph_locationDeleted')
		Utils.EventModule.RegisterCollapsedHandler(
			self.SceneGraphLocationRenamedEH,
			'sceneGraph_locationRenamed')

		# Always update this as we did not get callbacks while UI was hidden.
		# This also handles the initial init.
		self.multilight.FillLightList()
		self.multicam.FillCameraList()

	def __freeze(self):
		if self.__frozen:
			return
		self.__frozen = True

		# Remove event handlers so they don't pile up.
		Utils.EventModule.UnregisterCollapsedHandler(
			self.NodeGraphChangedEH, 'nodegraph_changed')
		Utils.EventModule.UnregisterCollapsedHandler(
			self.SceneGraphLocationDeletedEH,
			'sceneGraph_locationDeleted')
		Utils.EventModule.UnregisterCollapsedHandler(
			self.SceneGraphLocationRenamedEH,
			'sceneGraph_locationRenamed')

	# With some of the events we get, we can't update the light list right away
	# as the scene graph does not yet have the change. We call this to queue an
	# update for later.
	# A timer was suggested by Stefan and appears to be the only reliable
	# solution. And it needs a decent delay to actually work (50 does not cut
	# it). I also tried a low priority event, as in:
	# Utils.EventModule.QueueEvent(
	#   'delightSettings_updateLights', id(self), priority=-100)
	# But that only worked most of the time (~90%). And it needs some extra
	# code to register a handler.
	def QueueLightListUpdate(self):
		QtCore.QTimer.singleShot(250, self.multilight.FillLightList)

	def QueueCameraListUpdate(self):
		QtCore.QTimer.singleShot(250, self.multicam.FillCameraList)

	# Updates the multi-light list in the three following cases:
	#
	# a) 'Switch' type is detected, user has press 'd' on some node
	#    (but we can't be sure that it is a light - so we always ask
	#     for an update)
	# b) 'Group' type is detected. Search in the list of args for a
	#    'paramName' which contains 'Group.__gaffer' indicating a
	#    node create for a light
	# c) 'Group' type is detected. Search in the list of args for a
	#    'newName' which contains '__SAVE_' indicating an 'undo' of
	#    previous deletion
	#
	# Note: we can't use neither 'node_create' event nor 'onNodeCreate'
	# callback (or 'onGafferLightCreated' callback) because scene graph
	# is not yet updated from the node graph.
	def NodeGraphChangedEH(self, args):
		# Always update the camera list, by the KISS principle.
		self.QueueCameraListUpdate()

		if not 'node' in args[0][2]:
			return

		node = args[0][2]['node']
		if node.getType() == 'Switch':
			# case a)
			self.QueueLightListUpdate()
			return

		if node.getType() != 'Group':
			return
		for i in args:
			if 'paramName' in i[2]:
				paramName = i[2]['paramName']
				if paramName == 'Group.__gaffer':
					# case b)
					self.QueueLightListUpdate()
					break
			if 'newName' in i[2]:
				newName = i[2]['newName']
				if newName[:7] == '__SAVE_':
					# case c)
					self.multilight.FillLightList()
					break

	# Updates the multi-light list if some location has been deleted.
	# Note: can't check Type info about location path in args since
	# scene graph has already been modified - so we always update the
	# multi-light list even if it is not needed
	def SceneGraphLocationDeletedEH(self, args):
		self.multilight.FillLightList()

	# Updates the multi-light list if some location has been renamed.
	# Note: we always update the multi-light list even if it is not
	# needed, because checking Type info on location path in args needs
	# to cook the scene with Nodes3DAPI.GetGeometryProducer.
	def SceneGraphLocationRenamedEH(self, args):
		self.QueueLightListUpdate()

	def InitNodeGroupUI( self, node, group ):
		globals_node = SA.GetRefNode(self.__node, node)
		group_parameter = \
			globals_node.getParameter(
				'args.' + _3DELIGHT_RENDERER_NAME + 'GlobalStatements.' + group)

		policy = UI4.FormMaster.CreateParameterPolicy(None, group_parameter)
		widget_factory = UI4.FormMaster.KatanaFactory.ParameterWidgetFactory
		widget = widget_factory.buildWidget(self, policy)
		self.layout().addWidget(widget)
		return widget

	def InitCameraUI( self ):
		self.setStyleSheet( QDLStyle.GetStyle() )

		# Build a group widget
		group_widget = QT4FormWidgets.GroupFormWidget(
				self,
				QT4FormWidgets.PythonGroupPolicy('Camera'),
				None )
		group_layout = group_widget.getPopdownWidget().layout()

		# Builds subgroup Shutter
		subgroup1_policy = QT4FormWidgets.PythonGroupPolicy('Shutter')

		self.InitNodeSGSingleItemUI('render_globals', 'GlobalStatements.', 'camera.', 'shutterAngle', subgroup1_policy)
		self.InitNodeSGSingleItemUI('camera_settings', 'CameraSettings.', 'shutter.', 'shutterOpenEff', subgroup1_policy)
		self.InitNodeSGSingleItemUI('camera_settings', 'CameraSettings.', 'shutter.', 'shutterCloseEff', subgroup1_policy)
		widget_factory = UI4.FormMaster.KatanaFactory.ParameterWidgetFactory
		widget = widget_factory.buildWidget(self, subgroup1_policy)
		group_layout.addWidget( widget )

		# Adds subgroup Lens
		self.InitNodeSubGroupUI('camera_settings', 'lens', group_widget)

		# Builds subgroup Projection
		subgroup2_policy = QT4FormWidgets.PythonGroupPolicy('Projection')
		self.InitNodeSingleItem2UI('camera_settings', 'geometry.', 'projection', subgroup2_policy)
		self.InitNodeSingleItem2UI('camera_settings', 'geometry.', 'auxFov', subgroup2_policy)
		widget_factory = UI4.FormMaster.KatanaFactory.ParameterWidgetFactory
		widget = widget_factory.buildWidget(self, subgroup2_policy)
		group_layout.addWidget( widget )

		self.layout().addWidget( group_widget )

	def InitOverridesUI( self ):
		self.setStyleSheet( QDLStyle.GetStyle() )

		group_widget = self.InitNodeGroupUI('render_globals', 'overrides')
		group_layout = group_widget.getPopdownWidget().layout()

		buttons_layout = QtWidgets.QVBoxLayout()
		buttons_layout.setSpacing( 5 )
		buttons_layout.setContentsMargins( 5, 5, 0, 0 )

		message = QtWidgets.QLabel()
		message.setText("These settings are ignored in batch rendering\n(unless specifically called for using the command\nline option -overrides).")
		buttons_layout.addWidget(message)

		group_layout.addLayout( buttons_layout )

	def InitNodeSingleItemUI( self, node, name, subgroup, item, parent ):
		some_node = SA.GetRefNode(self.__node, node)
		single_parameter = \
			some_node.getParameter(
				'args.' + _3DELIGHT_RENDERER_NAME + name + subgroup + item)

		policy = UI4.FormMaster.CreateParameterPolicy(None, single_parameter)
		widget_factory = UI4.FormMaster.KatanaFactory.ParameterWidgetFactory
		widget = widget_factory.buildWidget(self, policy)
		parent.getPopdownWidget().layout().addWidget(widget)
		return policy,widget

	def InitNodeSGSingleItemUI( self, node, name, subgroup, item, parent_policy ):
		some_node = SA.GetRefNode(self.__node, node)
		single_parameter = \
			some_node.getParameter(
				'args.' + _3DELIGHT_RENDERER_NAME + name + subgroup + item)

		policy = UI4.FormMaster.CreateParameterPolicy(parent_policy, single_parameter)
		parent_policy.addChildPolicy(policy)

	def InitNodeSingleItem2UI( self, node, name, item, parent_policy ):
		some_node = SA.GetRefNode(self.__node, node)
		single_parameter = \
			some_node.getParameter('args.' + name + item)

		# Needs to put parent_policy in constructor and add policy to it
		# in order to process conditional lock for item 'auxFov'
		policy = UI4.FormMaster.CreateParameterPolicy(parent_policy, single_parameter)
		parent_policy.addChildPolicy(policy)

	def InitNodeSubGroupUI( self, node, subgroup, parent ):
		camera_node = SA.GetRefNode(self.__node, node)
		group_parameter = \
			camera_node.getParameter(
				'args.' + _3DELIGHT_RENDERER_NAME + 'CameraSettings.' + subgroup)

		policy = UI4.FormMaster.CreateParameterPolicy(None, group_parameter)
		widget_factory = UI4.FormMaster.KatanaFactory.ParameterWidgetFactory
		widget = widget_factory.buildWidget(self, policy)
		parent.getPopdownWidget().layout().addWidget(widget)

	def InitImageLayersUI( self ):
		self.setStyleSheet( QDLStyle.GetStyle() )

		# Build a group widget
		group_widget = QT4FormWidgets.GroupFormWidget(
				self,
				QT4FormWidgets.PythonGroupPolicy('Image Layers'),
				None )
		group_layout = group_widget.getPopdownWidget().layout()

		metrics = group_widget.fontMetrics()
		if hasattr(metrics, 'horizontalAdvance'):
			text_width = metrics.horizontalAdvance("Image Filename") + 22
		else:
			text_width = metrics.width("Image Filename") + 22

		warning_layout = QtWidgets.QHBoxLayout()
		self.warning_label = QtWidgets.QLabel(
			"Files will not be saved. Please turn off standard render output.")
		self.warning_label.setStyleSheet("QLabel { color : red; }");
		self.warning_label.setVisible(False)
		warning_layout.addWidget( self.warning_label )
		group_layout.addLayout( warning_layout )

		## Default Image Filename
		self.filenameParamPolicy = \
			self.InitNodeSingleItemUI('render_globals', 'GlobalStatements.', 'layers.', 'imageFilename', group_widget)[0]
		self.filenameParamPolicy.addCallback(
			self.filenameParamChangedCallback)

		if not self.filenameParamPolicy.getValue():
			self.filenameParamPolicy.setValue(SA.GetDefaultFilename())

		# Image Format
		filetype_layout = QtWidgets.QHBoxLayout()

		self.filetype_label_widget = QtWidgets.QLabel("Image Format")
		self.filetype_label_widget.setAlignment(
			QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		self.filetype_label_widget.setFixedWidth(text_width)

		self.filetype_widget = QtWidgets.QComboBox()
		self.filetype_widget.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
		self.filetype_widget.insertItems(
				0, [row[0] for row in SA.display_drivers] )

		# Fetch a reference layer node to init the UI.
		ref_layer = SA.GetFirstLayerNode(self.__node)

		# Get default type from the reference layer. OpenEXR if no layer.
		type_index = 1
		if ref_layer:
			driver_param = ref_layer.getParameter(
				SA.rendererSettings_prefix + 'driver.value')
			if driver_param:
				for i in range(len(SA.display_drivers)):
					if SA.display_drivers[i][2] == driver_param.getValue(0):
						type_index = i
		self.filetype_widget.setCurrentIndex( type_index )

		self.filetype_widget.currentIndexChanged[int].connect(
				self.OnFiletypeChanged)

		# Image Depth
		self.filedepth_widget = QtWidgets.QComboBox()
		self.filedepth_widget.setContextMenuPolicy(QtCore.Qt.NoContextMenu);
		self.filedepth_widget.insertItems( 0, SA.quantizes )
		# Get default depth from the reference layer. 16 if no layer.
		bit_depth = '16'
		if ref_layer:
			ext_param = ref_layer.getParameter(
				SA.rendererSettings_prefix + 'fileExtension.value')
			if ext_param:
				depth_param = ref_layer.getParameter(
					SA.rendererSettings_prefix + 'convertSettings.' +
					ext_param.getValue(0) + 'BitDepth.value')
				if depth_param:
					bit_depth = depth_param.getValue(0)
		# See if we should fetch a value from the legacy parameter. This
		# overrides the layer default as that was not set back then.
		globals_node = SA.GetRefNode(self.__node, 'render_globals')
		depth_enable_param = globals_node.getParameter(
			'args.dlGlobalStatements.layers.depth.enable')
		if depth_enable_param.getValue(0):
			depth_param = globals_node.getParameter(
				'args.dlGlobalStatements.layers.depth.value')
			bit_depth = str(int(depth_param.getValue(0)))
		# Finally set the widget.
		if bit_depth == '8':
			self.filedepth_widget.setCurrentIndex(0)
		elif bit_depth == '32':
			self.filedepth_widget.setCurrentIndex(2)
		else:
			self.filedepth_widget.setCurrentIndex(1)

		self.filedepth_widget.currentIndexChanged[int].connect(
				self.OnFiledepthChanged)

		# Add widgets to HBox Layout
		filetype_layout.addWidget( self.filetype_label_widget )
		filetype_layout.addWidget( self.filetype_widget )
		filetype_layout.addWidget( self.filedepth_widget )
		group_layout.addLayout( filetype_layout )

		# Add line
		line = QtWidgets.QFrame()
		line.setFrameShape( QtWidgets.QFrame.HLine )
		line.setFrameShadow( QtWidgets.QFrame.Sunken )
		group_layout.addWidget( line );

		# Header layout. Small text "Output" and "Image Layer (AOV)"
		header_layout = QtWidgets.QHBoxLayout()
		layer_label_widget = QtWidgets.QLabel( "<b>Image Layer (AOV)</b>" )
		layer_label_widget.setStyleSheet( QDLStyle.GetSmallStyle() )

		# Format it
		layer_label_widget.setAlignment(
			QtCore.Qt.AlignCenter|QtCore.Qt.AlignVCenter)
		header_layout.addWidget( layer_label_widget )

		# Put it to the group widget
		group_layout.addLayout( header_layout )

		# The table
		self.table = QtWidgets.QTableWidget(aovs_minimum_rows, 1)
		self.table.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
		self.table.setShowGrid(False)

		# Turn off scroll bars. The widget should change the size with adding
		# AOVs
		self.table.setHorizontalScrollBarPolicy(
                        QtCore.Qt.ScrollBarAlwaysOff )
		self.table.setVerticalScrollBarPolicy(
                        QtCore.Qt.ScrollBarAlwaysOff )

		metrics = self.table.fontMetrics()
		section_size = int(metrics.height() * 1.6)

		verticalHeader = self.table.verticalHeader()
		verticalHeader.hide()
		if hasattr(verticalHeader, 'setSectionResizeMode'): # New Qt 5 name
			verticalHeader.setSectionResizeMode(QtWidgets.QHeaderView.Fixed)
		else:
			verticalHeader.setResizeMode(QtWidgets.QHeaderView.Fixed)
		verticalHeader.setDefaultSectionSize(section_size)

		horizontalHeader = self.table.horizontalHeader()
		horizontalHeader.hide()
		horizontalHeader.setDefaultSectionSize(section_size)
		if hasattr(horizontalHeader, 'setSectionResizeMode'): # New Qt 5 name
			horizontalHeader.setSectionResizeMode(
				0, QtWidgets.QHeaderView.Stretch)
		else:
			horizontalHeader.setResizeMode(0, QtWidgets.QHeaderView.Stretch)

		self.ResizeAOVsTable()

		self.table.cellChanged[int, int].connect(
				self.OnAOVNameChanged)
		self.table.itemSelectionChanged.connect(
				self.OnAOVSelectionChanged)

		group_layout.addWidget(self.table)

		# Add... Remove Duplicate View...
		buttons_layout = QtWidgets.QHBoxLayout()
		buttons_layout.setSpacing( 0 )
		margin = int(metrics.lineSpacing() / 4)
		buttons_layout.setContentsMargins(margin, margin, margin, margin)

		# Note: the button height multiplicator was derived from 
		# button.sizeHint().height / fontMetrics.lineSpacing() on a standard def
		# display.
		#
		button_height = int(metrics.lineSpacing() * 1.77)

		add_button = QtWidgets.QPushButton("Add...")
		add_button.setMinimumHeight( button_height )
		add_button.clicked.connect(self.OnAddPressed)
		buttons_layout.addWidget( add_button )

		remove_button = QtWidgets.QPushButton("Remove")
		remove_button.setMinimumHeight( button_height )
		remove_button.clicked.connect(self.OnRemovePressed)
		buttons_layout.addWidget( remove_button )

		# Put it to the group widget
		group_layout.addLayout( buttons_layout )

		# Add Multilight group
		self.multilight_group_widget = QT4FormWidgets.GroupFormWidget(
				self,
				QT4FormWidgets.PythonGroupPolicy('Multi-Light'),
				None )

		multilight_group_layout = \
				self.multilight_group_widget.getPopdownWidget().layout()

		self.multilight = QDLMultilightWidget(self, self.__node)
		# The style doesn't inherit from the main layout because of
		# GroupFormWidget
		self.multilight.setStyleSheet( QDLStyle.GetStyle() )

		multilight_group_layout.addWidget(self.multilight)

		# Put it to the group widget
		group_layout.addWidget( self.multilight_group_widget )

		# Add Multicam group and widget
		self.multicam_group_widget = QT4FormWidgets.GroupFormWidget(
				self,
				QT4FormWidgets.PythonGroupPolicy('Multi-Camera'),
				None )
		multicam_group_layout = \
				self.multicam_group_widget.getPopdownWidget().layout()
		self.multicam = QDLMulticamWidget(self, self.__node)
		self.multicam.setStyleSheet(QDLStyle.GetStyle())
		multicam_group_layout.addWidget(self.multicam)
		group_layout.addWidget(self.multicam_group_widget)


		self.layout().addWidget( group_widget )

	def InitAOVSelector( self ):
		self.aov_selector = QtWidgets.QDialog()

		# Get the font metrics to adjust the UI reltative to font size
		font_metrics = self.aov_selector.fontMetrics()
		# line spacing appears to be a good metric to base the layout on
		fm_line_spacing = font_metrics.lineSpacing()

		self.aov_selector.setWindowTitle( "AOV Selector" )

		# The dialog appears to come with built-in margins of 10 pixels.
		# Replace these with margins adapted to font size.
		#
		self.aov_selector.setContentsMargins( 
			-10 + 2 * fm_line_spacing, -10 + fm_line_spacing, \
			-10 + 2 * fm_line_spacing, -10 + int(fm_line_spacing / 2) )

		dialog_layout = QtWidgets.QVBoxLayout()

		main_layout = QtWidgets.QHBoxLayout()

		# The default list item height is too thight when the UI is scaled 
		# (which is the case for hiDPI displays)
		#
		item_height = int(font_metrics.lineSpacing() * 1.77)
		column_width = int(font_metrics.lineSpacing() * 13)
		
		# Same height as the add / remove / duplicate buttons
		button_height = int(font_metrics.height() * 1.77)
		
		# The labels at the top of the AOV selector columns.
		selector_labels = (
			"<b>Shading Components</b>",
			"<b>Auxiliary Variables</b>",
			"<b>User Variables</b>")

		# The AOV definitions for each column.
		self._aov_selector_defs = (
			SA.shading_components,
			SA.auxiliary_components,
			SA.GetUserComponents())

		# The list widgets for those columns.
		self._aov_selector_widgets = [None, None, None]

		num_rows = 0
		for c in range(len(self._aov_selector_defs)):
			# Add a spacer between the lists
			if c != 0:
				main_layout.addSpacing(2 * fm_line_spacing)

			column_label = QtWidgets.QLabel(selector_labels[c])
			column_label.setAlignment(QtCore.Qt.AlignCenter)
			column_label.setFixedHeight(int(font_metrics.lineSpacing() * 1.77))

			# Table
			self._aov_selector_widgets[c] = QtWidgets.QListWidget()
			self._aov_selector_widgets[c].setStyleSheet(QDLStyle.GetStyle())
			self._aov_selector_widgets[c].setSelectionMode(
				QtWidgets.QAbstractItemView.MultiSelection)

			FillAOVList(self._aov_selector_widgets[c],
				self._aov_selector_defs[c], item_height)
			num_rows = max(num_rows, self._aov_selector_widgets[c].count())

			# Turn off scroll bars.
			self._aov_selector_widgets[c].setHorizontalScrollBarPolicy(
					QtCore.Qt.ScrollBarAlwaysOff )
			self._aov_selector_widgets[c].setVerticalScrollBarPolicy(
					QtCore.Qt.ScrollBarAlwaysOff )

			# Put the column's widgets into a layout and add that to dialog.
			column_layout = QtWidgets.QVBoxLayout()
			column_layout.addWidget(column_label)
			column_layout.addWidget(self._aov_selector_widgets[c])
			main_layout.addLayout(column_layout)

		# Set list height to num items * item height - 1 to hide the last item
		# divider line, then add the list border (top & bottom borders)
		#
		row_height = self._aov_selector_widgets[0].sizeHintForRow(0)
		height = row_height * num_rows - 1 + \
			2 * self._aov_selector_widgets[0].frameWidth()
		# Give all list widgets the same size.
		for c in range(len(self._aov_selector_defs)):
			self._aov_selector_widgets[c].setFixedSize(column_width, height)

		dialog_layout.addLayout(main_layout)
		# Add a spacer below the lists. There is something that adds 6 blank 
		# linesa above the buttons. Replace that with a spacer adapted to font 
		# size.
		dialog_layout.addSpacing( -6 + 2 * fm_line_spacing )

		# Buttons bottom
		ok_widget = QtWidgets.QPushButton("OK")
		ok_widget.setMinimumWidth(int(column_width * 2 / 5))
		ok_widget.setMaximumWidth(int(column_width * 2 / 5))
		ok_widget.setMinimumHeight( button_height )
		ok_widget.clicked.connect(self.OnAcceptAOVSelector)

		cancel_widget = QtWidgets.QPushButton("Cancel")
		cancel_widget.setMinimumWidth(int(column_width * 2 / 5))
		cancel_widget.setMaximumWidth(int(column_width * 2 / 5))
		cancel_widget.setMinimumHeight( button_height )
		cancel_widget.clicked.connect(self.OnCancelAOVSelector)

		buttons_layout = QtWidgets.QHBoxLayout()
		buttons_layout.addWidget(ok_widget)
		buttons_layout.addWidget(cancel_widget)

		# Widget to control buttons' position
		buttons_widget = QtWidgets.QWidget()
		buttons_widget.setLayout(buttons_layout)
		buttons_widget.setMinimumWidth(column_width )
		buttons_widget.setMaximumWidth(column_width )
		# We need this layout to keep the buttons in the center
		buttons_widget_layout = QtWidgets.QHBoxLayout()
		buttons_widget_layout.addWidget(buttons_widget)

		dialog_layout.addLayout(buttons_widget_layout)

		self.aov_selector.setLayout(dialog_layout)

	def IsEmptyRow(self, i_row):
		item = self.table.item(i_row, 0)
		return not item or not item.text()

	#
	# Add aov layer in the widget table. Adjust selection if needed.
	#
	def AddLayer(self, i_aov, i_row=-1):
		if not i_aov:
			print("Can't add AOV to the Image Layers Widget")
			return

		rows = self.table.rowCount()
		# Count how many filled rows we have
		filled_rows = 0
		while not self.IsEmptyRow( filled_rows ) and filled_rows < rows:
			filled_rows += 1

		if filled_rows >= rows:
			# Get number of current row
			row = rows if i_row<0 else i_row
			self.table.insertRow(row)
			self.ResizeAOVsTable()
		else:
			row = filled_rows

		# Push the name of the AOV
		item = QtWidgets.QTableWidgetItem( i_aov )
		self.table.setItem( row, 0, item )
		item.setFlags( item.flags() & ~QtCore.Qt.ItemIsEditable )

		# Add empty item if needed (so we can't select empty row)
		rows = self.table.rowCount()
		for i in range(row+1, rows):
			if not self.table.item(i, 0):
				empty_item = QtWidgets.QTableWidgetItem( "" )
				empty_item.setFlags( QtCore.Qt.NoItemFlags )
				self.table.setItem( i, 0, empty_item )

		layer_node = SA.GetLayerGroup(self.__node, i_aov)
		selected_param = layer_node.getParameter("selected")
		selected = True if selected_param.getValue(0) == "Yes" else False

		# Block signals to prevent OnAOVSelectionChanged
		old_state = self.table.blockSignals(True);
		item.setSelected(selected)
		self.table.blockSignals(old_state);

	def GetSelection(self):
		# Returns the indexes of the selected AOVs
		selection = set()
		for i in self.table.selectedItems():
			row = i.row()
			# Check if it's not an empty row
			if not self.IsEmptyRow( row ):
				selection.add( i.row() )

		return list(selection)

	def OnAOVNameChanged(self, row, column):
		if column != 0:
			return

		item = self.table.item(row, column)
		if not item.text():
			return
		self.__node.RenameLayer(row, item.text())

	def OnAOVSelectionChanged(self):
		old_state = self.table.blockSignals(True);

		selection = self.GetSelection()

		for i in range(self.table.rowCount()):
			# Check if it's an empty row
			if self.IsEmptyRow( i ):
				continue

			item = self.table.item(i, 0)
			if not item:
				continue

			value = "Yes" if item.isSelected() else "No"

			layer_group = SA.GetLayerGroup(self.__node, item.text())
			parameter = layer_group.getParameter("selected")
			parameter.setValue( value, 0 )

		self.table.blockSignals(old_state);

	def OnAddPressed(self):
		# The whole dialog is rebuilt every time because user AOVs can change.
		self.InitAOVSelector()
		self.aov_selector.setStyleSheet( QDLStyle.GetStyle() )
		if hasattr(self.aov_selector, 'exec'):
			getattr(self.aov_selector, 'exec')()
		else:
			self.aov_selector.exec_()

	def OnRemovePressed(self):
		while True:
			# While there is selection
			items = self.table.selectedItems()
			if len(items) == 0 or not items[0]:
				break

			# Remove the first row from the selectin list
			row = items[0].row()
			self.table.removeRow(row)
			self.__node.RemoveImageLayer(row)

			rows = self.table.rowCount()
			if rows < aovs_minimum_rows:
				self.table.insertRow(rows)
				# Add empty item if needed (so we can't select empty row)
				empty_item = QtWidgets.QTableWidgetItem( "" )
				empty_item.setFlags( QtCore.Qt.NoItemFlags )
				self.table.setItem( rows, 0, empty_item )

		self.ResizeAOVsTable()

	def OnAcceptAOVSelector(self):
		drivers = [row[0] for row in SA.display_drivers]

		driver = drivers.index( str(self.filetype_widget.currentText()) )
		extension = SA.display_drivers[driver][1]
		drivername = SA.display_drivers[driver][2]

		for c in range(len(self._aov_selector_defs)):
			# Find selected AOVs in the table and add it to the table
			# if there is no item like selected
			aovs = [col[0] for col in self._aov_selector_defs[c]]
			for i in range(self._aov_selector_widgets[c].count()):
				item = self._aov_selector_widgets[c].item(i)
				if item and item.isSelected():
					aov = str(item.text())
					layer_groups = SA.GetLayerGroupsForAOV(
						self.__node,
						self._aov_selector_defs[c][aovs.index(aov)][1] )

					if len(layer_groups) == 0:
						group = self.__node.AddImageLayer(
								aov,
								self._aov_selector_defs[c][aovs.index(aov)][1],
								extension,
								self.GetBitDepth(),
								drivername)
						if group:
							self.AddLayer(aov)

		self.aov_selector.close()
		del self.aov_selector

	def OnCancelAOVSelector(self):
		self.aov_selector.close()
		del self.aov_selector

	def filenameParamChangedCallback(self, *args, **kwds):
		"""
		Called by the filenameParamPolicy when ever a modification is
		made to the underlying filename parameter data.
		"""
		for group in SA.GetLayerGroups(self.__node):
			for output in SA.GetLayerNodes(group):
				SA.SetOutputNodeParameters(output)

	def OnFiletypeChanged(self, index):
		drivers = [row[0] for row in SA.display_drivers]
		driver = drivers.index( str(self.filetype_widget.itemText(index)) )
		extension = SA.display_drivers[driver][1]
		available_quantizes = SA.display_drivers[driver][3]
		driver = SA.display_drivers[driver][2]
		for group in SA.GetLayerGroups(self.__node):
			for output in SA.GetLayerNodes(group):
				# We set the bit depth again as it's stored per file extension.
				SA.SetOutputNodeParameters(
					output, extension=extension, bit_depth=self.GetBitDepth(),
					driver=driver )

		for i, q in enumerate(SA.quantizes):
			model_index = self.filedepth_widget.model().index(i, 0)

			if q in available_quantizes:
				flag = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
			else:
				flag = QtCore.Qt.NoItemFlags

			self.filedepth_widget.model().setData(
					model_index, flag, QtCore.Qt.UserRole-1)

	def GetBitDepth(self):
		index = self.filedepth_widget.currentIndex()
		# Return as a string. This is what RenderOutputDefine node wants.
		return ('8', '16', '32')[index]

	def OnFiledepthChanged(self, index):
		globals_node = SA.GetRefNode(self.__node, 'render_globals')
		# Unset the old global attribute.
		single_parameter = \
			globals_node.getParameter('args.' + 'dlGlobalStatements.' + 'layers.' + 'depth.enable')
		single_parameter.setValue(False, 0)
		# Set the new value on every single output.
		for group in SA.GetLayerGroups(self.__node):
			for output in SA.GetLayerNodes(group):
				SA.SetOutputNodeParameters(
					output, bit_depth=self.GetBitDepth())

	def InitTables(self):
		old_state = self.table.blockSignals(True);

		try:
			self.table.clear()
			for group_name in SA.GetLayerGroupNames( self.__node ):
				self.AddLayer( group_name )

		finally:
			self.table.blockSignals(old_state);

	def ResizeAOVsTable( self ):
		# Resize AOVs table to show all the available data
		rows = self.table.rowCount()
		height = self.table.rowHeight(0)*rows + self.table.frameWidth()*2
		self.table.setMinimumHeight(height)
		self.table.setMaximumHeight(height)
		self.table.horizontalScrollBar().setValue(0)
		self.table.verticalScrollBar().setValue(0)

def AdjustItemHeight( item, height ):
	size_hint = item.sizeHint()
	size_hint.setHeight( height )
	item.setSizeHint( size_hint )

def FillAOVList( list_widget, AOV_definition_tuples, item_height ):
	# Add item for every AOV definition tuple 
	for aov_tuple in AOV_definition_tuples:
		aov_label = SA.GetAOVLabel( aov_tuple )
		aov_item = QtWidgets.QListWidgetItem( aov_label )
			
		AdjustItemHeight(aov_item, item_height)
			
		list_widget.addItem( aov_item )

	# Fill it with empty items to have the grid in the UI
	for i in range( max( 0, selector_min_items - list_widget.count() ) ):
		empty_item = QtWidgets.QListWidgetItem( "" )

		empty_item.setFlags( QtCore.Qt.NoItemFlags )
		AdjustItemHeight( empty_item, item_height )

		list_widget.addItem( empty_item )
