################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2016                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

main_style_base = '''
	QWidget:item:selected
	{
		background-color:
			QLinearGradient(
				x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #6488ab, stop: 1 #486b8e);
	}

	QLabel
	{
		color : #757575;
	}

	QAbstractItemView
	{
		background-color:
			QLinearGradient(
				x1: 0,
				y1: 0,
				x2: 0,
				y2: 1,
				stop: 0 #4d4d4d,
				stop: 0.1 #646464,
				stop: 1 #5d5d5d);
	}

	QTableView::item
	{
		border-bottom: 1px solid #434343;
	}

	QAbstractItemView
	{
		background-color: #282828;
		border-width: 2px;
		border-color: #282828;
		border-style: solid;
		border-radius: 3;
		padding: 0px;
	}

	QListWidget
	{
		background-color: #282828;
		border-color: #404040;
	}

	QListWidget::item
	{
		border-bottom: 1px solid #434343;
		padding: 3px;
		background-color: #282828;
	}

	# Note: Multi-light widget overrides this style.
	QListWidget::item:disabled
	{
		color: black;
		padding-left: -10px;
	}

'''

main_style = '''
	QPushButton
	{
		color: #b1b1b1;
		background-color:
			QLinearGradient(
				x1: 0,
				y1: 0,
				x2: 0,
				y2: 1,
				stop: 0 #565656,
				stop: 0.1 #525252,
				stop: 0.5 #4e4e4e,
				stop: 0.9 #4a4a4a,
				stop: 1 #464646);
		border-width: 1px;
		border-color: #1e1e1e;
		border-style: solid;
		border-radius: 3;
		padding: 4px;
		padding-left: 1px;
		padding-right: 1px;
	}

	QPushButton:pressed,QPushButton:open
	{
		border-style: inset;
		background-color:
			QLinearGradient(
				x1: 0,
				y1: 0,
				x2: 0,
				y2: 1,
				stop: 0 #2d2d2d,
				stop: 0.1 #2b2b2b,
				stop: 0.5 #292929,
				stop: 0.9 #282828,
				stop: 1 #252525);
	}

	QComboBox
	{
		selection-background-color: #ffaa00;
		background-color:
			QLinearGradient(
				x1: 0,
				y1: 0,
				x2: 0,
				y2: 1,
				stop: 0 #565656,
				stop: 0.1 #525252,
				stop: 0.5 #4e4e4e,
				stop: 0.9 #4a4a4a,
				stop: 1 #464646);
		border-style: solid;
		border: 1px solid #1e1e1e;
		border-radius: 3;
		padding: 1px;
		padding-left: 5px;
	}

	QComboBox:hover,QPushButton:hover
	{
		border: 2px solid #6488ab;
		border-left: 0px solid #6488ab;
		border-right: 0px solid #6488ab;
		padding-left: 6px;
		padding-right: 6px;
	}

	QComboBox:on
	{
		padding-top: 3px;
		padding-left: 4px;
		background-color:
			QLinearGradient(
				x1: 0,
				y1: 0,
				x2: 0,
				y2: 1,
				stop: 0 #2d2d2d,
				stop: 0.1 #2b2b2b,
				stop: 0.5 #292929,
				stop: 0.9 #282828,
				stop: 1 #252525);
		selection-background-color: #ffaa00;
	}

	QComboBox QAbstractItemView
	{
		border: 2px solid darkgray;
		selection-background-color:
			QLinearGradient(
				x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #6488ab, stop: 1 #486b8e);
	}

	QComboBox::drop-down
	{
		 subcontrol-origin: padding;
		 subcontrol-position: top right;
		 width: 15px;
		 border-left-width: 0px;
		 border-left-color: darkgray;
		 border-left-style: solid; /* just a single line */
		 border-top-right-radius: 3px; /* same radius as the QComboBox */
		 border-bottom-right-radius: 3px;
	}
'''

small_style = '''
	QLabel
	{
		font-size: 7pt;
		color : #757575;
	}

	QPushButton:pressed,QPushButton:open
	{
		border-style: solid;
		background-color:
			QLinearGradient(
				x1: 0,
				y1: 0,
				x2: 0,
				y2: 1,
				stop: 0 #565656,
				stop: 0.1 #525252,
				stop: 0.5 #4e4e4e,
				stop: 0.9 #4a4a4a,
				stop: 1 #464646);
	}
'''

def GetBaseStyle():
	return main_style_base

def GetStyle():
    return main_style_base + main_style

def GetSmallStyle():
    return small_style

