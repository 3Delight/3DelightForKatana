################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2016                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

from .Node import DelightSettingsNode

def GetEditor():
	from .Editor import DelightSettingsEditor
	return DelightSettingsEditor
