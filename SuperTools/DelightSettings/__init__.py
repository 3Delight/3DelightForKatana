################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2016                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

import Katana
from . import v1 as DelightSettings

_3DELIGHT_RENDERER_NAME = "dl"

if DelightSettings:
	PluginRegistry = [ (
		"SuperTool",
		2,
		_3DELIGHT_RENDERER_NAME.title() + "Settings",
		(DelightSettings.DelightSettingsNode, DelightSettings.GetEditor) ) ]

