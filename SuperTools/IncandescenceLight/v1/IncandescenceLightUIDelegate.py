# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2017                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

# Copyright (c) 2015 The Foundry Visionmongers Ltd. All Rights Reserved.

from PackageSuperToolAPI import UIDelegate
from PackageSuperToolAPI import NodeUtils as NU
from PackageSuperToolAPI import Packages
from .IncandescenceLightPackage import (
    IncandescenceLightPackage, IncandescenceLightEditPackage)
from Katana import QT4FormWidgets, FormMaster, Plugins, UI4

# Our module with common light package code.
from . import LightBase;

# Get the base classes for our UI delegate classes from the PackageSuperToolAPI
# using the base classes of our custom Sky Dome Package classes
GafferThreeAPI = Plugins.GafferThreeAPI
LightUIDelegate = UIDelegate.GetUIDelegateClassForPackageClass(
    GafferThreeAPI.PackageClasses.LightPackage)
LightEditUIDelegate = UIDelegate.GetUIDelegateClassForPackageClass(
    GafferThreeAPI.PackageClasses.LightEditPackage)

class IncandescenceLightCommonUI(LightBase.BaseUIDelegate):
    """
    The common base class of the UIDelegate and the EditUIDelegate.

    This is used for code shared by both classes.
    """

    def addLightAttributesPolicy(self, parentPolicy, editing):
        packageNode = self.getPackageNode()
        attrNode = NU.GetRefNode(packageNode, 'lightAttrNode')
        if attrNode is None:
            return
        paramsToAdd = ['args.geometry.sourceMesh']
        for paramName in paramsToAdd:
            param = attrNode.getParameter(paramName)
            policy = UI4.FormMaster.CreateParameterPolicy(parentPolicy, param)
            self.disableStateChange(policy, editing)
            parentPolicy.addChildPolicy(policy)

class IncandescenceLightUIDelegate(LightUIDelegate,IncandescenceLightCommonUI):
    """
    The UI delegate for the IncandescenceLight package.

    This class is responsible for exposing the parameters on each of the
    parameter tabs. This is done by creating parameter policies attached to the
    parameters on the package's nodes. We can also modify the appearance of the
    parameter tabs by modifying the hints dictionaries on the policies.
    """

    # The hash used to uniquely identify the action of creating a package
    # This was generated using:
    #     hashlib.md5('IncandescenceLight.AddMeshLight').hexdigest()
    AddPackageActionHash = 'c4acbeb08340c994863ecc2d15325008'

    # The keyboard shortcut for creating a package
    DefaultShortcut = 'I'

    def getTabPolicy(self, tabName):
        """
        The main method of a UIDelegate. This is responsible for returning a
        policy instance for each tab. The policy will contain other policies
        that should drive the actual package node's parameters.
        """
        if tabName == "Object":
            return self.__getObjectTabPolicy()
        elif tabName == "Linking":
            return self.__getLinkingTabPolicy()
        else:
            return LightUIDelegate.getTabPolicy(self, tabName)

    def __getObjectTabPolicy(self):
        """
        Returns the widget that should be displayed under the 'Object' tab.
        """
        # Create a root group policy and add some hints on it
        rootPolicy = QT4FormWidgets.PythonGroupPolicy('object')
        rootPolicy.getWidgetHints()['open'] = True
        rootPolicy.getWidgetHints()['hideTitle'] = True

        # Add attributes specific to this light
        self.addLightAttributesPolicy(rootPolicy, False)

        return rootPolicy

    def __getLinkingTabPolicy(self):
        return LightUIDelegate.GetLightLinkingTabPolicy(
            self.getReferencedNode("node_lightLink_illumination"),
            self.getReferencedNode("node_lightLink_shadow"),
            self.getReferencedNode("node_create") )


class IncandescenceLightEditUIDelegate(
        LightEditUIDelegate,IncandescenceLightCommonUI):
    """
    The UI delegate for the IncandescenceLightEdit package.
    """

    def getTabPolicy(self, tabName):
        """
        The main method of a UIDelegate. This is responsible for returning a
        Value Policy for each tab. The Value Policy will contain other policies
        that should drive the actual package node's parameters.
        """
        if tabName == "Object":
            return self.__getObjectTabPolicy()
        else:
            return LightEditUIDelegate.getTabPolicy(self, tabName)

    def __getObjectTabPolicy(self):
        """
        Returns the widget that should be displayed under the 'Object' tab.
        """

        # Create a root group policy and add some hints on it
        rootPolicy = QT4FormWidgets.PythonGroupPolicy('object')
        rootPolicy.getWidgetHints()['open'] = True
        rootPolicy.getWidgetHints()['hideTitle'] = True

        # Add attributes specific to this light
        self.addLightAttributesPolicy(rootPolicy, True)

        return rootPolicy


# Register the UI delegates

UIDelegate.RegisterUIDelegateClass(
    IncandescenceLightPackage, IncandescenceLightUIDelegate)
UIDelegate.RegisterUIDelegateClass(
    IncandescenceLightEditPackage, IncandescenceLightEditUIDelegate)

